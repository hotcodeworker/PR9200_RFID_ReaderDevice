//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file   config.h 
//! @brief	Configuration File
//! 
//! $Id: config.h 1735 2012-08-31 03:03:46Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2011/10/25		jsyi 	Initial Release


#ifndef CONFIG_H
#define CONFIG_H

////////////////////////////////////////////////////////////
// CLOCK Configuration
////////////////////////////////////////////////////////////
#define SYS_OSC_CLK	 	(19200000UL)

#if (SYS_OSC_CLK 	== 	19200000UL)
	#define SYS_DIG_CLK		(SYS_OSC_CLK)
	#define SYS_ANLG_CLK	(SYS_OSC_CLK)
#elif (SYS_OSC_CLK 	== 	38400000UL)
	#define SYS_DIG_CLK		(SYS_OSC_CLK/2)
	#define SYS_ANLG_CLK	(SYS_OSC_CLK)
#endif

#define SYSTEM_TICK_FREQ	(1000) // 1 ms

////////////////////////////////////////////////////////////
// Chip version 
////////////////////////////////////////////////////////////
#define __CHIP_VER_PR9200CS__


////////////////////////////////////////////////////////////
// Simulation Feature
////////////////////////////////////////////////////////////
//#define __FEATURE_SIM_TAG__
//#define __FEATURE_SIM_PR9000_SPI_SINGLE__

////////////////////////////////////////////////////////////
// RCP 
////////////////////////////////////////////////////////////

// RCP Path
//#define __FEATURE_GPIO_SIO_SEL__
#define __FEATURE_UART_RCP__
//#define __FEATURE_SPI_SLAVE_RCP__ 
//#define __FEATURE_I2C_SLAVE_RCP__ 


// RCP CRC
#define __FEATURE_ENABLE_RCP_CRC__
#ifdef __FEATURE_SIM_PR9000_SPI_SINGLE__
#define __FEATURE_SPI_SINGLE_TAG__
#undef __FEATURE_ENABLE_RCP_CRC__
#endif

// RCP TAG Report
#define __FEATURE_PREPEND_RCP_TAG_PC__
//#define __FEATURE_APPEND_RCP_TAG_CRC__

////////////////////////////////////////////////////////////
// UART Feature
////////////////////////////////////////////////////////////
#define UART0_BAUDRATE	(115200)

#ifdef __FEATURE_UART_RCP__
#define __FEATURE_UART0__
//#define __FEATURE_UART1__		
#endif
 
////////////////////////////////////////////////////////////
// SPI Feature
////////////////////////////////////////////////////////////
#ifdef __FEATURE_SPI_SLAVE_RCP__
//#define __FEATURE_SPI_SINGLE_TAG__ 
//#undef __DEBUG__
#endif
 
////////////////////////////////////////////////////////////
// RF Feature
////////////////////////////////////////////////////////////
//#define __FEATURE_ANT_SEL__
//#define __FEATURE_RF_TEMP_COMP__
//#define __FEATURE_RF_TX_HP_MODE__
#define __FEATURE_RF_TX_LP_MODE__

#ifdef __FEATURE_RF_TX_LP_MODE__
#define __FEATURE_EXT_PA__
#endif

//#define __FEATURE_USE_XTAL__
#define	__FEATURE_USE_TCXO__

////////////////////////////////////////////////////////////
// MODEM Feature
////////////////////////////////////////////////////////////
#define	__FEATURE_PHASE_ROTATE__
//#define __FEATURE_TAG_RSSI__

//#define	__FEATURE_TARI_6P25__
#define	__FEATURE_TARI_12P5__
#define	__FEATURE_TARI_25__

#define __FEATURE_RX_MOD_FM0__
#define __FEATURE_RX_MOD_M2__
#define __FEATURE_RX_MOD_M4__
#define __FEATURE_RX_MOD_M8__

//#define	__FEATURE_BLF_040__ 
//#define	__FEATURE_BLF_080__ 
#define	__FEATURE_BLF_160__ 
//#define	__FEATURE_BLF_200__ 
//#define	__FEATURE_BLF_213__ 
//#define	__FEATURE_BLF_250__ 
//#define	__FEATURE_BLF_300__ 	
//#define	__FEATURE_BLF_320__ 
//#define	__FEATURE_BLF_640__ 


////////////////////////////////////////////////////////////
// MISC Feature
////////////////////////////////////////////////////////////
//#define  __FEATURE_SCAN_GPIO__
//#define  __FEATURE_USE_SYSTICK__

////////////////////////////////////////////////////////////
// Customized Feature
////////////////////////////////////////////////////////////
#define __FEATURE_INTERNAL__
#define __FEATURE_NXP_UCODE__ // UCODE
//#define __READ_BTN_ENABLE__
//#define __ENGINEER_MODE__
////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////
//#define	 __DEBUG__
//#define __AJT_DEBUG__
#ifdef 	__DEBUG__
	//#define	__DEBUG_ISO180006C__
	#ifdef __DEBUG_ISO180006C__
	//#define __DEBUG_ISO180006C_BLOCKWRITE__
	//#define __DEBUG_ISO180006C_KILL__
	#endif
	//#define	__DEBUG_RF__
	//#define __DEBUG_REGISTRY__
	//#define __DEBUG_RFIDFSM__
	//#define __DEBUG_ANTICOL__
	//#define  	__DEBUG_NXPFSM__
#endif // __DEBUG__

 
////////////////////////////////////////////////////////////
// TARGET Info
////////////////////////////////////////////////////////////
#define STR_MODEL       	"SA-i1000"
//#define STR_SN				__DATE__
#define STR_SN				"1.1.5"//BT BLE:check ack from BT
#define STR_FW_TIME			__TIME__
#define STR_MANFACT			"AJANTECH"
#define STR_FREQ			"840-960"

 
#define FW_YEAR ((__DATE__ [9] - '0')* 10 + (__DATE__ [10] - '0'))
#define FW_MONTH (__DATE__ [2] == 'n' ? (__DATE__ [1] == 'a' ? 0 : 5) \
  : __DATE__ [2] == 'b' ? 1 \
  : __DATE__ [2] == 'r' ? (__DATE__ [0] == 'M' ? 2 : 3) \
  : __DATE__ [2] == 'y' ? 4 \
  : __DATE__ [2] == 'n' ? 5 \
  : __DATE__ [2] == 'l' ? 6 \
  : __DATE__ [2] == 'g' ? 7 \
  : __DATE__ [2] == 'p' ? 8 \
  : __DATE__ [2] == 't' ? 9 \
  : __DATE__ [2] == 'v' ? 10 : 11)
#define FW_DAY ((__DATE__ [4] == ' ' ? 0 : __DATE__ [4] - '0') * 10 \
  + (__DATE__ [5] - '0'))
  

#endif
