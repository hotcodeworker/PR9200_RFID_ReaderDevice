//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	hal.c
//! @brire	Hardware Abstract Layer
//! 
//! $Id: hal.c 1763 2012-09-24 06:56:31Z sjpark $			
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/03/02	sjpark	Initial release
//! 2012/03/13	jsyi	Revisied

//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------  
#include "PR9200.h"
#include "modem.h"
#include "iso180006c.h"
#include "sio.h"
#include "rf.h"
#include "gpio.h"
#include "pwrmgmt.h"
#include "registry.h"
#include "i2c.h"
#include "hal.h"
#include "iap.h"
#include "rfidfsm.h"
#include "rcp.h"

//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!-------------------------------------------------------------------  
extern reg_band_type		reg_band;
extern modem_prm_type		modem_c;
extern rssi_scan_type		rssi_scan_c;
extern reg_fh_tbl_type		reg_fh_tbl;
extern hal_gpio_out_type	external_pa_mode_sel_ctxt;
extern reg_serial_no_type	reg_serial_no;

//!-------------------------------------------------------------------                 
//! Strunctures and enumerations
//!------------------------------------------------------------------- 

//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!------------------------------------------------------------------- 
hal_pwrmgmt_type hal_pwrmgmt = { {PMODE_S2_IDLE} };
hal_gpio_out_type	pwr_hold = {{HAL_GPIO_DEV_OUT, 1, 1, HAL_GPIO_POL_NORMAL},
		NULL,NULL,NULL};
	
//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------
#define HAL_SYSTICK_DISABLE()	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk
#define HAL_SYSTICK_ENABLE()	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk	
#define IAP_MAIN(mode)			((void (*)(int)) (IAP_START_ADDRESS + 0x1))(mode);

//!---------------------------------------------------------------
//!	@brief
//! 	Initialize peripheral, modem and rf module 
//!
//! @param
//!		None
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S hal_init()
{		
	timer_init();
#ifdef __TARGET_USIM__
	delay_10us(5000); // 50 ms
#endif
 	hal_gpio_register(&pwr_hold);
	if(!reg_sys_rst.reset)		// if system_reset is true
	{
		//pwr_hold.off_cb(&pwr_hold);
	}

	pwrmgmt_init();
	modem_init();
  rf_init();
	

	hal_set_region(reg_band.region);
	hal_set_ch(reg_band.cur_ch, reg_band.cur_ch_ext);	
	reg_fhlbt.prm.fh_enable = HAL_FH_ON;
//	hal_set_reg_fb_param();
//	reg_band.active.val = REG_INACTIVE;
	hal_set_tx_pwr_lvl(reg_tx.power);
//	reg_tx.active.val = REG_INACTIVE;	
		
	return SYS_S_OK;
}

SYS_S hal_get_fhlbt_prm(hal_fhlbt_prm_type *prm)
{
	*prm = reg_fhlbt.prm;

	return SYS_S_OK;
}
	
SYS_S hal_set_fhlbt_prm(const hal_fhlbt_prm_type *prm)
{
	reg_fhlbt.prm = *prm;
		
	reg_fhlbt.prm.sense_time
		= LIMIT (prm->sense_time, 1, 1000); // 1ms to 1s
	reg_fhlbt.prm.tx_on_time 
		= LIMIT (prm->tx_on_time, 10, 40000); // 10ms to 40s
	reg_fhlbt.prm.tx_off_time 
		= LIMIT (prm->tx_off_time, prm->sense_time + 10, 40000); // 10ms to 40s

	reg_fhlbt.active.val = REG_ACTIVE;

	return SYS_S_OK;
}




//!---------------------------------------------------------------
//!	@brief
//!		Set the system to the deep sleep mode
//!
//! @param
//!		None
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_DEEP_SLEEP
//!
//!---------------------------------------------------------------
SYS_S hal_set_sys_deep_sleep(void)
{
	if(hal_pwrmgmt.pwr_mode != PMODE_S2_IDLE) 
		return SYS_S_HAL_ERR_DEEP_SLEEP;
	
	hal_pwrmgmt.pwr_mode = PMODE_S0_DEEPSLEEP;
	
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);
	
	timer_suspend_all(TRUE); // suspend timers
	pwrmgmt_deepsleep();
	timer_suspend_all(FALSE); // resume timers
	
	hal_pwrmgmt.pwr_mode = PMODE_S2_IDLE;
	
	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Set the system to the sleep mode
//!
//! @param
//!		None
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S hal_set_sys_sleep(void)
{
	hal_pwrmgmt.pwr_mode = PMODE_S1_SLEEP;
	
	HAL_SYSTICK_DISABLE();
	pwrmgmt_sleep();
	HAL_SYSTICK_ENABLE();

	hal_pwrmgmt.pwr_mode = PMODE_S2_IDLE;

	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Gets current rssi value
//!
//! @param
//!		uint16 *rssi16
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S hal_get_rssi(uint16 *rssi16)
{  
	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
	RF_SET_TX_OFF();
	*rssi16 = modem_get_rssi();
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);

	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Enable/Disable the RFID block
//!
//! @param
//!		uint8 mode
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_RFIDBLK_CTRL
//!
//!---------------------------------------------------------------
SYS_S hal_set_rfidblk_pwr(uint8 mode)
{
#ifdef __FEATURE_SIM_TAG__
	return SYS_S_OK;
#endif 

	switch(mode)
	{
	case HAL_RFIDBLK_ON:
		{
			static int8 temp_prev = -100;
			
			if(hal_pwrmgmt.pwr_mode != PMODE_S2_IDLE) 
				return SYS_S_HAL_ERR_RFIDBLK_CTRL;
			
//			MODEM_R_ENABLE();

			RF_SET_TX_ON();
			RF_SET_LDO_ON();
			delay_10us(30); // 300 us	  
			RF_SET_PLL_ON();
			delay_10us(20); // 200 us	  
			if(temp_prev != RFID_SYSTEM_TEMP)
			{
 				RF_SET_TX_OFF();
				debug_msg_str("rf_vco_coarse_tune");
				rf_vco_coarse_tune();
				rf_update_vco();
				temp_prev = RFID_SYSTEM_TEMP;
				delay_ms(1);
				RF_SET_TX_ON();	
			}
#ifdef	__FEATURE_EXT_PA__
			external_pa_mode_sel_ctxt.on_cb(&external_pa_mode_sel_ctxt);
#endif
			hal_pwrmgmt.pwr_mode = PMODE_S3_RFPLLRX;  			
		}
		break;
	case HAL_RFIDBLK_OFF:
		{
			if(hal_pwrmgmt.pwr_mode == PMODE_S4_RFPLLRXTX)	
			{
				hal_set_cw(HAL_CW_OFF);
			}
			else if(hal_pwrmgmt.pwr_mode == PMODE_S2_IDLE) 
			{
				return SYS_S_HAL_ERR_RFIDBLK_CTRL;
			}
#ifdef	__FEATURE_EXT_PA__
			external_pa_mode_sel_ctxt.off_cb(&external_pa_mode_sel_ctxt);
#endif
//#if	!defined(__FEATURE_SIM_TAG__)
			RF_SET_PLL_OFF();
			RF_SET_LDO_OFF();
//#endif
//			MODEM_R_DISABLE();

			hal_pwrmgmt.pwr_mode = PMODE_S2_IDLE;
		}
		break;
	}
	
	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		On/Off the CW
//!
//! @param
//!		uint8 mode
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_CW_CTRL
//!
//!---------------------------------------------------------------
SYS_S hal_set_cw(uint8 mode)
{
	switch(mode)
	{
	case HAL_CW_ON:
		{
			if(hal_pwrmgmt.pwr_mode == PMODE_S2_IDLE) 
			{
				hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
			}
			else if(hal_pwrmgmt.pwr_mode == PMODE_S4_RFPLLRXTX) 
			{
				return SYS_S_HAL_ERR_CW_CTRL;
			}				 

			delay_10us(1); // 10 us
			modem_set_dac_ramp_up(50);	
			hal_pwrmgmt.pwr_mode = PMODE_S4_RFPLLRXTX;
			delay_10us(100); // < 1500 us settling time
		}
		break;
	case HAL_CW_OFF:
		{
			if(hal_pwrmgmt.pwr_mode != PMODE_S4_RFPLLRXTX)
			{
				return SYS_S_HAL_ERR_CW_CTRL;
			}
			else
			{
				modem_set_dac_ramp_dn(50);
 				RF_SET_TX_OFF();
				hal_pwrmgmt.pwr_mode = PMODE_S3_RFPLLRX;
			}		
		}
		break;		
	default:
		return SYS_S_HAL_ERR_CW_CTRL;
	}
	
	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Set current tx power level
//!
//! @param
//!		uint16 pwr_pa
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S hal_set_tx_pwr_lvl(int16 pwr_pa)
{		 
	modem_set_tx_gain(pwr_pa);
	
	if(hal_pwrmgmt.pwr_mode == PMODE_S4_RFPLLRXTX) //to dodge modem issue
	{
		modem_set_dac_ramp_dn(1);	
		modem_set_dac_ramp_up(1);	
	}
	reg_tx.power = pwr_pa;
	reg_tx.active.val = REG_ACTIVE;

	return SYS_S_OK;
}


SYS_S hal_get_tx_pwr_lvl(int16 *pwr_pa)
{		 
	*pwr_pa = modem_get_tx_gain();

	return SYS_S_OK;
}


//!---------------------------------------------------------------
//!	@brief
//!		Set frequency hopping table
//!
//! @param
//!		
//!
//! @return
//!		SYS_S_OK
//!		
//!
//!---------------------------------------------------------------	 
SYS_S hal_get_freq_hopping_table(hal_rf_freq_hopping_table_type *tbl)
{
	rf_get_hopping_table(tbl);

	return SYS_S_OK;
}


//!---------------------------------------------------------------
//!	@brief
//!		Set frequency hopping table
//!
//! @param
//!		
//!
//! @return
//!		SYS_S_OK
//!		
//!
//!---------------------------------------------------------------	 
SYS_S hal_set_freq_hopping_table(hal_rf_freq_hopping_table_type *tbl)
{
	rf_set_hopping_table(tbl);

   	reg_fh_tbl.active.val = REG_ACTIVE;

	return SYS_S_OK;
}


//!---------------------------------------------------------------
//!	@brief
//!		Set current region
//!
//! @param
//!		uint8 region
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_SET_REGION
//!
//!---------------------------------------------------------------	 
SYS_S hal_set_region(uint8 region)
{
	if( !rf_set_region(region) )
		return SYS_S_HAL_ERR_SET_REGION;

	reg_band.active.val = REG_ACTIVE;

	return SYS_S_OK;
}


//!---------------------------------------------------------------
//!	@brief
//!		Retrieves current region
//!
//! @param
//!		uint8 region
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------	 
SYS_S hal_get_region(uint8 *region)
{
	*region = reg_band.region;
	
	return SYS_S_OK;
}


//!---------------------------------------------------------------
//!	@brief
//!		Set current channel
//!
//! @param
//!		uint8 ch
//!		uint8 ch_ext
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_SET_CH
//!
//!---------------------------------------------------------------	 
SYS_S hal_set_ch(uint8 ch, uint8 ch_ext)
{
	if( !rf_set_ch(ch, ch_ext) )
		return SYS_S_HAL_ERR_SET_CH;

	reg_fhlbt.prm.fh_enable = HAL_FH_OFF;

	reg_band.active.val = REG_ACTIVE;
	
	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Retrieves current channel
//!
//! @param
//!		uint8 *ch
//!		uint8 *ch_ext
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------	
SYS_S hal_get_ch(uint8 *ch, uint8 *ch_ext)
{	
	*ch = reg_band.cur_ch;
	*ch_ext = reg_band.cur_ch_ext;
	
	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Retrieves a item from the registry
//!	
//! @param
//!		uint16 reg_address
//!		registry_item_type* item
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_REG_ADD_NOT_EXIST
//!
//!---------------------------------------------------------------
SYS_S hal_get_registry(uint16 reg_address, uint8 *ret_len, uint8 *ret_byte)
{
	if(!reg_read(reg_address,ret_len,ret_byte))
		return 	SYS_S_HAL_ERR_REG_ADD_NOT_EXIST;

	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Update the registry 
//!   	Executes erase, write and verify operation sequentially
//!
//! @param
//!		None
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_ERASE_REGISTRY
//!
//!---------------------------------------------------------------
SYS_S hal_update_registry(void)
{
	if(!reg_erase())
		return SYS_S_HAL_ERR_ERASE_REGISTRY;

	if(!reg_write())
	{
		debug_msg_str_reg("Failure to write registry.");
		reg_erase();
		return SYS_S_HAL_ERR_WRITE_REGISTRY;
	}

	return SYS_S_OK;	
}

SYS_S hal_erase_registry(void)
{
	if(!reg_erase())
		return SYS_S_HAL_ERR_ERASE_REGISTRY;

	return SYS_S_OK;	
}


SYS_S hal_start_download(void)
{
	HAL_SYSTICK_DISABLE();

#if defined(__FEATURE_UART_RCP__)
	IAP_MAIN(MODE_IAP_UART);   // UART
#elif defined(__FEATURE_SPI_SLAVE_RCP__)
	IAP_MAIN(MODE_IAP_SPI);   // SPI
#elif defined(__FEATURE_I2C_SLAVE_RCP__)
	IAP_MAIN(MODE_IAP_I2C);   // I2C
#endif
	return SYS_S_OK;
}


//!---------------------------------------------------------------
//!	@brief
//!		Initialize I2C master
//!
//! @param
//!		None
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S hal_i2c_master_init()
{
	i2c_master_init();		
	return SYS_S_OK;	
}


//!---------------------------------------------------------------
//!	@brief
//!		Write a block of data to the I2C Interface
//!
//! @param
//!		uint8 slave_add
//!		uint8 *data	
//!		uint16 length
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_I2C_NACK
//!
//!---------------------------------------------------------------
SYS_S hal_i2c_master_write(uint8 slave_add, uint8 *data, uint16 length)
{
	uint16 i;

	if(!i2c_master_write_start(slave_add))
	{		
		i2c_gen_stop();
		return SYS_S_HAL_ERR_I2C_NACK;
	}

	for(i=0; i<length; i++)
	{
		if(!i2c_master_write(*data++))
		{
			i2c_gen_stop();
			return SYS_S_HAL_ERR_I2C_NACK;
		}
			
	}	
	
	i2c_gen_stop();

	return SYS_S_OK;	
}

//!---------------------------------------------------------------
//!	@brief
//!		Read a block of data from the I2C Interface
//!
//! @param
//!		uint8 slave_add
//!		uint8 *data	
//!		uint16 length	
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_I2C_NACK
//!
//!---------------------------------------------------------------
SYS_S hal_i2c_master_read(uint8 slave_add, uint8 *data, uint16 length)
{
	uint16 i;

	
	if(!i2c_master_read_start(slave_add))
	{		
		i2c_gen_stop();
		return SYS_S_HAL_ERR_I2C_NACK;
	}

	for(i=0; i<length; i++)
	{
		if(i  == (length - 1))
			I2C_GEN_NACK();		
		*data++ = i2c_master_read();	
	}	

	i2c_gen_stop();

	return SYS_S_OK;
}



//!---------------------------------------------------------------
//!	@brief
//!		Register gpio type
//!
//! @param
//!		hal_gpio_id_type* dev
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_NULL_DEV_TYPE
//!		SYS_S_HAL_ERR_GPIO_RESERVED
//!
//!---------------------------------------------------------------
SYS_S hal_gpio_register(void* hal_gpio_id)
{
	//registry_item_type *reg_map = &registry_map.GPIO;

	if( ((hal_gpio_id_type*)hal_gpio_id)->dev == HAL_GPIO_DEV_NONE)
		return SYS_S_HAL_ERR_NULL_DEV_TYPE;

	/*	
	if( reg_map[( (((hal_gpio_id_type*)hal_gpio_id)->port * 8) 
		+ ((hal_gpio_id_type*)hal_gpio_id)->bit)].active == REG_ACTIVE)		
		return SYS_S_HAL_ERR_GPIO_RESERVED;
	*/
	
	gpio_register_dev(hal_gpio_id);

	return SYS_S_OK;
}


//!---------------------------------------------------------------
//!	@brief
//!		Register timer callback function
//!
//! @param
//!		hal_timer_type* timer
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_ERR_TIMER3_CB_EXIST
//!
//!---------------------------------------------------------------
SYS_S hal_timer_register(hal_timer_type* timer)
{
	if(!timer3_register(timer))
		return SYS_S_HAL_ERR_TIMER_ALREADY_REGISTERED;		

	return SYS_S_OK;
}

SYS_S hal_timer_deregister()
{
	timer3_deregister();
	return SYS_S_OK;
}

SYS_S hal_get_temperature(int8 *temp)
{
	*temp = RFID_SYSTEM_TEMP;
	return SYS_S_OK;
}


SYS_S hal_get_cw_detected(uint8 *cw_detected)
{
	uint16 rssi;

	hal_get_rssi(&rssi);

	if((rssi * -1) < reg_fhlbt.prm.lbt_rf_level)
		*cw_detected = FALSE;
	else 
		*cw_detected = TRUE;		
	
	
	delay_10us(15);
	return SYS_S_OK;
}

SYS_S hal_power_off()
{
	FSM_EVENT(EVENT_FSM_INVENTORY_DISCONTINUE);
	rcp_write_msg_ack(RCP_MSG_RSP, RCP_CMD_PWR_OFF);
	pwr_hold.off_cb(&pwr_hold);

	return SYS_S_OK;
}

SYS_S hal_set_serial_no(hal_serial_no_type *tbl)
{
	uint8 i;

	for(i=0; i<tbl->size ; i++)
	{
		reg_serial_no.serial_no[i] = tbl->ptr[i];
	}

   	reg_serial_no.active.val = REG_ACTIVE;

	return SYS_S_OK;
}

SYS_S hal_get_power_off_time(uint16 *time)
{
	*time = reg_sys_off.time;

	return SYS_S_OK;
}

SYS_S hal_set_power_off_time(uint16 time)
{
	reg_sys_off.time = time;
	reg_sys_off.active.val = REG_ACTIVE;

	return SYS_S_OK;
}

