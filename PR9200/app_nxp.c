//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file 	app_nxp.c
//! @brief  Example of user application - NXP requirement
//! 
//! $Id: app_nxp.c 1748 2012-09-06 08:07:56Z sjpark $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/05/10	jsyi		initial release

//!-------------------------------------------------------------------
//! Include Files
//!------------------------------------------------------------------- 
#include "PR9200.h"
#include "hal.h"
#include "protocol.h"
#include "app_nxp.h"
#include "app_nxp_led.h"
#include "rcp.h"
#include "debug.h"

//!-------------------------------------------------------------------
//! Definitions
//!-------------------------------------------------------------------


//!-------------------------------------------------------------------
//! External Data References                                          
//!-------------------------------------------------------------------
extern nxp_led_ctxt_type 	nxp_led_ctxt;

//!-------------------------------------------------------------------
//! Global Data Declaration                                           
//!-------------------------------------------------------------------
nxp_ctxt_type		nxp_fsm_ctxt;
//rfid_pkt_rx_type	nxp_epc_c[NXP_EPC_TAG_MAX];

//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------	
#define DEV_SEL	 	0xA2
#define CONFIG_WORD	0x2040 
#define WORD1_ADD	0x6000		// user memory				  
#define WORD2_ADD	0x6002		// user memory

#define UCODEI2C_RF_PORT1			(1<<7)
#define UCODEI2C_RF_PORT2			(1<<6)

#define UCODEI2C_RF_ACTIVE_FLAG	(1<<13)

#define RF_PORT_ON	(1)
#define RF_PORT_OFF	(0)	

#define ACTIVE		(0)
#define INACTIVE	(1)

hal_gpio_ext_in_type nxp_gpio_in1 
	= {	{HAL_GPIO_DEV_EXT_INT, 0, 3},
		HAL_GPIO_EXTINT_EDGE_BOTH,
		NULL,  NULL, NULL, nxp_input1_handler};
hal_gpio_ext_in_type nxp_gpio_in2 
	= {	{HAL_GPIO_DEV_EXT_INT, 1, 0}, 
		HAL_GPIO_EXTINT_EDGE_SINGLE_LOW,
		NULL,  NULL, NULL, nxp_input2_handler};


#define NXP_FSM_EVENT(_eparam) do {\
	EVENT _e;\
	nxp_fsm_ctxt.next_state = _eparam;\
	_e.handler = nxp_event_dispatch;\
	_e.param = _eparam;\
	event_post(_e);} while(0)


//!---------------------------------------------------------------
//!	@brief
//!		Initialize parameters for customer
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void nxp_init(void)
{				
	hal_gpio_register(&nxp_gpio_in1);
	hal_gpio_register(&nxp_gpio_in2);
	
	rcp_register_req_cb(&nxp_parse_rcp);
	protocol_register_tag_report_cb(nxp_get_epc);
	protocol_register_tag_completed_cb(nxp_complete_inventory);
	
	nxp_led_init();
	hal_i2c_master_init();
	nxp_ucodei2c_set_rf_interface(RF_PORT_OFF);	

	nxp_fsm_ctxt.sw_status = INACTIVE;	
	nxp_fsm_ctxt.bt_status = INACTIVE;	
	nxp_fsm_ctxt.led_status = LED_STOP;

	nxp_gpio_in1.enint_cb(&nxp_gpio_in1);
	nxp_gpio_in2.enint_cb(&nxp_gpio_in2);

	NXP_FSM_EVENT(EVENT_NXP_CLR_WORD);

}

//!---------------------------------------------------------------
//!	@brief
//!		Input 1 Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void nxp_input1_handler()
{
	if(nxp_gpio_in1.in_cb(&nxp_gpio_in1))
		nxp_fsm_ctxt.sw_status = INACTIVE;	
	else 
		nxp_fsm_ctxt.sw_status = ACTIVE;
		
	NXP_FSM_EVENT(nxp_fsm_ctxt.next_state);					
}

//!---------------------------------------------------------------
//!	@brief
//!		Input 2 Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void nxp_input2_handler()
{				   
	if(nxp_fsm_ctxt.next_state == EVENT_NXP_LED_MODE1)
	{
		nxp_fsm_ctxt.bt_status = ACTIVE;	
		NXP_FSM_EVENT(nxp_fsm_ctxt.next_state);		
	}
}

void nxp_led_handler()
{
	NXP_FSM_EVENT(nxp_fsm_ctxt.next_state);	
}



#ifdef __DEBUG_NXPFSM__
void nxp_fsm_debug_identify_event(const EVENT e)
{
	switch(e.param)
	{
	case EVENT_NONE:
		debug_msg_str_nxp_fsm("EVENT_NONE");
		break;
	case EVENT_ENTRY:
		debug_msg_str_nxp_fsm("EVENT_ENTRY");		
		break;
	case EVENT_NXP_CHECK_WORD1_EXPIRED:
		debug_msg_str_nxp_fsm("EVENT_NXP_CHECK_WORD1_EXPIRED");		
		break;		
	case EVENT_NXP_CLR_WORD:
		debug_msg_str_nxp_fsm("EVENT_NXP_CLR_WORD");
		break;		
	case EVENT_NXP_CHECK_WORD1:
		debug_msg_str_nxp_fsm("EVENT_NXP_CHECK_WORD1");
		break;
	case EVENT_NXP_LED_MODE0:
		debug_msg_str_nxp_fsm("EVENT_NXP_LED_MODE0");		
		break;		
	case EVENT_NXP_READ_EPC:
		debug_msg_str_nxp_fsm("EVENT_NXP_READ_EPC");		
		break;
	case EVENT_NXP_CHECK_EPC:
		debug_msg_str_nxp_fsm("EVENT_NXP_CHECK_EPC");		
		break;		
	case EVENT_NXP_LED_MODE1:
		debug_msg_str_nxp_fsm("EVENT_NXP_LED_MODE1");
		break;		
	case EVENT_NXP_LED_MODE2:
		debug_msg_str_nxp_fsm("EVENT_NXP_LED_MODE2");		
		break;		
	case EVENT_NXP_WAIT_EVENT:
		debug_msg_str_nxp_fsm("EVENT_NXP_WAIT_EVENT");		
		break;		
	case EVENT_NXP_WAIT_IN1_INACTIVE:
		debug_msg_str_nxp_fsm("EVENT_NXP_WAIT_IN1_INACTIVE");		
		break;	
	default:
		debug_msg_str_nxp_fsm("EVENT_[%d]", e.param);		
		break;
	}
}


#endif

//!---------------------------------------------------------------
//!	@brief
//!		Read a word from ucode i2c moemory via i2c
//!
//! @param
//!		uint16 add
//!		uint16 data
//!
//! @return
//!		TRUE/FALSE
//!
//!---------------------------------------------------------------
BOOL nxp_read_word(uint16 add, uint16 *data)
{
	uint8 addr[2] = {0,};
	uint8 temp[2] = {0,};

	addr[0] = MSB(add);
	addr[1] = LSB(add);


	if(hal_i2c_master_write(DEV_SEL, &addr[0], 2) != SYS_S_OK)
		return FALSE;

	if(hal_i2c_master_read(DEV_SEL, &temp[0], 2) != SYS_S_OK)
		return FALSE;
				 	
	*data =  TO_U16(temp[0] , temp[1]);
	delay_ms(20);

	return TRUE;
		
}


//!---------------------------------------------------------------
//!	@brief
//!		Write a word to ucode i2c moemory via i2c
//!
//! @param
//!		uint16 add
//!		uint16 data
//!
//! @return
//!		TRUE/FALSE
//!
//!---------------------------------------------------------------
BOOL nxp_ucodei2c_wirte_word(uint16 add, uint16 data)
{
	uint8 temp[4] = {0,};

	temp[0] = MSB(add);
	temp[1] = LSB(add);
	temp[2] = MSB(data);
	temp[3] = LSB(data);
	
	if(hal_i2c_master_write(DEV_SEL, &temp[0], 4))
		return FALSE;
	
	delay_ms(20);
	return TRUE;
}


//!---------------------------------------------------------------
//!	@brief
//!		Cofigure the rf interface of ucode i2c
//!
//! @param
//!		mode - RF_PORT_ON / RF_PORT_OFF
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void nxp_ucodei2c_set_rf_interface(uint8 mode)  // on or off
{
	uint16 cfg_wd;
	uint16 active_rf;

	nxp_read_word(CONFIG_WORD, &cfg_wd);

	if(cfg_wd & UCODEI2C_RF_ACTIVE_FLAG)
		active_rf = UCODEI2C_RF_PORT2;
	else
		active_rf = UCODEI2C_RF_PORT1;

	if(mode ==  RF_PORT_ON)
	{
		if(!(cfg_wd & active_rf)) return;	 // on	
	}
	else
	{
		if(cfg_wd & active_rf)	return;	 // off
	}

	
	nxp_ucodei2c_wirte_word(CONFIG_WORD, active_rf);
}


//!---------------------------------------------------------------
//!	@brief
//!		Configure the parameters for tag_buf read
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void nxp_start_inventory()
{
	rfid_inventory_prm_type param;

	param.cmd_sel = CMD_INVENTORY_MULTIPLE_CYCLE;
	param.count = 10;
	param.mtnu = 0;
	param.mtime = 0;

	
	nxp_fsm_ctxt.inv_completed	= FALSE;	
	protocol_perform_inventory(&param);					   
}


void nxp_complete_inventory(uint8 discontinued, uint8 detected)
{
	EVENT e;
										
	if(nxp_fsm_ctxt.next_state != EVENT_NXP_CHECK_EPC)
		return;
	
	nxp_fsm_ctxt.inv_completed = TRUE;
	
	if(detected)
		nxp_fsm_ctxt.tag_detected= TRUE;
	else
		nxp_fsm_ctxt.tag_detected= FALSE;
	
	e.handler = nxp_event_dispatch;
	e.param = EVENT_NXP_CHECK_EPC;
	event_send(e);	
}


//!---------------------------------------------------------------
//!	@brief
//!		Get tag_buf data and store last byte of EPC 
//!
//! @param
//!		uint8* data
//!		uint8 len
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void nxp_get_epc(uint8 type, uint8 len, uint8* data)
{	

	if(nxp_fsm_ctxt.next_state != EVENT_NXP_CHECK_EPC)
		return;

#if(0)
	uint8 i;
	uint8 j;
	uint8 diff_cnt;
	uint8 *src_ptr;
	uint8 *dst_ptr;

	for(j=0 ; j<nxp_fsm_ctxt.epc_cnt ; j++)
	{
		diff_cnt = 0;
		src_ptr = (uint8 *) &(data[0]);
		dst_ptr = (uint8 *) &(nxp_epc_c[j].air_pkt[0]);
	

		for( i = 0; i < len; i++)
		{
			
			if(*dst_ptr != *src_ptr)
			{
				diff_cnt++;
				break;			
			}

			dst_ptr++; src_ptr++;
			
		}

		if( diff_cnt == 0 ) return;		
	}

	src_ptr = (uint8 *) &(data[0]);
	dst_ptr = (uint8 *) &(nxp_epc_c[nxp_fsm_ctxt.epc_cnt].air_pkt[0]);
	
	
	for(i=0 ; i<len ; i++)
	{
		*dst_ptr++ = *src_ptr++;
	}	

	nxp_epc_c[nxp_fsm_ctxt.epc_cnt].byte_length = len;											   	
	nxp_fsm_ctxt.word2 = nxp_epc_c[nxp_fsm_ctxt.epc_cnt].air_pkt[nxp_epc_c[nxp_fsm_ctxt.epc_cnt].byte_length - 3];
	nxp_fsm_ctxt.epc_cnt++;
#endif
	nxp_fsm_ctxt.word2 = data[len - 3];

}


//!---------------------------------------------------------------
//!	@brief
//!		Clear content of word1 and word2 of ucode i2c memory
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void nxp_ucodei2c_clear_word()
{
	// clear word1, word2
	nxp_fsm_ctxt.word1 = 0x0000;
	nxp_fsm_ctxt.word2 = 0x0000;
	nxp_ucodei2c_wirte_word(WORD1_ADD, nxp_fsm_ctxt.word1);
	nxp_ucodei2c_wirte_word(WORD2_ADD, nxp_fsm_ctxt.word2);
}





//!---------------------------------------------------------------
//!	@brief
//!		Process for customer functions
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------

uint8 custom_timer_expired = FALSE;

void nxp_event_dispatch(EVENT e)
{
#ifdef __DEBUG_NXPFSM__
		nxp_fsm_debug_identify_event(e);	
#endif
	
	switch(e.param)
	{
	
	case EVENT_ENTRY:
		// initialize your state !! :) jsyi
		break;
	case EVENT_NXP_CLR_WORD:
		{		
			nxp_ucodei2c_clear_word();
			nxp_ucodei2c_set_rf_interface(RF_PORT_ON);
			
			NXP_FSM_EVENT(EVENT_NXP_CHECK_WORD1);
		}
		break;
	case EVENT_NXP_CHECK_WORD1:
		{			
			nxp_read_word(WORD1_ADD, &nxp_fsm_ctxt.word1);
			nxp_led_perform(MODE0, (uint8)(nxp_fsm_ctxt.word1));	 
			NXP_FSM_EVENT(EVENT_NXP_LED_MODE0);			
		}
		break;		
	case EVENT_NXP_LED_MODE0:
		{	
			if(nxp_fsm_ctxt.sw_status == ACTIVE)
			{				
				nxp_led_stop();
				nxp_ucodei2c_set_rf_interface(RF_PORT_OFF);
				NXP_FSM_EVENT(EVENT_NXP_READ_EPC);
			}
			else if(nxp_fsm_ctxt.led_status == LED_STOP)
			{
				custom_timer_expired = FALSE;
				e.param = EVENT_NXP_CHECK_WORD1_EXPIRED;				
				event_post_delayed(e, 500);
	
				NXP_FSM_EVENT(EVENT_NXP_WAIT_EVENT);		
			}
		}
		break;
	case EVENT_NXP_CHECK_WORD1_EXPIRED:
		{
			custom_timer_expired = TRUE;
			NXP_FSM_EVENT(EVENT_NXP_WAIT_EVENT);
		}
		break;		
	case EVENT_NXP_WAIT_EVENT:
		{		
			if(nxp_fsm_ctxt.sw_status == ACTIVE)
			{
				nxp_ucodei2c_set_rf_interface(RF_PORT_OFF);			
				NXP_FSM_EVENT(EVENT_NXP_READ_EPC);			 
			}
			else if(custom_timer_expired)			//if(fsm_timer.custom_time >= 5000) 		
			{	
				NXP_FSM_EVENT(EVENT_NXP_CHECK_WORD1);
			}	 
		}
		break;				  
	case EVENT_NXP_READ_EPC:
		{	
			nxp_start_inventory();		// nxp_start_inventory()

			NXP_FSM_EVENT(EVENT_NXP_CHECK_EPC);
		}
		break;
	case EVENT_NXP_CHECK_EPC:
		{
			if(nxp_fsm_ctxt.inv_completed)			// success
			{
				nxp_fsm_ctxt.word1 = 0x0000;		// clear word1
				if(!nxp_fsm_ctxt.tag_detected)
				{
					nxp_fsm_ctxt.word2 = 0x0000;	//clear word2
				}
				
				nxp_ucodei2c_wirte_word(WORD1_ADD, nxp_fsm_ctxt.word1);
				nxp_ucodei2c_wirte_word(WORD2_ADD, nxp_fsm_ctxt.word2);
				nxp_led_perform(MODE1, (uint8)(nxp_fsm_ctxt.word2));
				nxp_ucodei2c_set_rf_interface(RF_PORT_ON);
				NXP_FSM_EVENT(EVENT_NXP_LED_MODE1);

			}
			else if(nxp_fsm_ctxt.sw_status == INACTIVE)
			{
				NXP_FSM_EVENT(EVENT_NXP_CHECK_WORD1);
			}
		}
		break;
	case EVENT_NXP_LED_MODE1:
		{
			if(nxp_fsm_ctxt.sw_status == INACTIVE)
			{
				nxp_led_stop();
				NXP_FSM_EVENT(EVENT_NXP_CHECK_WORD1);

			}
			else if(nxp_fsm_ctxt.bt_status == ACTIVE)
			{
				nxp_led_stop();
				nxp_led_perform(MODE2, (uint8)(nxp_fsm_ctxt.word2));
				nxp_fsm_ctxt.bt_status = INACTIVE;
				
				NXP_FSM_EVENT(EVENT_NXP_LED_MODE2);
			}
		}
		break;
	case EVENT_NXP_LED_MODE2:
		{
			if(nxp_fsm_ctxt.sw_status == INACTIVE)
			{
				nxp_led_stop();				
				NXP_FSM_EVENT(EVENT_NXP_CHECK_WORD1);
			}
			else if(nxp_fsm_ctxt.led_status == LED_STOP)
			{						
				NXP_FSM_EVENT(EVENT_NXP_WAIT_IN1_INACTIVE);
			}
		}
		break;
	case EVENT_NXP_WAIT_IN1_INACTIVE:		
		if(nxp_fsm_ctxt.sw_status == INACTIVE)
		{
			NXP_FSM_EVENT(EVENT_NXP_CLR_WORD);
		}
		break;

	}

}

void nxp_read_ucodei2c(rcp_req_type *req)
{
	uint16 add;
	uint16 word_cnt;
	uint8 rarray[RCP_PKT_MAX_BUF_SIZ];
	uint16 temp = 0;
	uint8 err;
	
	uint16 i;

	add = TO_U16(req->pkt.payload[0], req->pkt.payload[1]);
	word_cnt = TO_U16(req->pkt.payload[2], req->pkt.payload[3]);

	for(i=0; i<word_cnt; i+=2)
	{
		if(!nxp_read_word(add + i, &temp))
		{
			err = FALSE;
			break;
		}
		rarray[i] = MSB(temp);
		rarray[i+1] = LSB(temp);
		
	}
	
	if(err)
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_CUSTOM_READ_I2C, &rarray[0], word_cnt);	
	else
		rcp_write_msg_nack(RCP_MSG_RSP,RCP_CMD_CUSTOM_READ_I2C);
}


void nxp_write_ucodei2c(rcp_req_type *req)
{
	uint16 add;
	uint16 word_cnt;
	uint16 temp;
	uint8 *ptr;
	uint8 err = TRUE;
	
	uint16 i;

	add = TO_U16(req->pkt.payload[0], req->pkt.payload[1]);
	word_cnt = TO_U16(req->pkt.payload[2], req->pkt.payload[3]);
	ptr = &(req->pkt.payload[4]);

	for(i=0; i<word_cnt; i+=2)
	{
		temp = TO_U16(*ptr++, *ptr++);		
		if(!nxp_ucodei2c_wirte_word(add + i, temp))
		{  
			err = FALSE;
			break;
		}		
	}
 
	if(err)
	{	
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_CUSTOM_WRITE_I2C);
	}
	else
	{
		rcp_write_msg_nack(RCP_MSG_RSP,RCP_CMD_CUSTOM_WRITE_I2C);
	}

}

void nxp_set_led_time(rcp_req_type *req)
{
	uint8 mode;
	nxp_led_perform_type param;

	mode = req->pkt.payload[0];

		param.on_cnt = 
		(req->pkt.payload[1] << 24) |
		(req->pkt.payload[2] << 16) |
		(req->pkt.payload[3] << 8 ) |
		(req->pkt.payload[4]) ;

	param.off_cnt = 
		(req->pkt.payload[5] << 24) |
		(req->pkt.payload[6] << 16) |
		(req->pkt.payload[7] << 8 ) |
		(req->pkt.payload[8]) ;

	param.duration = 
		(req->pkt.payload[9] << 24) |
		(req->pkt.payload[10] << 16) |
		(req->pkt.payload[11] << 8 ) |
		(req->pkt.payload[12]) ;

	nxp_led_set_time(mode, &param);

	rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_CUSTOM_SET_LED_TIME);
}


void nxp_parse_rcp(void *p)
{
	rcp_req_type *req;

	req = (rcp_req_type *)p;

	switch(req->pkt.cmd_code)
	{
	case RCP_CMD_CUSTOM_READ_I2C:
		nxp_read_ucodei2c(req);				
		break;
	case RCP_CMD_CUSTOM_WRITE_I2C:
		nxp_write_ucodei2c(req);
		break;
	case RCP_CMD_CUSTOM_SET_LED_TIME:
		nxp_set_led_time(req);
		break;
		
	default:
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_UNDEF_CMD);
		break;
	}

}


