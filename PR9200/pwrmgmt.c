//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	pwrmgmt.c
//! @brief	Power Management Module
//! 
//! $Id: pwrmgmt.c 1575 2012-04-12 13:11:05Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release
//! 2011/07/25	sjpark	Modified pwrmgmt_deepsleep


//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------  
#include "PR9200.h"
#include "pwrmgmt.h"
#include "rfidfsm.h"

//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!-------------------------------------------------------------------  
extern hal_gpio_out_type	pwr_hold;
extern hal_gpio_ext_in_type power_btn_press;
extern hal_gpio_out_type	external_pa_mode_sel_ctxt_cw;
#ifdef __READ_BTN_ENABLE__
extern BOOL read_btn_press;
#endif
//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 

//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    

//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  



//!---------------------------------------------------------------
//!	@brief
//!		Initialization function for power management
//!
//! @param
//!  	none
//!
//! @return
//!  	none
//!---------------------------------------------------------------
void pwrmgmt_init(void)
{
	NVIC_EnableIRQ(EXT0_IRQn);	//enable power key interrupt 
}


//!---------------------------------------------------------------
//!	@brief
//!		Put the device in sleep mode
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void pwrmgmt_sleep()
{
	uint8 gpio_val[3];

	// Set Sleep Mode
	SCB->SCR &= ~SCB_SCR_SLEEPDEEP_Msk;

	// xp02 -> extint0
	gpio_val[0] = GPIO0->DIR;
	gpio_val[1] = GPIO0->IEV;
	gpio_val[2] = GPIO0->IE;
	
	GPIO0->DIR &= ~BIT2;
	GPIO0->IEV  |= BIT2;
	GPIO0->IE |= BIT2;	


	NVIC_EnableIRQ(EXT0_IRQn);
		
	__WFI();	
	
	NVIC_DisableIRQ(EXT0_IRQn);


	GPIO0->DIR = gpio_val[0];
	GPIO0->IEV = gpio_val[1];
	GPIO0->IE = gpio_val[2];

}

//!---------------------------------------------------------------
//!	@brief
//!		Put the device in deep sleep mode
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void pwrmgmt_deepsleep()
{
	uint8 gpio_val[4];

	// Set DeppSleep Mode
	SCB->SCR |= SCB_SCR_SLEEPDEEP_Msk;
	// Clock Control (STOP1 = 1 : StopOsc)
	SYSCON->CLOCKCON = (SYSCON->CLOCKCON & 0x3FFF) | 0x8000;		
	// ClockCnt Delay  (temp value)
	SYSCON->CLOCKCNT = 0x00FF; 

	// xp02 -> extint0
	gpio_val[0] = GPIO0->DIR;
	gpio_val[1] = GPIO0->IS;
	gpio_val[2] = GPIO0->IEV;
	gpio_val[3] = GPIO0->IE;

#ifdef __FEATURE_EXT_PA__
	GPIO0->DIR &= ~BIT3;
	GPIO0->IS |= BIT3;
	GPIO0->IEV  &= ~BIT3;		
	GPIO0->IE |= BIT3;
	NVIC_EnableIRQ(EXT0_IRQn);
#else
	GPIO0->DIR &= ~BIT2;
	GPIO0->IS |= BIT2;
	GPIO0->IEV  &= ~BIT2;		
	GPIO0->IE |= BIT2;	
	NVIC_EnableIRQ(EXT1_IRQn);
#endif

	NVIC_EnableIRQ(WAKEINT_TX_IRQn);

		
	__WFI();	

#ifdef __FEATURE_EXT_PA__	
//	NVIC_DisableIRQ(EXT0_IRQn);
#else
 	NVIC_DisableIRQ(EXT1_IRQn);
#endif
	NVIC_DisableIRQ(WAKEINT_TX_IRQn);		

	GPIO0->DIR = gpio_val[0];
	GPIO0->IS |= gpio_val[1];
	GPIO0->IEV = gpio_val[2];
	GPIO0->IE = gpio_val[3];
	
}

//!---------------------------------------------------------------
//!	@brief
//!		Wakeup Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void WAKEINT_TX_IRQHandler(void)
{
	__NOP();
}

//!---------------------------------------------------------------
//!	@brief
//!		External 0 Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
#ifdef __FEATURE_RF_TX_LP_MODE__	 
void External_0_IRQHandler(void) //power key interrupt
{
	uint32 i,j;

	external_pa_mode_sel_ctxt_cw.on_cb(&external_pa_mode_sel_ctxt_cw); //turn on red led
#ifdef __READ_BTN_ENABLE__
	read_btn_press = TRUE;
#endif
	
	for(i=0;i<10000000;i++)
	{
		if(power_btn_press.in_cb(&power_btn_press)) break;
		debug_msg_str("%d",i);

		if(i==300000) // long press
		{
#ifdef __READ_BTN_ENABLE__
			read_btn_press = FALSE;
#endif
			pwr_hold.toggle_cb(&pwr_hold);
			FSM_EVENT(EVENT_FSM_INVENTORY_DISCONTINUE);
			for(j=0;j<37000;j++) // blink red led
			{
				if(j%6000==0)	external_pa_mode_sel_ctxt_cw.toggle_cb(&external_pa_mode_sel_ctxt_cw);
			}
		}
	}		

	external_pa_mode_sel_ctxt_cw.off_cb(&external_pa_mode_sel_ctxt_cw); //turn off red led

//	__NOP();
//	GPIO0->IC |= BIT3;
//	debug_msg_str("External_0_IRQHandler");
	
	
}
#endif

//!---------------------------------------------------------------
//!	@brief
//!		External 1 Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
#ifdef __FEATURE_RF_TX_HP_MODE__
void External_1_IRQHandler(void)
{
	__NOP();
	GPIO0->IC |= BIT2;	
}
#endif
