//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	registry.h
//! @brief	Registry Module
//! 
//! $Id: registry.h 1702 2012-08-02 02:05:39Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release

#ifndef REGISTRY_H
#define REGISTRY_H

//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "commontypes.h"
#include "debug.h"
#include "flash.h"





//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 

#define REG_ADDRESS			FLASH_LAST_PAGE_ADDRESS
//#define REGISTRY_SIZE		FLASH_PAGE_SIZE				// 4 byte aline!

#define REG_INACTIVE		(0x00)
#define REG_ACTIVE			(0xA5)
#define REG_READONLY		(0xBC)

#define REG16_H(val)		(val >> 8)
#define REG16_L(val)		(val & 0x00ff)


//!-------------------------------------------------------------------
//! Strunctures and enumerations
//!------------------------------------------------------------------- 

#ifdef	__DEBUG_REGISTRY__
#define	debug_msg_str_reg(a,...)	debug_msg_str(a,##__VA_ARGS__)
#else
#define	debug_msg_str_reg(a,...)
#endif







//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!------------------------------------------------------------------- 
extern reg_ver_type 	reg_ver; // register version
extern reg_fw_data_type	reg_fw_data;
extern reg_band_type 	reg_band; // RF_BAND, RF_CH, RF_CH_EXT
extern reg_tx_type		reg_tx;
extern reg_fhlbt_type	reg_fhlbt; // tx_on_time, tx_off_time, sense_time, fh, lbt, cw
extern reg_anticol_type	reg_anticol; // mode
extern reg_modulation_type reg_modulation; // mode
extern reg_prt_prm_type	reg_prt_prm; // qry_q
extern reg_fh_tbl_type		reg_fh_tbl;
extern reg_sys_reset_type reg_sys_rst;
extern reg_sys_off_type reg_sys_off;



//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------
uint8 reg_erase(void);
uint8 reg_write(void);
uint8 reg_load(void);
uint8 reg_read(uint16 item_add, uint8 *ret_len, uint8 *ret_byte);
#endif

