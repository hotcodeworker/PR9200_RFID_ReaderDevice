//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file 	protocol.c
//! @brief	Protol interface layer
//! 
//! $Id: protocol.c 1763 2012-09-24 06:56:31Z sjpark $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/03/16	jsyi		initial release

//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------  
#include <STRING.H>
#include "pr9200.h"
#include "registry.h"
#include "modem.h"
#include "iso180006c.h"
#include "rfidfsm.h"
#include "timer.h"
#include "rfidtypes.h"
#include "hal.h"
#include "timer.h"
#include "utility.h"
#include "event.h"
#include "protocol.h"


//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 

//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!------------------------------------------------------------------- 
extern iso180006c_prm_type iso180006c_ctxt;
extern iso180006c_inventory_prm_type inventory_prm;
extern rfid_fsm_ctxt_type rfid_fsm_ctxt;
extern iso180006c_tag_buffer_type tag_buf;
extern uint8 iso180006c_handle[2];
extern uint8 error_value;


//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------
const protocol_modulation_type PROTOCOL_MODULATION_CONFIG[] =
{
	{MI8, BLF_160, 	DR_64_DIV_3},		// High Sensitivity
	{MI2, BLF_160, 	DR_64_DIV_3}		// High Speed
	// configure your settings below! :)jsyi
};


rfid_inventory_prm_type rfid_inventory_prm;

//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------

//!---------------------------------------------------------------
//!	@brief
//!		Initialize the RFID protocol parameters
//!
//! @param
//!		None
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S protocol_init(void)
{
	iso180006c_init();
	protocol_set_modulation(reg_modulation.mode);
//	reg_modulation.active.val = REG_INACTIVE;
	
	rfid_inventory_prm.count = INVENTORY_COUNT_INFINITE;
	rfid_inventory_prm.mtnu = INVENTORY_MTNU_INFINITE;
	rfid_inventory_prm.mtime = INVENTORY_MTIME_INFINITE;
	
	inventory_prm.update = TRUE;
	rfid_fsm_ctxt.tag_report_cb_count = 0;
	return SYS_S_OK;
}

void protocol_register_tag_report_cb(protocol_tag_report_cb_type cb)
{
	if(rfid_fsm_ctxt.tag_report_cb_count < TAG_CB_COUNT_MAX)
	{
		rfid_fsm_ctxt.tag_report_cb[rfid_fsm_ctxt.tag_report_cb_count] = cb;
		rfid_fsm_ctxt.tag_report_cb_count++;
	}
#ifdef __DEBUG__	
	else while (1); // assert, terminating the program execution. Too many cbs are registered.
#endif
}

void protocol_register_tag_completed_cb(protocol_tag_completed_cb_type cb)
{
	if(rfid_fsm_ctxt.tag_completed_cb_count < TAG_CB_COUNT_MAX)
	{
		rfid_fsm_ctxt.tag_completed_cb[rfid_fsm_ctxt.tag_completed_cb_count] = cb;
		rfid_fsm_ctxt.tag_completed_cb_count++;
	}
#ifdef __DEBUG__		
	else while (1); // assert, terminating the program execution. Too many cbs are registered.
#endif
}


//!---------------------------------------------------------------
//!	@brief
//!		Retrieves ISO18000-6C select command paramters.
//!
//! @param
//!		protocol_c_prm_sel_type **param
//!
//! @return
//! 		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S protocol_get_c_sel_param(protocol_c_prm_sel_type **param)
{
	*param = (protocol_c_prm_sel_type*) &iso180006c_ctxt.sel;
	
	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Configures ISO18000-6C select command paramters.
//!
//! @param
//!		protocol_c_prm_sel_type *param
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S protocol_set_c_sel_param(const protocol_c_prm_sel_type *param)
{
	memcpy((void*) &iso180006c_ctxt.sel, param, sizeof(protocol_c_prm_sel_type));
	inventory_prm.update = TRUE;
	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Retrieves ISO18000-6C query command paramters.
//!
//! @param
//!		protocol_c_prm_qry_type **param
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S protocol_get_c_qry_param(protocol_c_prm_qry_type **param)
{
	*param = (protocol_c_prm_qry_type*) &iso180006c_ctxt.qry;
	
	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Configures ISO18000-6C query command paramters.
//!
//! @param
//!		protocol_c_prm_qry_type *param
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S protocol_set_c_qry_param(const protocol_c_prm_qry_type *param)
{		
	memcpy((void*) &iso180006c_ctxt.qry, param, sizeof(protocol_c_prm_qry_type));
	
	iso180006c_ctxt.qry.q = LIMIT(param->q, 0, 15);
	iso180006c_ctxt.qry.trext = (iso180006c_ctxt.qry.m & 0x02) ? 0 : 1;
		
	iso180006c_set_modulation_param(
		modem_c.rx_blf, 
		(modem_rx_mod_type)(iso180006c_ctxt.qry.m << 6),
		(modem_rx_dr_type)iso180006c_ctxt.qry.dr );
	
	inventory_prm.update = TRUE;
	reg_prt_prm.qry_q = iso180006c_ctxt.qry.q;
	reg_prt_prm.active.val = REG_ACTIVE;
	return SYS_S_OK;
}




//!---------------------------------------------------------------
//!	@brief
//!		Set current moddulation mode for test ( )
//!
//! @param
//!		uint8 mode
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_INVALID_PARAM
//!
//!---------------------------------------------------------------
SYS_S protocol_set_modulation_raw(protocol_modulation_type *param)
{  	 								 
	if(!iso180006c_set_modulation_param(
		(modem_rx_blf_type)param->rx_blf,
		(modem_rx_mod_type)(param->rx_mod << 6),
		(modem_rx_dr_type)param->rx_dr))
		return SYS_S_PRT_ERR_INVALID_PARAM;
	
	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Set current moddulation mode 
//!
//! @param
//!		uint8 mode
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_INVALID_PARAM
//!
//!---------------------------------------------------------------
SYS_S protocol_set_modulation(const uint8 mode)
{  	 								 
	if(mode > (sizeof(PROTOCOL_MODULATION_CONFIG) / sizeof(protocol_modulation_type)) )
		return SYS_S_PRT_ERR_INVALID_PARAM;
		
	if(!iso180006c_set_modulation_param(
		(modem_rx_blf_type)PROTOCOL_MODULATION_CONFIG[mode].rx_blf, 
		(modem_rx_mod_type)PROTOCOL_MODULATION_CONFIG[mode].rx_mod,
		(modem_rx_dr_type)PROTOCOL_MODULATION_CONFIG[mode].rx_dr)) 
		return SYS_S_PRT_ERR_INVALID_PARAM;

	reg_modulation.active.val = REG_ACTIVE;
	reg_modulation.mode = mode;
	
	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Get current moddulation mode 
//!
//! @param
//!		uint8 mode
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_HAL_ERR_INVALID_PARAM
//!
//!---------------------------------------------------------------
SYS_S protocol_get_modulation(uint8 *mode)
{
	*mode = reg_modulation.mode;
	
	return SYS_S_OK;
}



//!---------------------------------------------------------------
//!	@brief
//!		Select the algorithm for performing inventory.
//!
//! @param
//!		uint8 mode
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S protocol_set_anticol_mode(const uint8 mode)
{
	if( mode >= ANTI_COL_MODE_MAX )
		return SYS_S_PRT_ERR_INVALID_PARAM;
	
	iso180006c_set_anticol_mode((protocol_anti_col_type) mode);

	reg_anticol.active.val = REG_ACTIVE;
	
	return SYS_S_OK;
}


//!---------------------------------------------------------------
//!	@brief
//!		Select the algorithm for performing inventory.
//!
//! @param
//!		uint8 mode
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S protocol_get_anticol_mode(uint8 *mode)
{
	*mode =	inventory_prm.anti_col;	
	return SYS_S_OK;
}

//!---------------------------------------------------------------
//!	@brief
//!		Performs inventory.
//!
//! @param
//!		rfid_inventory_prm_type *param
//!
//! @return
//!		SYS_S_OK
//!		SYS_S_PRT_ERR_BUSY
//!		SYS_S_PRT_ERR_INVALID_PARAM
//!
//!---------------------------------------------------------------
SYS_S protocol_perform_inventory(const rfid_inventory_prm_type *param)
{
	if( FSM_STATUS_BUSY == fsm_get_status() )
		return SYS_S_PRT_ERR_BUSY;

	if(param->cmd_sel >= CMD_CODE_MAX)
		return SYS_S_PRT_ERR_INVALID_PARAM;

	memcpy(	(void*) &rfid_inventory_prm, (void*) param, sizeof(rfid_inventory_prm_type));
	
	if(!rfid_inventory_prm.count)
		rfid_inventory_prm.count = INVENTORY_COUNT_INFINITE;

	if(!rfid_inventory_prm.mtime)
		rfid_inventory_prm.mtime = INVENTORY_MTIME_INFINITE;

	if(!rfid_inventory_prm.mtnu)
		rfid_inventory_prm.mtnu = INVENTORY_MTNU_INFINITE;

	tag_buf.count = 0;		
	iso180006c_reset_inventory_mode();

	FSM_EVENT(EVENT_ENTRY);
								    
	return SYS_S_OK;
}


//!---------------------------------------------------------------
//!	@brief
//!		Discontinues inventory.
//!
//! @param
//!		None
//!
//! @return
//!		SYS_S_OK
//!
//!---------------------------------------------------------------
SYS_S protocol_discontinue_inventory(void)	
{		
	FSM_EVENT(EVENT_FSM_INVENTORY_DISCONTINUE);

	return SYS_S_OK;
}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		uint8 err_code
//!
//! @return
//!		SYS_S_PRT_ERR_MEM_OVERRUN
//!		SYS_S_PRT_ERR_MEM_LOCK
//!		SYS_S_PRT_ERR_TAG_LOW_POWER
//!		SYS_S_PRT_ERR_TAG_KNOWN
//!
//!---------------------------------------------------------------
SYS_S protocol_convert_tagerrcode(uint8 err_code)
{
	switch(err_code)
	{
	case 0x81:
		return SYS_S_PRT_ERR_MEM_OVERRUN;
	case 0x82:
		return SYS_S_PRT_ERR_MEM_LOCK;
	case 0x85:
		return SYS_S_PRT_ERR_TAG_LOW_POWER;
	default:
		return SYS_S_PRT_ERR_TAG_KNOWN;
	}
}

//!---------------------------------------------------------------
//!	@brief
//!		Read part or all of a tag's momory
//!
//! @param
//!		const protocol_c_prm_read_erase_type *param
//!		rfid_pkt_rx_type *ret_data
//!
//! @return
//!		SYS_S_OK 
//!		SYS_S_PRT_ERR_BUSY 
//!		SYS_S_PRT_ERR_INVALID_PARAM 
//!		SYS_S_PRT_ERR_NO_TAG
//!		SYS_S_PRT_ERR_ACCESS_TAG 
//!		SYS_S_PRT_ERR_READ_MEM 
//!
//!---------------------------------------------------------------
SYS_S protocol_read_or_block_erase_memory(const protocol_c_prm_read_erase_type *param, rfid_data_type *ret_data)
{		
	rfid_pkt_rx_type pc_epc_crc = {0,};
	rfid_packet_long_type tag_data = {0,};
	uint16 i;
	int8 err = SYS_S_OK;
	uint8 tmpErr[3] = {0,};
    uint8 resultErr = 0x00;

	if( FSM_STATUS_BUSY == fsm_get_status() )
		return SYS_S_PRT_ERR_BUSY;

//	if(param->target_id_len > 32)
//		return SYS_S_PRT_ERR_INVALID_PARAM;
	
//	if(param->word_count > 32)
//		return SYS_S_PRT_ERR_INVALID_PARAM;

	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
	hal_set_cw(HAL_CW_ON);
	
	for(i = 0; i <= param->retry; i++)
	{
		MODEM_AGC_ENABLE();
		error_value = 0x00;
		// 2) EPC Read + Handle Acquisition & EPC Confirmation
		if(!iso180006c_handle_acquisition(param->target_id_ptr, param->target_id_len ,&pc_epc_crc))
		{
			err = SYS_S_PRT_ERR_NO_TAG;
			continue;
		}

		MODEM_AGC_DISABLE();
		// 3) Access	
		if(param->access_pw != 0)
		{
			if(!iso180006c_access(param->access_pw))
			{

				tmpErr[i] = error_value;
				resultErr += tmpErr[i];
				if(resultErr == (param->retry >> 1))
                {
                	err = SYS_S_PRT_ERR_ACCESS_TAG;
	                break;
                }

				err = SYS_S_PRT_ERR_READ_MEM;
				continue;
			}
		}



		if(param->cmd_sel == CMD_READ)
		{
			// 4)  Memory Read
			if(!iso180006c_read( param->mem_bank,
							param->start_add,
							param->word_count,						
							&tag_data))
			{
				err = SYS_S_PRT_ERR_READ_MEM;
				continue;
			}
			else
			{
				
				if((tag_data.air_pkt[0]&0x80) != 0x00)
				{
					err = protocol_convert_tagerrcode(tag_data.air_pkt[0]);
					continue;
				}
				else
				{
					uint16 ret_len;
					uint16 temp_handle;
					temp_handle = TO_U16(iso180006c_handle[1], iso180006c_handle[0]);
					
					bit_array_shift_l(&tag_data.air_pkt[0], &tag_data.air_pkt[0], tag_data.bit_length, 1);
			 		ret_len = (tag_data.bit_length >> 3);
			 		ret_data->len = 0;

					// verify data and find out the length of data using handle. esp. param->word_count == 0. jsyi.
					for(i = 2; i < ret_len; i+= 2)
					{		
						if( *((uint16*) &tag_data.air_pkt[i]) == temp_handle )	
						{
							ret_data->len = i;
							break;
						}		
					}

					if(ret_data->len == 0)
					{
						err = SYS_S_PRT_ERR_READ_MEM;
						continue;
					}
					else
					{
						ret_data->ptr_data = &tag_data.air_pkt[0];
						err = SYS_S_OK;
						break;
					}
					
				}


			}
			
		}
		else if(param->cmd_sel == CMD_BLOCK_ERASE)
		{
			// 4)  Memory Erase
			if(!iso180006c_blockerase( param->mem_bank,
							param->start_add,
							param->word_count))
			{
				err = SYS_S_PRT_ERR_ERASE_MEM;
				continue;
			}
			else
			{
				err = SYS_S_OK;
				break;
			}	
		}
		else
		{
			err = SYS_S_PRT_ERR_INVALID_CMD_SEL;
		}
				
	}


	MODEM_AGC_ENABLE();				 

	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF); 


	if(err != SYS_S_OK)
		return err;

	
	
	return err;
	
}



//!---------------------------------------------------------------
//!	@brief
//!		Write a word in a tag's momory
//!
//! @param
//!		const protocol_c_prm_write_type *param
//!
//! @return
//!		SYS_S_OK 
//!		SYS_S_PRT_ERR_BUSY 
//!		SYS_S_PRT_ERR_INVALID_PARAM
//!		SYS_S_PRT_ERR_NO_TAG 
//!		SYS_S_PRT_ERR_ACCESS_TAG 
//!		SYS_S_PRT_ERR_WRITE_MEM
//!
//!---------------------------------------------------------------
SYS_S protocol_write_memory(const protocol_c_prm_write_type *param)
{
	rfid_pkt_rx_type pc_epc_crc = {0,};
	rfid_packet_long_type tag_data;
	uint8 i;
	int8 err = SYS_S_OK;

	if( FSM_STATUS_BUSY == fsm_get_status() )
		return SYS_S_PRT_ERR_BUSY;

//	if(param->target_id_len > 32)
//		return SYS_S_PRT_ERR_INVALID_PARAM;
	
//	if(param->word_count > 32)
//		return SYS_S_PRT_ERR_INVALID_PARAM;

	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
	hal_set_cw(HAL_RFIDBLK_ON);

	for(i = 0; i <= param->retry; i++)
	{		
		uint8 j;

		MODEM_AGC_ENABLE();
	 	// 2) EPC Read + Handle Acquisition & EPC Confirmation
		if(!iso180006c_handle_acquisition(param->target_id_ptr, param->target_id_len ,&pc_epc_crc))
		{
			err = SYS_S_PRT_ERR_NO_TAG;
			continue;
		}

		MODEM_AGC_DISABLE();
		// 3) Access	
		if(param->access_pw != 0)
		{
			if(!iso180006c_access(param->access_pw))
			{
				err = SYS_S_PRT_ERR_ACCESS_TAG;
				continue;
			}
		}

		

		// 4) Write
		if(param->cmd_sel == CMD_WRITE)
		{
			for(j = 0 ; j < param->word_count; j++)
			{
				uint8 k;
				
				for(k = 0; k <= param->retry; k++)
				{		
					if(!iso180006c_write(
							(protocol_c_membank_type)param->mem_bank, 
							param->start_add + j,
							(param->word_data_ptr)[j]))					
					{
						if(iso180006c_read(
								(protocol_c_membank_type)param->mem_bank,
								param->start_add + j,
								1,
								&tag_data))
						{
							bit_array_shift_l(&tag_data.air_pkt[0],&tag_data.air_pkt[0], 16, 1);																		
							if( param->word_data_ptr[j] == *((uint16*) &tag_data.air_pkt[0]) )
							{
								err = SYS_S_OK;
								break;
							}
							else
								err = SYS_S_PRT_ERR_READ_MEM;
						}
						else
							err = SYS_S_PRT_ERR_READ_MEM;
					}
					else
					{
						err = SYS_S_OK;					
						break;			
					}
				}		

				if(err == SYS_S_PRT_ERR_WRITE_MEM)
				{
					break;
				}
				
			}		

		}
		else if(param->cmd_sel == CMD_BLOCK_WRITE)
		{
			if(!iso180006c_blockwrite(
				(protocol_c_membank_type)param->mem_bank, 
				param->start_add,
				param->word_count,
				param->word_data_ptr))			
			{
				err = SYS_S_PRT_ERR_WRITE_MEM;
			}
		}
		else
		{
			err = SYS_S_PRT_ERR_INVALID_CMD_SEL;
		}
		
		if(err != SYS_S_OK)
			continue;
		else
			break;
		
	}

	MODEM_AGC_ENABLE();
	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF); // OFF
	
	return err;
}

//!---------------------------------------------------------------
//!	@brief
//!		Kill a tag to permanently disable
//!
//! @param
//!		const protocol_c_prm_kill_type *param
//!
//! @return
//!		SYS_S_OK 
//!		SYS_S_PRT_ERR_BUSY 
//!		SYS_S_PRT_ERR_INVALID_PARAM
//!		SYS_S_PRT_ERR_NO_TAG 
//!		SYS_S_PRT_ERR_KILL_TAG
//!
//!---------------------------------------------------------------
SYS_S protocol_kill_memory(const protocol_c_prm_kill_type *param)
{
	rfid_pkt_rx_type pc_epc_crc = {0,};
	uint8 i;
	int8 err = SYS_S_OK;

	if( FSM_STATUS_BUSY == fsm_get_status() )
		return SYS_S_PRT_ERR_BUSY;

//	if(param->target_id_len > 32)
//		return SYS_S_PRT_ERR_INVALID_PARAM;
	
	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
	hal_set_cw(HAL_RFIDBLK_ON);

	for(i = 0; i <= param->retry; i++)
	{	
		MODEM_AGC_ENABLE();
		// 2) EPC Read + Handle Acquisition & EPC Confirmation	
		if(!iso180006c_handle_acquisition(param->target_id_ptr, param->target_id_len ,&pc_epc_crc))
		{
			err = SYS_S_PRT_ERR_NO_TAG;
			continue;
		}
		
		MODEM_AGC_DISABLE();
		
		// 3) Kill
		if(!iso180006c_kill(param->kill_pw, param->recom))
		{
			if(!iso180006c_handle_acquisition(param->target_id_ptr, param->target_id_len ,&pc_epc_crc))
			{
				err = SYS_S_OK;
				break;
			}
			else
			{
				err = SYS_S_PRT_ERR_KILL_TAG;
				continue;
			}
		}
		else
		{
			err = SYS_S_OK;
			break;
		}
		
	}

	MODEM_AGC_ENABLE();
	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);

	return err;

}



//!---------------------------------------------------------------
//!	@brief
//!		Lock a tag to change lock status
//!
//! @param
//!		const protocol_c_prm_lock_type *param
//!
//! @return
//!		SYS_S_OK 
//!		SYS_S_PRT_ERR_BUSY 
//!		SYS_S_PRT_ERR_INVALID_PARAM
//!		SYS_S_PRT_ERR_NO_TAG 
//!		SYS_S_PRT_ERR_ACCESS_TAG 
//!		SYS_S_PRT_ERR_LOCK_TAG
//!
//!---------------------------------------------------------------
SYS_S protocol_lock_memory(const protocol_c_prm_lock_type *param)
{
	rfid_pkt_rx_type pc_epc_crc = {0,};
	uint8 i;
	int8 err = SYS_S_OK;

	if( FSM_STATUS_BUSY == fsm_get_status() )
		return SYS_S_PRT_ERR_BUSY;

//	if(param->target_id_len > 32)
//		return SYS_S_PRT_ERR_INVALID_PARAM;
	
	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
	hal_set_cw(HAL_RFIDBLK_ON);

	for(i = 0; i <= param->retry; i++)
	{
		MODEM_AGC_ENABLE();
		
	 	// 2) EPC Read + Handle Acquisition & EPC Confirmation
		if(!iso180006c_handle_acquisition(param->target_id_ptr, param->target_id_len ,&pc_epc_crc))
		{
			err = SYS_S_PRT_ERR_NO_TAG;
			continue;
		}

		MODEM_AGC_DISABLE();
		
		// 3) Access	
		if(param->access_pw != 0)
		{
			if(!iso180006c_access(param->access_pw))
			{
				err = SYS_S_PRT_ERR_ACCESS_TAG;
				continue;
			}
		}
		
		// 4) Lock
		if(!iso180006c_lock(param->mask_action))
		{
			err = SYS_S_PRT_ERR_LOCK_TAG;
			continue;
		}
		else
		{
			err = SYS_S_OK;
			break;
		}

	}
	MODEM_AGC_ENABLE();

	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);

	return err;
	
}

#ifdef __FEATURE_NXP_UCODE__

//!---------------------------------------------------------------
//!	@brief
//!		Enable read protection of the entire tag memory (Custom Commands)
//!
//! @param
//!		const protocol_c_prm_readprotect_type *param
//!
//! @return
//!		SYS_S_OK 
//!		SYS_S_PRT_ERR_BUSY
//!		SYS_S_PRT_ERR_INVALID_PARAM
//!		SYS_S_PRT_ERR_NO_TAG
//!		SYS_S_PRT_ERR_ACCESS_TAG
//!		SYS_S_PRT_ERR_READPROTECT_TAG
//!
//!---------------------------------------------------------------
SYS_S protocol_readprotect(const protocol_c_prm_readprotect_type *param)
{
	rfid_pkt_rx_type pc_epc_crc = {0,};
	uint8 i;
	int8 err = SYS_S_OK;

	if( FSM_STATUS_BUSY == fsm_get_status() )
		return SYS_S_PRT_ERR_BUSY;

	if(param->target_id_len > 32)
		return SYS_S_PRT_ERR_INVALID_PARAM;
	
	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
	hal_set_cw(HAL_CW_ON);

	for(i = 0; i <= param->retry; i++)
	{	
		MODEM_AGC_ENABLE();
		// 2) EPC Read + Handle Acquisition & EPC Confirmation	
		if(!iso180006c_handle_acquisition(param->target_id_ptr, param->target_id_len ,&pc_epc_crc))
		{
			err = SYS_S_PRT_ERR_NO_TAG;
			continue;
		}

		MODEM_AGC_DISABLE();		// 3) Access	
		if(param->access_pw != 0)
		{
			if(!iso180006c_access(param->access_pw))
			{
				err = SYS_S_PRT_ERR_ACCESS_TAG;
				continue;
			}
		}

		// 4) ReadProtect
		if(!iso180006c_readprotect())
		{		
			err = SYS_S_PRT_ERR_READPROTECT_TAG;
			continue;
		}
		else
		{
			err = SYS_S_OK;
			break;
		}
	}
	
	MODEM_AGC_ENABLE();
	
	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF); // OFF

	return err;	
}


//!---------------------------------------------------------------
//!	@brief
//!		Reset the ReadProtect and re-enable reading of the tag memory (Custom Commands)
//!
//! @param
//!		const protocol_c_prm_reset_readprotect_type *param
//!
//! @return
//!		SYS_S_OK 
//!		SYS_S_PRT_ERR_BUSY 
//!		SYS_S_PRT_ERR_NO_TAG 
//!		SYS_S_PRT_ERR_RESET_READPROTECT_TAG
//!
//!---------------------------------------------------------------
SYS_S protocol_reset_readprotect(const protocol_c_prm_reset_readprotect_type *param)
{
	rfid_pkt_rx_type pc_epc_crc = {0,};
	int8 err = SYS_S_OK;
	uint8 i;	
 
	if( FSM_STATUS_BUSY == fsm_get_status() )
		return SYS_S_PRT_ERR_BUSY;
	
	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
	hal_set_cw(HAL_CW_ON);

	for(i = 0; i <= param->retry; i++)
	{	
		MODEM_AGC_ENABLE();
		// 2) EPC Read + Handle Acquisition & EPC Confirmation	
		if(!iso180006c_handle_acquisition(NULL, 0, &pc_epc_crc))
		{
			err = SYS_S_PRT_ERR_NO_TAG;
			continue;
		}

		MODEM_AGC_DISABLE();

		// 4) Reset ReadProtect
		if(!iso180006c_reset_readprotect(param->readprotect))
		{
			err = SYS_S_PRT_ERR_RESET_READPROTECT_TAG;
			continue;

		}
	}		

	MODEM_AGC_ENABLE();
	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF); // OFF

	return err;	
}

//!---------------------------------------------------------------
//!	@brief
//!		Change an EAS-Alarm code (Custom Commands)
//!
//! @param
//!		const protocol_c_prm_change_eas_type *param
//!
//! @return
//!		SYS_S_OK 
//!		SYS_S_PRT_ERR_BUSY
//!		SYS_S_PRT_ERR_INVALID_PARAM
//!		SYS_S_PRT_ERR_NO_TAG
//!		SYS_S_PRT_ERR_ACCESS_TAG
//!		SYS_S_PRT_ERR_CHANGE_EAS_TAG
//!
//!---------------------------------------------------------------
SYS_S protocol_change_eas(const protocol_c_prm_change_eas_type *param)
{
	rfid_pkt_rx_type pc_epc_crc = {0,};
	uint8 i;
	int8 err = SYS_S_OK;

	if( FSM_STATUS_BUSY == fsm_get_status() )
		return SYS_S_PRT_ERR_BUSY;

	if(param->target_id_len > 32)
		return SYS_S_PRT_ERR_INVALID_PARAM;

	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
	hal_set_cw(HAL_CW_ON);

	for(i = 0; i <= param->retry; i++)
	{
		MODEM_AGC_ENABLE();
		// 2) EPC Read + Handle Acquisition & EPC Confirmation	
		if(!iso180006c_handle_acquisition(param->target_id_ptr, param->target_id_len ,&pc_epc_crc))
		{
			err = SYS_S_PRT_ERR_NO_TAG;
			continue;
		}

		MODEM_AGC_DISABLE();
		// 3) Access	
		if(param->access_pw!= 0)
		{
			if(!iso180006c_access(param->access_pw))
			{
				err = SYS_S_PRT_ERR_ACCESS_TAG;
				continue;
			}
		}

		// 4) ChangeEAS
		if(!iso180006c_change_eas(param->change))
		{
			err = SYS_S_PRT_ERR_CHANGE_EAS_TAG;
			continue;
		}
	}

	MODEM_AGC_ENABLE();
	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF); // OFF

	return err;
}


//!---------------------------------------------------------------
//!	@brief
//!		Get an EAS-Alarmcode (Custom Commands)
//!
//! @param
//!		const protocol_c_prm_eas_alarm_type *param
//!		rfid_data_type *ret_data
//!
//! @return
//!		SYS_S_OK 
//!		SYS_S_PRT_ERR_BUSY
//!		SYS_S_PRT_ERR_EAS_ALARM_TAG
//!
//!---------------------------------------------------------------
SYS_S protocol_eas_alarm(const protocol_c_prm_eas_alarm_type *param)
{
	int8 err = SYS_S_OK;
	rfid_packet_long_type tag_data;
	uint8 i;	

	if( FSM_STATUS_BUSY == fsm_get_status() )
		return SYS_S_PRT_ERR_BUSY;

	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
	hal_set_cw(HAL_CW_ON);

	for(i = 0; i <= param->retry; i++)
	{														   
		// 2) EPC Read + Handle Acquisition & EPC Confirmation
		if(!iso180006c_eas_alarm(&tag_data))
		{
			err = SYS_S_PRT_ERR_EAS_ALARM_TAG;
			continue;
		}
	}
	
	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF); // OFF

	return err;	
}

//!---------------------------------------------------------------
//!	@brief
//!		Execution of the custom Calibrate command
//!
//! @param
//!		const protocol_c_prm_calibrate_type*param
//!		rfid_data_type *ret_data
//!
//! @return
//!		SYS_S_OK 
//!		SYS_S_PRT_ERR_BUSY
//!		SYS_S_PRT_ERR_INVALID_PARAM
//!		SYS_S_PRT_ERR_NO_TAG
//!		SYS_S_PRT_ERR_ACCESS_TAG
//!		SYS_S_PRT_ERR_CALIBRATE_TAG
//!
//!---------------------------------------------------------------
SYS_S protocol_calibrate(const protocol_c_prm_calibrate_type*param, rfid_data_type *ret_data)
{
	rfid_pkt_rx_type pc_epc_crc = {0,};
	rfid_packet_long_type tag_data;
	uint8 i;
	int8 err = SYS_S_OK;

	if( FSM_STATUS_BUSY == fsm_get_status() )
		return SYS_S_PRT_ERR_BUSY;

	if(param->target_id_len > 32)
		return SYS_S_PRT_ERR_INVALID_PARAM;

	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);; 
	hal_set_cw(HAL_CW_ON);

	for(i = 0; i <= param->retry; i++)
	{		
		MODEM_AGC_ENABLE();
		// 2) EPC Read + Handle Acquisition & EPC Confirmation	
		if(!iso180006c_handle_acquisition(param->target_id_ptr, param->target_id_len ,&pc_epc_crc))
		{
			err = SYS_S_PRT_ERR_NO_TAG;
			continue;
		}

		MODEM_AGC_DISABLE();

		// 3) Access	
		if(param->access_pw != 0)
		{
			if(!iso180006c_access(param->access_pw))
			{				
				err = SYS_S_PRT_ERR_ACCESS_TAG;
				continue;
			}
		}

		// 4)  Calibrate
		if(!iso180006c_calibrate(&tag_data))
		{
			err = SYS_S_PRT_ERR_CALIBRATE_TAG;
			continue;
		}
	}	

	ret_data->len = tag_data.bit_length>> 3;
	ret_data->ptr_data = &tag_data.air_pkt[0];

	MODEM_AGC_ENABLE();

	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF); // OFF

	return err;
	
}

#endif


