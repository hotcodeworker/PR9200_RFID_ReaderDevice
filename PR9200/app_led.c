//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file 	app_led.c
//! @brief  Example of user application - LED blink
//!			This example shows you how to use gpio and event
//! 
//! $Id: app_led.c 1608 2012-05-15 03:01:09Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/05/10	jsyi		initial release


#include "event.h"
#include "hal.h"
#include "app_led.h"


hal_gpio_out_type	led_blink_ctxt = {HAL_GPIO_DEV_NONE,}; // configure your device :) jsyi
/*
	= {{HAL_GPIO_DEV_LED, 0, 4, HAL_GPIO_POL_NORMAL},
		NULL,NULL,NULL};
*/

void led_init(void)
{
	hal_gpio_register(&led_blink_ctxt);
}


// event handler
void led_event(EVENT e)
{
	switch(e.param)
	{
	case EVENT_LED_ON:
		led_blink_ctxt.on_cb(&led_blink_ctxt);
		break;
	case EVENT_LED_OFF:
		led_blink_ctxt.off_cb(&led_blink_ctxt);
		break;
	}
}

// blink 1 time
void led_blink(void) 
{
	EVENT e;

	e.handler = led_event;

	// LED will turn on immediately
	e.param = EVENT_LED_ON;		
	event_post(e);	// Put the event into the event fifo immediately

	// LED will turn off 1000 ms later
	e.param = EVENT_LED_OFF;	
	event_post_delayed(e,1000);	// Put the event into the event fifo 1000 ms later
}
