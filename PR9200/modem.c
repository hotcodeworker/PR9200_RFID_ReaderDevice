//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	modem.c
//! @brief	Implementation of Interface for MODEM registers
//! 
//! $Id: modem.c 1756 2012-09-14 09:21:57Z sjpark $			
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release
//! 2011/07/25	sjpark	modified for PR9200



//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------         
#include <STRING.H>
#include <math.h>
#include "PR9200.h"
#include "registry.h"
#include "modem.h"
#include "rf.h"
#include "iso180006c.h"
#include "utility.h"
//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!-------------------------------------------------------------------  
extern	reg_band_type 		reg_band;			
	
#define RSSI_START_VAL_H	(550)
#define RSSI_START_VAL_L	(750)
#define RSSI_TBL_H_LEN	(20)
#define RSSI_TBL_L_LEN	(16)

#define RSSI_CNT_MAX	(3)

//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    
modem_prm_type 		modem_c;
modem_status_type 	modem_s = {0,};
rssi_scan_type 		rssi_scan_c = {0,};

const uint16 RSSI_VAL_H[RSSI_TBL_H_LEN] = { 161,158,154,150,145,139,132,123,114,101,90,79,71,
							 64,55,49,44,38,32,29};		   // -55dBm ~ -74dBm
const uint16 RSSI_VAL_L[RSSI_TBL_L_LEN] = { 143,136,129,121,107,95,87,78,67,60,53,47,44,38,34,29};	// -75dBm ~ -90dBm


#ifdef __FEATURE_TARI_25__
const uint8 DELIMITER_25US[64] = {0x7D,0x7D,0x7D,0x7D,0x7D,0x7C,0x7B,0x79,
								0x77,0x74,0x71,0x6D,0x67,0x61,0x5B,0x53,
								0x4B,0x43,0x3A,0x32,0x2A,0x23,0x1D,0x18,
								0x14,0x10,0x0E,0x0B,0x0A,0x09,0x09,0x09,
								0x09,0x09,0x09,0x0A,0x0B,0x0E,0x10,0x14,
								0x18,0x1D,0x23,0x2A,0x32,0x3A,0x43,0x4B,
								0x53,0x5B,0x61,0x67,0x6D,0x71,0x74,0x77,
								0x79,0x7B,0x7C,0x7C,0x7C,0x7C,0x7C,0x7C };
#endif
#ifdef __FEATURE_TARI_12P5__
const uint8 DELIMITER_12_5US[64] = {0x7D,0x7d,0x7d,0x7d,0x7d,0x7d,0x7d,0x7d,
								0x7d,0x7d,0x7d,0x7d,0x7d,0x7d,0x7c,0x79,
								0x74,0x6d,0x61,0x53,0x43,0x32,0x22,0x15,
								0x0c,0x06,0x02,0x01,0x01,0x01,0x01,0x01,
								0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
								0x01,0x01,0x01,0x01,0x01,0x01,0x03,0x08,
								0x10,0x1b,0x2a,0x3a,0x4b,0x5b,0x67,0x71,
								0x77,0x7b,0x7c,0x7c,0x7c,0x7c,0x7c,0x7c	};
#endif
#ifdef __FEATURE_TARI_6P25__
const uint8 DELIMITER_6_25US[64] = {0x7d,0x7d,0x7d,0x7d,0x7d,0x7d,0x7d,0x7d,
								0x7d,0x7d,0x7d,0x7d,0x7d,0x7d,0x7d,0x7d,
								0x7d,0x7d,0x7d,0x7d,0x7d,0x7d,0x7d,0x7d,
								0x7d,0x7c,0x74,0x61,0x43,0x22,0x0c,0x02,
								0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
								0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
								0x01,0x01,0x01,0x01,0x01,0x01,0x01,0x01,
								0x4b,0x67,0x77,0x7c,0x7c,0x7c,0x7c,0x7c};
#endif

const uint8 ASK_WAVE_TABLE[64] = {0x7e,0x7e,0x7e,0x7e,0x7d,0x7d,0x7b,0x79, 
								0x77,0x74,0x71,0x6c,0x68,0x63,0x5c,0x56, 
								0x4f,0x48,0x42,0x3b,0x35,0x2f,0x2a,0x26, 
								0x23,0x20,0x1e,0x1c,0x1b,0x1a,0x19,0x19, 
								0x19,0x19,0x1a,0x1b,0x1c,0x1e,0x20,0x23, 
								0x26,0x2a,0x2f,0x35,0x3b,0x42,0x48,0x4f, 
								0x56,0x5c,0x63,0x68,0x6c,0x71,0x74,0x77, 
								0x79,0x7b,0x7d,0x7d,0x7e,0x7e,0x7e,0x7e};

const uint8 RX_FAIL_VALUE[4][10] = {	
				  //         40   80  160  200  213  250  256  300  320  640  kHz
  				             75,  75,  75,  60,  57,  97,  94,  80,  75,  48,         // RoMod = 0
				             90,  90,  90,  72,  68, 116, 113,  97,  90,  50,         // RoMod = 1
				             90,  90,  90,  72,  68, 116, 113,  97,  90,  45,         // RoMod = 2
				             90,  90,  90,  72,  68, 116, 113,  97,  90,  45         // RoMod = 3	   					
							};

const uint8 T1_OFFSET[10] = {	
					    //         40  80   160  200  213  250  256  300  320  640  kHz
					           	  80,  40, 20,  20,  20,  10,  10,  10,  10, 10};

							
//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  


//!---------------------------------------------------------------
//!	@brief
//!		T1 Time Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void MT1_IRQHandler(void)			
{
	if(modem_s.t1.status == T1_START)
		modem_s.t1.status = T1_DONE;	
}
  
//!---------------------------------------------------------------
//!	@brief
//!		Tx Done Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void TX_DONE_IRQHandler(void)		
{
	if(modem_s.tx.status == TX_AP_START)
		modem_s.tx.status = TX_AP_DONE;
}

//!---------------------------------------------------------------
//!	@brief
//!		Reset MODEM
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void modem_reset()
{
	SYSCON->MODEMRST = 0x01;
	delay_10us(100);
	SYSCON->MODEMRST = 0x00;
	delay_10us(100);
}

//!---------------------------------------------------------------
//!	@brief
//!		Initaileze MODEM Parameters	
//!
//! @param
//!  	none
//!
//! @return
//!  	none
//!---------------------------------------------------------------
void modem_init(  )
{		   
	modem_reset();
	modem_init_register();

	EMI_WRITE(RM_INT_VECTOR, T1_INT | TX_DONE_INT);
	NVIC_EnableIRQ(MT1_IRQn);
	NVIC_EnableIRQ(TX_DONE_IRQn);

}



//!---------------------------------------------------------------
//!	@brief
//!		Initaileze MODEM Register
//!
//! @param
//!  	none
//!
//! @return
//!  	none
//!---------------------------------------------------------------
void modem_init_register()
{
// 	EMI_WRITE(RM_Tari_MSB,		0x00 			);	//Tari( High ) 		// EMI_WRITE(MODEM_Base + 0x000,	0x00);
//	EMI_WRITE(RM_Tari_LSB, 		0x3B			);	//Tari( Low  ) 		// EMI_WRITE(MODEM_Base + 0x001, 	0x3B);
//	EMI_WRITE(RM_Data1_MSB,		0x00			);	//Data-1 ( High ) 	// EMI_WRITE(MODEM_Base + 0x002,	0x00);
	EMI_WRITE(RM_Data1_LSB, 	0x77			);	//Data-1 ( Low )  	// EMI_WRITE(MODEM_Base + 0x003, 	0x77);
//	EMI_WRITE(MODEM_Base + 0x004, 	0x05);
	EMI_WRITE(MODEM_Base + 0x005,	0xA2);
	EMI_WRITE(MODEM_Base + 0x006, 	0x0F);
	EMI_WRITE(MODEM_Base + 0x007,	0x06);
//	modem_set_delimiter(0x3B);	//	EMI_WRITE(MODEM_Base + 0x008,	0x00);	//	EMI_WRITE(MODEM_Base + 0x009, 	0x3B);
//	EMI_WRITE(MODEM_Base + 0x00A, 	0x01);
//	EMI_WRITE(MODEM_Base + 0x00B, 	0xFF);
//	EMI_WRITE(MODEM_Base + 0x00C, 	0x07);
	EMI_WRITE(RM_DelimiterWaveform_Downsample, 	0x07);	// Delimiter WaveForm downsample	EMI_WRITE(MODEM_Base + 0x00D, 	0x07);
//	EMI_WRITE(MODEM_Base + 0x00E, 	0x00);
//	EMI_WRITE(MODEM_Base + 0x00F,	0x00);

//	EMI_WRITE(MODEM_Base + 0x010,	0x00);
//	EMI_WRITE(MODEM_Base + 0x011, 	0x00);
	EMI_WRITE(MODEM_Base + 0x012, 	0x80);
	EMI_WRITE(MODEM_Base + 0x013, 	0x18); //EMI_CLR_BIT( RM_Tx_Modem_Config, Twos_Complete);		// DAC output offset binary 
//	EMI_WRITE(MODEM_Base + 0x014, 	0x00);
//	EMI_WRITE(MODEM_Base + 0x015, 	0x00);
//	EMI_WRITE(MODEM_Base + 0x016, 	0x00);
//	EMI_WRITE(MODEM_Base + 0x017, 	0x64);
//	EMI_WRITE(MODEM_Base + 0x017, 	0x5A);	// tx gain index

//	EMI_WRITE(MODEM_Base + 0x018,	0x7F);
//	EMI_WRITE(MODEM_Base + 0x019,	0x00);
	EMI_WRITE(MODEM_Base + 0x01A,	0xFF);
//	EMI_WRITE(MODEM_Base + 0x01B,	0x00);
//	EMI_WRITE(MODEM_Base + 0x01C,	0x3F);
//	EMI_WRITE(MODEM_Base + 0x01D, 	0x15);



//	EMI_WRITE(MODEM_Base + 0x022,	0x00);
//	EMI_WRITE(MODEM_Base + 0x023,	0x00);
//	EMI_WRITE(MODEM_Base + 0x024, 	0x00);
	EMI_WRITE(MODEM_Base + 0x027, 	0x00);
	EMI_WRITE(MODEM_Base + 0x028, 	0x00);
	EMI_WRITE(MODEM_Base + 0x029,	0x03); //EMI_WRITE(RM_Rx_Modem_Config, Always_Enable | Twos_Complete); // EMI_CLR_BIT( RM_Rx_Modem_Config, First_LF_Enable);		// Disable First LF  	
//	EMI_WRITE(MODEM_Base + 0x02A,	0x00);
//	EMI_WRITE(MODEM_Base + 0x02B, 	0x00);
	EMI_SET_BIT(RM_IQ_select_Ctrl, Auto_Select);//EMI_WRITE(MODEM_Base + 0x02C,	0x00);
	EMI_WRITE(MODEM_Base + 0x02D, 	0x4B);
	EMI_WRITE(RM_RxFail_Power_Ctrl, 0x50); //	EMI_WRITE(MODEM_Base + 0x02E, 	0x50);

	EMI_WRITE(MODEM_Base + 0x02F,	0xFF);
	EMI_WRITE(MODEM_Base + 0x030, 	0xFF);

//	EMI_WRITE(RM_SENSING_THRESHOLD_16b_MSB, 0);//	EMI_WRITE(MODEM_Base + 0x031, 	0x00);
	EMI_WRITE(RM_SENSING_THRESHOLD_16b_LSB, 3);//	EMI_WRITE(MODEM_Base + 0x032, 	0x03);
//	EMI_WRITE(MODEM_Base + 0x033, 	0x07);
//	EMI_WRITE(MODEM_Base + 0x034, 	0x40);
//	EMI_WRITE(MODEM_Base + 0x035, 	0x04);
//	EMI_WRITE(MODEM_Base + 0x036, 	0x03);
//	EMI_WRITE(MODEM_Base + 0x037, 	0x94);
//	EMI_WRITE(MODEM_Base + 0x038,	0x14);
//	EMI_WRITE(MODEM_Base + 0x039, 	0x29);
	EMI_WRITE(MODEM_Base + 0x03A,	0x25);
	EMI_WRITE(MODEM_Base + 0x03B,	0x09);
//	EMI_WRITE(MODEM_Base + 0x03C,	0x00);
//	EMI_WRITE(MODEM_Base + 0x03D,	0x40);
//	EMI_WRITE(MODEM_Base + 0x03E,	0x20);
//	EMI_WRITE(MODEM_Base + 0x03F,	0x08);
//	EMI_WRITE(MODEM_Base + 0x040, 	0x50);

	// For LBT 
	EMI_WRITE(RM_RSSI_control, Period_852_us | Interval | Auto_RSSI_Enable);		// Auto RSSI Enable
//	EMI_WRITE(RM_LBT_time_MSB, 0x06);
//	EMI_WRITE(RM_LBT_time_LSB, 0x40);	  // 1ms


//	EMI_WRITE(MODEM_Base + 0x1B0, 	0x00);
	EMI_WRITE(RM_DCOC_mode1_Tpri_Wait, 1); //EMI_WRITE(MODEM_Base + 0x1B1,	0x01);
	EMI_WRITE(MODEM_Base + 0x1B2, 	0x01);
	EMI_WRITE(MODEM_Base + 0x1B3, 	0x0B);
	EMI_WRITE(MODEM_Base + 0x1B4, 	0xB8);
//	EMI_WRITE(MODEM_Base + 0x1B5,	0x03);
	EMI_WRITE(MODEM_Base + 0x1B6,	0xE8);
	
//	EMI_WRITE(MODEM_Base + 0x1C0,	0x00);
//	EMI_WRITE(MODEM_Base + 0x1C1,	0x0F);
//	EMI_WRITE(MODEM_Base + 0x1C2,	0xFF);
//	EMI_WRITE(MODEM_Base + 0x1C3, 	0x01);
//	EMI_WRITE(MODEM_Base + 0x1C4,	0x01);

	EMI_WRITE(MODEM_Base + 0x1D0,	0x91);
//	EMI_WRITE(MODEM_Base + 0x1D1,	0x00);
	EMI_WRITE(MODEM_Base + 0x1D2,	0x09);

//	EMI_WRITE(MODEM_Base + 0x1EB, 	0x00);
	EMI_WRITE(RM_MODEM_TP_select, 0xFB);


	modem_set_ask_wave_table();
	modem_set_agc();
	
}
 

#if(0)
//!---------------------------------------------------------------
//!	@brief
//!		Set MODEM Interrupt 
//!
//! @param
//!  	int_vec: interrupt vector type
//!			
//! @return
//!  	none
//!---------------------------------------------------------------
void modem_set_interrupt(uint8 int_vec)
{		 
	EMI_WRITE(RM_INT_VECTOR, int_vec);


	if(int_vec & ALL_IN_ONE || int_vec & RX_SUCCESS_INT)
		NVIC_EnableIRQ(RX_SUCCESS_IRQn);
	else
		NVIC_DisableIRQ(RX_SUCCESS_IRQn);


	if(int_vec & T2_INT)
		NVIC_EnableIRQ(MT2_IRQn);
	else
		NVIC_DisableIRQ(MT2_IRQn);

	
	if(int_vec & T1_INT)
		NVIC_EnableIRQ(MT1_IRQn);
	else
		NVIC_DisableIRQ(MT1_IRQn);

	if(int_vec & TX_DONE_INT)
		NVIC_EnableIRQ(TX_DONE_IRQn);
	else
		NVIC_DisableIRQ(TX_DONE_IRQn);

	if(int_vec & RX_FAIL_INT)
		NVIC_EnableIRQ(RX_FAIL_IRQn);
	else
		NVIC_DisableIRQ(RX_FAIL_IRQn);
 
	if(int_vec & RSSI_INT)
		NVIC_EnableIRQ(RSSI_IRQn);
	else
		NVIC_DisableIRQ(RSSI_IRQn);

	if(int_vec & LBT_INT)
		NVIC_EnableIRQ(LBT_IRQn);
	else
		NVIC_DisableIRQ(LBT_IRQn);
}
#endif

//!---------------------------------------------------------------
//!	@brief
//!		Search lowest RSSI channel
//!
//! @param
//!  	none
//!
//! @return
//!  	none
//!---------------------------------------------------------------
void modem_scan_rssi()
{	 
	uint8 i;
	uint8 ch;
	uint8 best_rssi;
	uint16 temp;

	memset((void*) &rssi_scan_c.scan_val[0], 0x5A, (sizeof(rssi_scan_c) - 3));
			
	rssi_scan_c.best_ch = reg_band.cur_ch;
	temp = modem_get_rssi();
//	temp = modem_get_rssi_s(); //temp = modem_get_rssi_l();		
	best_rssi = temp / 10;

	ch = rssi_scan_c.start_ch;
	for(i = 0; i < (rssi_scan_c.stop_ch  - rssi_scan_c.start_ch + 1); i++)
	{		

		rf_set_ch(ch,0);
		delay_10us(10); //100us
		temp = modem_get_rssi();
		temp = temp / 10;

		rssi_scan_c.scan_val[i] = (uint8)temp;

		if(temp > best_rssi) 
		{
			best_rssi = temp;
			rssi_scan_c.best_ch = ch;	
			//debug_msg_str2("2. [%d] best rssi = %d", (int) rssi_scan_c.best_ch , (int) best_rssi);
		}
		//debug_msg_str2("[%d] rssi = %d", (int) ch, (int) rssi_scan_c.scan_val[i]);		
		ch++;	
	}

	rf_set_ch(rssi_scan_c.best_ch,0); 			  
	
}


//!---------------------------------------------------------------
//!	@brief
//!		Get tag rssi value
//!
//! @param
//!  	uint8* ret_data (raw rssi value)
//!
//! @return
//!  	none
//!---------------------------------------------------------------
void modem_get_tag_rssi(uint8* ret_data)
{
	uint16 gain;

	ret_data[0] = EMI_READ(RM_RSSI_I);
	ret_data[1] = EMI_READ(RM_RSSI_Q);

	gain = TO_U16(EMI_READ(RM_Ich_Gain_Value_MSB), EMI_READ(RM_Ich_Gain_Value_LSB));
	ret_data[2] = (uint8)((gain >> 4) &0xFF);

	gain = TO_U16(EMI_READ(RM_Qch_Gain_Value_MSB), EMI_READ(RM_Qch_Gain_Value_LSB));
 	ret_data[3] = (uint8)((gain >> 4) &0xFF);

}


//!---------------------------------------------------------------
//!	@brief
//!		Set MODEM Delimiter table
//!
//! @param
//!  	tari: tari type
//!
//! @return
//!  	none
//!---------------------------------------------------------------
void modem_set_delimiter_table(modem_tx_tari_type tari)
{
	uint8 i ; 
	uint8 *del;
	
	modem_c.tx_tari = tari;

	switch(tari)
	{
#ifdef 	__FEATURE_TARI_25__
	case TARI_25U:
		del = (uint8*) &DELIMITER_25US;
		break;
#endif		
#ifdef	__FEATURE_TARI_12P5__
	case TARI_12_5U:
		del = (uint8*) &DELIMITER_12_5US;
		break;
#endif		
#ifdef __FEATURE_TARI_6P25__		
	case TARI_6_25U:
		del = (uint8*) &DELIMITER_6_25US;		
		break;
#endif		
	}
	
	for(i=0; i<64; i++)
		EMI_WRITE(RM_Table_delimiter(i), del[i]);
	
}
//!---------------------------------------------------------------
//!	@brief
//!		Set MODEM ASK wave table
//!
//! @param
//!  	None
//!
//! @return
//!  	none
//!---------------------------------------------------------------
void modem_set_ask_wave_table()
{
	uint8 i;

	for(i=0; i<sizeof(ASK_WAVE_TABLE); i++)
		EMI_WRITE(RM_Table_waveform(i), ASK_WAVE_TABLE[i]);
}

//!---------------------------------------------------------------
//!	@brief
//!		Set Rx Modulation Type
//!
//! @param
//!		rx_mod: rx modulation type
//!
//! @return
//!  	none
//!---------------------------------------------------------------
void modem_set_m(modem_rx_mod_type  rx_mod)
{
//	uint8 temp;
//	temp = EMI_READ(RM_Rx_Enable_Ctrl) & 0x3F;
//	EMI_WRITE(RM_Rx_Enable_Ctrl, (uint8) temp  | rx_mod);

#ifdef __FEATURE_RX_MOD_FM0__
	if(rx_mod == FM0)
	{
		EMI_SET_BIT(RM_TED_Control,  TED_Cancel);
		EMI_CLR_BIT(RM_DCOC_control,  DCOC_Cancel);
	}
	else 
#endif
#ifdef __FEATURE_RX_MOD_M2__
	if(rx_mod == MI2)
	{
		EMI_CLR_BIT(RM_TED_Control,  TED_Cancel);
		EMI_SET_BIT(RM_DCOC_control,  DCOC_Cancel);
  		EMI_WRITE(RM_Miller_peak_detect_Offset, 0x14);
		EMI_WRITE(RM_Miller_Premable_Detect_Ctrl, Fixed_Value | 0x08);
	}
	else 
#endif
#ifdef __FEATURE_RX_MOD_M4__
	if(rx_mod == MI4)
	{								  
		EMI_CLR_BIT(RM_TED_Control,  TED_Cancel);
		EMI_SET_BIT(RM_DCOC_control,  DCOC_Cancel);
  		EMI_WRITE(RM_Miller_peak_detect_Offset, 0x14);
		EMI_WRITE(RM_Miller_Premable_Detect_Ctrl, Fixed_Value | 0x10);
	}
	else 
#endif
#ifdef __FEATURE_RX_MOD_M8__
	if(rx_mod == MI8)
	{										
		EMI_CLR_BIT(RM_TED_Control,  TED_Cancel);
		EMI_SET_BIT(RM_DCOC_control,  DCOC_Cancel);
		EMI_WRITE(RM_Miller_peak_detect_Offset, 0x14);
		EMI_WRITE(RM_Miller_Premable_Detect_Ctrl, Fixed_Value | 0x1C);
	}
#endif

	modem_c.rx_mod = rx_mod;

}

//!---------------------------------------------------------------
//!	@brief
//!		Set Tx FIFO with Tx Packet
//!
//! @param
//!  	none
//!
//! @return
//!  	none
//!---------------------------------------------------------------	 
void modem_set_tx_fifo(const uint16 bit_len, const uint8 *byte_array)
{
	int i;
	uint8 *ptr;
	
	EMI_WRITE( RM_Tx_Data_Length_MSB, MSB(bit_len) );
	EMI_WRITE( RM_Tx_Data_Length_LSB, LSB(bit_len) );

	ptr = (uint8*) byte_array;
	for ( i = 0; i < (bit_len + 7) >> 3 ; i++ )
	{
		EMI_WRITE( RM_Tx_Data, *ptr++ );
	}			
}

//!---------------------------------------------------------------
//!	@brief
//!		Set MODEM TXpkt_cfg Register
//!
//! @param
//!  	tx_pkt_start_type: Frame_Sync / Preamble
//!		crc: NO_CRC / CRC_5 / CRC_16
//!		tx_en: Tx_Disable / Tx_Enable
//!
//! @return
//!  	none
//!---------------------------------------------------------------
void modem_transmit_pkt(	tx_header_type 	header,
								tx_crc_type	 	crc,
								const uint16 bit_len, 
								const uint8 *byte_array)
{	  
	modem_set_tx_fifo(bit_len, byte_array);
	
	modem_s.tx.status = TX_AP_START;

	EMI_WRITE(RM_Tx_Enable_Ctrl, (uint8) (TX_DSBASK | header | crc | Tx_Enable) );

	modem_wait_txpkt_send();
	modem_s.t1.status = T1_START;
}

//!---------------------------------------------------------------
//!	@brief
//!		Set MODEM RXcfg_ENV Register
//!
//! @param
//!  	auto_rx: Auto_ACK_En / Auto_ReqRn_En / Auto_Tx_Disable /
//!		crc: CRC_NO / CRC_5 / CRC_16
//!		
//!		
//!		*pc_epc_crc: received packet
//!
//! @return
//!  	BOOL 
//!---------------------------------------------------------------
BOOL modem_receive_pkt	(	auto_transmit_type 	auto_rx, 
					 			rx_crc_type 	crc,
								rx_t1_type t1,
				 				uint16 bit_len,				 				
					 			rfid_pkt_rx_type *ret_rx_pkt)
{
	BOOL ret = FALSE;
	uint8 backup_rxfail;
	uint16 received_bit_len;
	
//	uint8 test_count = 0;
		
	modem_set_rxpkt_len(bit_len);

	if(t1 != T1_TYPICAL)
	{
		uint8 lf_index;	
		uint8 rx_mod;
		set_t1_20ms();
		
		rx_mod = modem_c.rx_mod >> 6;
		lf_index = EMI_READ(RM_LF_rate_index);
		backup_rxfail = EMI_READ(RM_RxFail_value);
		EMI_WRITE(RM_RxFail_value, 240);
		
		if(t1 == T1_20MS_OFFSET)		  
			delay_t1(T1_OFFSET[lf_index]<<rx_mod);		 
	}
	
	modem_wait_t1_timeout();

	do
	{
		//test_count++;
		
	 	EMI_WRITE(RM_Rx_Enable_Ctrl, (uint8) (modem_c.rx_mod | auto_rx | crc | Rx_Enable) );	// Rx Enable

		modem_s.rx.status = RX_AP_START;

		modem_wait_rxpkt_receive();	 

		modem_s.rx.err = EMI_READ(RM_RxStatus);
		if((!(modem_s.rx.err & CRC_OK)) || (modem_s.rx.err & TIME_OUT)  )
		{
			if(t1 != T1_TYPICAL)
				continue;


			if(auto_rx == Auto_ACK_En || auto_rx == Auto_QryRep_En)			
				EMI_WRITE(RM_Rx_Enable_Ctrl,Auto_Tx_Disable);			
			
			return FALSE;
		}
		
		modem_change_rxfifo(); // Dual FIFO
						  
		if(ret_rx_pkt != NULL)
		{
			received_bit_len = modem_get_rxpkt_len();
			modem_get_rx_byte(&received_bit_len, &(ret_rx_pkt->air_pkt[0]));
			ret_rx_pkt->byte_length = (received_bit_len + 7) >> 3;
			if(bit_len == 0)		// if EPC
				modem_get_tag_rssi(&ret_rx_pkt->air_pkt[ret_rx_pkt->byte_length-2]);			
		
			
		}
	 	
		if( (auto_rx != Auto_Tx_Disable)  )
		{	
			modem_s.tx.status = TX_AP_START;				
	 		modem_s.t1.status = T1_START;
			modem_wait_txpkt_send();
		}

		ret = TRUE;
		break;

	}while (get_t1_20ms_expired());

	if(t1 != T1_TYPICAL)
	{
		EMI_WRITE(RM_RxFail_value, backup_rxfail);
		clear_t1_20ms();
		
		//debug_msg_str("test_count = %d",test_count);		
	}
		
	return ret;
	
}


//!---------------------------------------------------------------
//!	@brief
//!		Retrieve a Rx packet from FIFO
//!
//! @param
//!  	*bit_len: Bit length
//!
//! @return
//!  	*adata: Data 
//!---------------------------------------------------------------
void modem_get_rx_byte(uint16 *bit_len, uint8 *adata)
{			
	uint16 i;
	uint8 *ptr;
	uint32 read_add;
	uint16 byte_len;

	if(*bit_len == 0) 
	{		
		*bit_len = modem_get_rxpkt_len();
		if(*bit_len > 272 ) *bit_len = 272;
	}

	byte_len = ((*bit_len + 7) >> 3);
	
   	if(EMI_READ(RM_Rx_FIFO_Select))
		read_add = RM_Rx_Data_0;
	else
	   	read_add = RM_Rx_Data_1;

	ptr = &adata[0];
	for(i = 0; i < byte_len  ; i++)
	{
		*ptr++ = EMI_READ(read_add);
	}


}


//!---------------------------------------------------------------
//!	@brief
//!		Return RSSI value
//!
//! @param
//!  	none
//!
//! @return
//!  	- dBm x 10
//!---------------------------------------------------------------
BOOL modem_get_rssi_h(uint16 *rssi)
{
	uint8 cnt =0;
	uint16 energy = 0;
	uint16 dy = 0;
   	
	rf_set_gain(47,47);		
	delay_10us(5);

		// Get RSSI
	for(cnt = 0; cnt < RSSI_CNT_MAX; cnt++)
	{		
		EMI_WRITE(RM_RSSI_control, Period_106_us |  RSSI_Enable);
		while((EMI_READ(RM_RSSI_control) & RSSI_Enable) == RSSI_Enable);
		energy = energy + EMI_READ(RM_RSSI_I) + EMI_READ(RM_RSSI_Q);
	}

//	debug_msg_str("energy = %d", (int)energy);

	if(energy < RSSI_VAL_H[RSSI_TBL_H_LEN-1])
		return FALSE;

	// RSSI table interpolation
	for(cnt = 0; cnt < RSSI_TBL_H_LEN; cnt ++)
	{
		if( energy  > RSSI_VAL_H[cnt] ) 
		{
			dy = RSSI_VAL_H[cnt] - RSSI_VAL_H[cnt+1];
			dy = ((energy - RSSI_VAL_H[cnt]) * 10) / dy;	
			//dy / 2;
			break;
		}
	}

	// energy to dBm
	*rssi  = RSSI_START_VAL_H + 10 * cnt - dy;

	return TRUE;	
	
}

BOOL modem_get_rssi_l(uint16 *rssi)
{
	uint8 cnt =0;
	uint16 energy = 0;
	uint16 dy = 0;

	rf_set_gain(65,65);
	delay_10us(2);

	// Get RSSI
	for(cnt = 0; cnt < RSSI_CNT_MAX; cnt++)
	{		
		EMI_WRITE(RM_RSSI_control, Period_106_us |  RSSI_Enable);
		while((EMI_READ(RM_RSSI_control) & RSSI_Enable) == RSSI_Enable);
		energy = energy + EMI_READ(RM_RSSI_I) + EMI_READ(RM_RSSI_Q);
	}

//	debug_msg_str("energy = %d", (int)energy);

	 
	// RSSI table interpolation
	for(cnt = 0; cnt < RSSI_TBL_L_LEN; cnt ++)
	{
		if( energy  > RSSI_VAL_L[cnt] ) 
		{
			dy = RSSI_VAL_L[cnt] - RSSI_VAL_L[cnt+1];
			dy = ((energy - RSSI_VAL_L[cnt]) * 10) / dy;	
			//dy / 2;
			break;
		}
	}			  
	// energy to dBm
	*rssi  = RSSI_START_VAL_L + 10 * cnt - dy;

	return TRUE;
	
}

uint16 modem_get_rssi()
{
	uint16 energy = 0;
	uint8 agc_backup;

	uint8 m_reg6;
	uint8 m_reg7;
	uint8 m_reg190;

	uint8 r_reg6;
	uint8 r_reg7;
 	uint8 r_reg8;
	uint8 r_rega;
 	uint8 r_rege;

    agc_backup = EMI_READ(RM_AGC_control);
	m_reg6 = EMI_READ(RM_TRcal_MSB);
	m_reg7 = EMI_READ(RM_TRcal_LSB);
	m_reg190 = EMI_READ(RM_RSSI_control);

	r_reg6 = EMI_READ(RR_RX_FILT_BW_I); 
	r_reg7 = EMI_READ(RR_RX_FILT_BW_Q);
	r_reg8 = EMI_READ(RR_RX_DCOC_BW);
	r_rega = EMI_READ(RR_RX_FILT_AFT);
	r_rege = EMI_READ(RR_RX_MIX_DCOC);													   

	rf_set_ch_offset(24);

	EMI_WRITE(RM_AGC_control, 0x00);	 // Disable AGC
	EMI_WRITE(RM_TRcal_MSB,	0x0F);	//TRcal		
	EMI_WRITE(RM_TRcal_LSB, 0x06) ;	//TRcal	

	EMI_WRITE(RR_RX_FILT_BW_I, 0xEC);			
	EMI_WRITE(RR_RX_FILT_BW_Q, 0xEC);
													
	EMI_WRITE(RR_RX_DCOC_BW, 0xEE);	
	EMI_WRITE(RR_RX_MIX_DCOC, 0xBF);
	EMI_WRITE(RR_RX_FILT_AFT, BIT7 | 0x3E);
	
	delay_10us(20); // 200 us		  		 

   	if(!modem_get_rssi_h(&energy))
		modem_get_rssi_l(&energy);

//	debug_msg_str_modem("energy = %d", (int)energy);

	rf_set_ch_offset(0);			   

 	EMI_WRITE(RM_AGC_control, agc_backup);
	EMI_WRITE(RM_TRcal_MSB, m_reg6);
	EMI_WRITE(RM_TRcal_LSB, m_reg7);
	EMI_WRITE(RM_RSSI_control, m_reg190);

	EMI_WRITE(RR_RX_FILT_BW_I, r_reg6);
	EMI_WRITE(RR_RX_FILT_BW_Q, r_reg7);
	EMI_WRITE(RR_RX_DCOC_BW, r_reg8);
	EMI_WRITE(RR_RX_FILT_AFT, r_rega);
	EMI_WRITE(RR_RX_MIX_DCOC, r_rege);


	return energy;
	
}

//!---------------------------------------------------------------
//!	@brief
//!		Ramp up
//!
//! @param
//!  	cnt: duration
//!
//! @return
//!  	none
//!---------------------------------------------------------------
void modem_set_dac_ramp_up(uint8 cnt)
{
	EMI_WRITE(RM_Ramp_Time_control, cnt);	// Ramp_CNT[7:0]
	EMI_WRITE(RM_Ramp_control, Ramp_Up);	// Tx_Ramp_Up[1]

	while( (EMI_READ(RM_Ramp_control) & Ramp_Up) == Ramp_Up );

}

//!---------------------------------------------------------------
//!	@brief
//!		Ramp down
//!
//! @param
//!  	cnt: duration
//!
//! @return
//!  	none
//!---------------------------------------------------------------		 
void modem_set_dac_ramp_dn(uint8 cnt)
{	
	delay_10us(20); // packet end margin, 200 us
	EMI_WRITE(RM_Ramp_Time_control, cnt);	// Ramp_CNT[7:0]
	EMI_WRITE(RM_Ramp_control, Ramp_Down);	// Tx_Ramp_Up[1]

	while( (EMI_READ(RM_Ramp_control) & Ramp_Down) == Ramp_Down );
}

void modem_set_ted(modem_rx_blf_type blf, modem_rx_mod_type rx_mod)
{
	uint8 temp;
	
	temp = EMI_READ( RM_TED_Control) & 0xFC;

#if defined(__FEATURE_BLF_300__) && defined(__FEATURE_RX_MOD_FM0__)
  	if(rx_mod == FM0 && blf == BLF_300)
	{
		EMI_WRITE(RM_TED_Control, temp | TED_1);		// TED2
	}
	else
#endif	
	{
		EMI_WRITE(RM_TED_Control, temp | TED_2);		// TED2
	}
}




//!---------------------------------------------------------------
//!	@brief
//!		Set Back Link Frequency
//!
//! @param
//!  	blf: BLF_40 / BLF_80 / BLF_160 / BLF_200 / BLF_213 /
//!			BLF_200 / BLF_300 / BLF_320 / BLF_640
//!
//! @return
//!  	none
//!---------------------------------------------------------------

BOOL modem_set_blf(modem_rx_blf_type blf, modem_rx_dr_type dr)
{
	uint8 rx_m = modem_c.rx_mod >> 6;

	switch(blf)
	{
#ifdef __FEATURE_BLF_040__		
		case BLF_40:
			EMI_WRITE(RM_Waveform_Downsample	,	0x07 );			// waveform downsample
	
			EMI_WRITE(RM_RTcal_MSB, 	0x05			);	//RTcal	
			EMI_WRITE(RM_RTcal_LSB, 	0xA2			);	//RTcal	
	
			EMI_WRITE(RM_TRcal_MSB,	 	0x0F			);	//TRcal		
			EMI_WRITE(RM_TRcal_LSB, 	0x06			) ;	//TRcal	
	
			modem_set_delimiter_table(TARI_25U);		

			EMI_WRITE(RM_LF_rate_index,	 LF_40);	
			EMI_WRITE(RM_RxFail_value, RX_FAIL_VALUE[rx_m][LF_40]); 
			break;
#endif
#ifdef __FEATURE_BLF_080__
		case BLF_80:
			EMI_WRITE( RM_Waveform_Downsample, 	0x07 );		  //Down Sampling
													  
			EMI_WRITE( RM_RTcal_MSB,	0x05 );		  //RTcal
			EMI_WRITE( RM_RTcal_LSB,	0xA2 );		  //RTcal		
			
			EMI_WRITE( RM_TRcal_MSB, 	0x07 );		  //TRcal	
			EMI_WRITE( RM_TRcal_LSB , 	0x83 );		  //TRcal
	
			modem_set_delimiter_table(TARI_25U);
			
			EMI_WRITE(RM_LF_rate_index, 	LF_80);	
			EMI_WRITE(RM_RxFail_value, RX_FAIL_VALUE[rx_m][LF_80]); 

			break;
#endif	

#ifdef __FEATURE_BLF_160__
		case BLF_160:			
			if(dr == DR_8 ) 
			{	
				EMI_WRITE( RM_Waveform_Downsample, 0x03 );		  //Down Sampling
				
				EMI_WRITE( RM_RTcal_MSB,	0x02 );		  //RTcal
				EMI_WRITE( RM_RTcal_LSB, 	0xD1 );		  //RTcal	
			
				EMI_WRITE( RM_TRcal_MSB, 	0x03 );		  //TRcal		
				EMI_WRITE( RM_TRcal_LSB ,	0xC2 );		  //TRcal

				modem_set_delimiter_table(TARI_12_5U);
			
			}
			else
			{ 
				EMI_WRITE( RM_Waveform_Downsample, 	0x07 );		  //Down Sampling
	
				EMI_WRITE( RM_RTcal_MSB, 	0x05 );		  //RTcal
				EMI_WRITE( RM_RTcal_LSB, 	0xA2 );		  //RTcal	 
			
				EMI_WRITE( RM_TRcal_MSB, 	0x0A );		  //TRcal	
				EMI_WRITE( RM_TRcal_LSB ,	0x04 );		  //TRcal

				modem_set_delimiter_table(TARI_25U);
			}
	
			EMI_WRITE(RM_LF_rate_index, 	LF_160);
			EMI_WRITE(RM_RxFail_value, RX_FAIL_VALUE[rx_m][LF_160]); 
			break;
#endif
#ifdef __FEATURE_BLF_200__
		case BLF_200:

			EMI_WRITE( RM_Waveform_Downsample, 	0x03 );		  //Down Sampling														  
	
			EMI_WRITE( RM_RTcal_MSB,	0x02 );		  //RTcal
			EMI_WRITE( RM_RTcal_LSB, 	0xD1 );		  //RTcal	
			
			EMI_WRITE( RM_TRcal_MSB, 	0x03 );		  //TRcal		
			EMI_WRITE( RM_TRcal_LSB, 	0x01 );		  //TRcal
	
			modem_set_delimiter_table(TARI_12_5U);
			
			EMI_WRITE(RM_LF_rate_index, 	LF_200);
			EMI_WRITE(RM_RxFail_value, RX_FAIL_VALUE[rx_m][LF_200]); 
			break;
#endif
#ifdef __FEATURE_BLF_213__
#ifdef __FEATURE_TARI_6P25__	
		case BLF_213_3:
			EMI_WRITE( RM_Waveform_Downsample, 	0x01 );		  //Down Sampling
													  
			EMI_WRITE( RM_RTcal_MSB, 	0x01 );		  //RTcal
			EMI_WRITE( RM_RTcal_LSB, 	0x69 );		  //RTcal		
			
			EMI_WRITE( RM_TRcal_MSB, 	0x02 );		  //TRcal		
			EMI_WRITE( RM_TRcal_LSB, 	0xD1 );		  //TRcal
	
			modem_set_delimiter_table(TARI_6_25U);
			
			EMI_WRITE(RM_LF_rate_index, 	LF_213_3);
			EMI_WRITE(RM_RxFail_value, RX_FAIL_VALUE[rx_m][LF_213_3]); 

			break;
#endif	
#endif
#ifdef __FEATURE_BLF_250__
		case BLF_250:			
#ifdef __FEATURE_TARI_6P25__				
			if(dr == DR_8 ) 
			{	
				EMI_WRITE( RM_Waveform_Downsample, 	0x01 );		  //Down Sampling
														  
				EMI_WRITE( RM_RTcal_MSB, 	0x01 );		  //RTcal
				EMI_WRITE( RM_RTcal_LSB, 	0x69 );		  //RTcal	
				
				EMI_WRITE( RM_TRcal_MSB, 	0x02 );		  //TRcal	
				EMI_WRITE( RM_TRcal_LSB, 	0x67 );		  //TRcal
		
				modem_set_delimiter_table(TARI_6_25U);
			}
			else
#endif				
			{
				if(modem_c.rx_mod == MI2)
				{	 				
					EMI_WRITE( RM_Waveform_Downsample, 	0x03 );		  //Down Sampling
															  
					EMI_WRITE( RM_RTcal_MSB, 	0x02 );		  //RTcal
					EMI_WRITE( RM_RTcal_LSB, 	0xD1 );		  //RTcal	
					
					EMI_WRITE( RM_TRcal_MSB, 	0x06 );		  //TRcal	
					EMI_WRITE( RM_TRcal_LSB, 	0x69 );		  //TRcal
					modem_set_delimiter_table(TARI_12_5U);
			   }
			   else
			   {
			
					EMI_WRITE(  RM_Waveform_Downsample, 0x07 );		  //Down Sampling
	
					EMI_WRITE( RM_RTcal_MSB, 0x05 );		  //RTcal
					EMI_WRITE( RM_RTcal_LSB, 0xA2 );		  //RTcal		
					
					EMI_WRITE( RM_TRcal_MSB, 0x06 );		  //TRcal	
					EMI_WRITE( RM_TRcal_LSB, 0x69 );		  //TRcal
	
					modem_set_delimiter_table(TARI_25U);
				}

			}
	
		 	EMI_WRITE(RM_LF_rate_index, 	LF_250);
			EMI_WRITE(RM_RxFail_value, RX_FAIL_VALUE[rx_m][LF_250]); 

			break;
#endif
#ifdef __FEATURE_BLF_300__
		case BLF_300:			
#ifdef __FEATURE_TARI_6P25__	
			if(dr == DR_8 ) 
			{
				EMI_WRITE( RM_Waveform_Downsample, 	0x01 );		  //Down Sampling
				
				EMI_WRITE( RM_RTcal_MSB, 	0x01 );		  //RTcal	  
				EMI_WRITE( RM_RTcal_LSB, 	0x69 );		  //RTcal		
				
				EMI_WRITE( RM_TRcal_MSB, 	0x02 );		  //TRcal	
				EMI_WRITE( RM_TRcal_LSB , 	0x01 );		  //TRcal
		
				modem_set_delimiter_table(TARI_6_25U);		   	
			
			}
			else 
#endif				
			{
				EMI_WRITE(  RM_Waveform_Downsample, 0x03 );		  //Down Sampling

				EMI_WRITE( RM_RTcal_MSB, 0x02 );		  //RTcal
				EMI_WRITE( RM_RTcal_LSB, 0xD1 );		  //RTcal		
				
				EMI_WRITE( RM_TRcal_MSB, 0x05 );		  //TRcal		
				EMI_WRITE( RM_TRcal_LSB, 0x58 );		  //TRcal
				
				modem_set_delimiter_table(TARI_12_5U);	   
			}

			EMI_WRITE(RM_LF_rate_index, 	LF_300);
			EMI_WRITE(RM_RxFail_value, RX_FAIL_VALUE[rx_m][LF_300]); 
			break;

#endif

#ifdef __FEATURE_BLF_320__
		case BLF_320:
#ifdef __FEATURE_TARI_6P25__				
			if(dr == DR_8 ) 
			{
				EMI_WRITE( RM_Waveform_Downsample, 	0x01 );		  //Down Sampling
				EMI_WRITE( RM_RTcal_MSB, 	0x01 );		  //RTcal
				EMI_WRITE( RM_RTcal_LSB, 	0x69 );		  //RTcal		
			
				EMI_WRITE( RM_TRcal_MSB, 	0x01 );		  //TRcal		
				EMI_WRITE( RM_TRcal_LSB , 	0xE1 );		  //TRcal

				modem_set_delimiter_table(TARI_6_25U);

			}
			else 
#endif				
			{
				EMI_WRITE( RM_Waveform_Downsample,	 0x03 );		  //Down Sampling
				EMI_WRITE( RM_RTcal_MSB, 	0x02 );		  //RTcal
				EMI_WRITE( RM_RTcal_LSB, 	0xD1 );		  //RTcal	
			
				EMI_WRITE( RM_TRcal_MSB, 	0x05 );		  //TRcal	
				EMI_WRITE( RM_TRcal_LSB , 	0x02 );		  //TRcal

				modem_set_delimiter_table(TARI_12_5U);

			}
	
			EMI_WRITE(RM_LF_rate_index, 	LF_320);
			EMI_WRITE(RM_RxFail_value, RX_FAIL_VALUE[rx_m][LF_320]); 
			break;
#endif

#ifdef __FEATURE_BLF_640__
#ifdef __FEATURE_TARI_6P25__	
		case BLF_640:
	
			EMI_WRITE( RM_Waveform_Downsample, 	0x01 );		  //Down Sampling
			
			EMI_WRITE( RM_RTcal_MSB, 	0x01 );		  //RTcal
			EMI_WRITE( RM_RTcal_LSB, 	0x69 );		  //RTcal		
			
			EMI_WRITE( RM_TRcal_MSB,	0x02 );		  //TRcal	
			EMI_WRITE( RM_TRcal_LSB , 	0x81 );		  //TRcal
	
			modem_set_delimiter_table(TARI_6_25U);
	
			EMI_WRITE(RM_LF_rate_index, 	LF_640);
			EMI_WRITE(RM_RxFail_value, RX_FAIL_VALUE[rx_m][LF_640]); 
			break;
#endif	
#endif
		default:
			return FALSE;
	}

	modem_c.rx_blf = blf;

	
	return TRUE;
}

#if (0)
//!---------------------------------------------------------------
//!	@brief
//!		Initialize RX threshold and T2 timer
//!
//! @param
//!		type: READ_PWR_TMR / WRITE_PWR_TMR
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void modem_set_pwr_timer(modem_pwr_timer_type type)
{  
	uint8	temp ;
	temp = EMI_READ(RM_Timer_control) & 0xF3;
	switch(type)
	{
	case READ_PWR_TMR:
		EMI_WRITE(RM_Timer_control, temp | Typical);
		break;
	case WRITE_PWR_TMR:			
		EMI_WRITE(RM_Timer_control,	temp | Fixed_20_ms);
		break;
	}		 
}
#endif

//!---------------------------------------------------------------
//!	@brief
//!		Set MODEM AGC register
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void modem_set_agc(void)
{
	switch(modem_c.rx_blf)
	{
#ifdef __FEATURE_BLF_040__		
	case BLF_40:
#endif		
#ifdef __FEATURE_BLF_080__		
	case BLF_80:
#endif		
#ifdef __FEATURE_BLF_160__		
	case BLF_160:
#endif		
#ifdef __FEATURE_BLF_320__		
	case BLF_320:		
#endif		
#if defined(__FEATURE_BLF_040__) || defined(__FEATURE_BLF_080__) || defined(__FEATURE_BLF_160__) || defined(__FEATURE_BLF_320__)
		EMI_WRITE(RM_AGC_Ref_Power, 15);
		EMI_WRITE(RM_AGC_complete_Threshold, 96);
		EMI_WRITE(RM_AGC_Pterm_scale, 100);
		break;
#endif		
#ifdef __FEATURE_BLF_200__		
	case BLF_200:
		EMI_WRITE(RM_AGC_Ref_Power, 12);
		EMI_WRITE(RM_AGC_complete_Threshold, 76);
		EMI_WRITE(RM_AGC_Pterm_scale, 100);
		break;
#endif

#ifdef __FEATURE_BLF_213__		
	case BLF_213_3:
		EMI_WRITE(RM_AGC_Ref_Power, 11);
		EMI_WRITE(RM_AGC_complete_Threshold, 70);
		EMI_WRITE(RM_AGC_Pterm_scale, 100);
		break;
#endif

#ifdef __FEATURE_BLF_250__		
	case BLF_250:
		EMI_WRITE(RM_AGC_Ref_Power, 19);
		EMI_WRITE(RM_AGC_complete_Threshold, 121);
		EMI_WRITE(RM_AGC_Pterm_scale, 50);
		break;
#endif

#ifdef __FEATURE_BLF_300__		
	case BLF_300:
		EMI_WRITE(RM_AGC_Ref_Power, 16);
		EMI_WRITE(RM_AGC_complete_Threshold, 102);
		EMI_WRITE(RM_AGC_Pterm_scale, 100);
		break;
#endif

#ifdef __FEATURE_BLF_640__		
	case BLF_640:
		EMI_WRITE(RM_AGC_Ref_Power, 7);
		EMI_WRITE(RM_AGC_complete_Threshold, 44);
		EMI_WRITE(RM_AGC_Pterm_scale, 100);
		break;
#endif

	}

//	EMI_WRITE(RM_AGC_control, AGC_Enable| AGC_Result_Hold_Enable | (5 << 4) );	   
	EMI_WRITE(RM_AGC_control, AGC_Enable| (5 << 4) );	   
  	EMI_WRITE(RM_AGC_Init, 45);

	EMI_WRITE(RM_AGC_Iterm_scale, 16);		

	EMI_WRITE(RM_AGC_complete_LF	, 4);

}

//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void modem_set_dcoc(void)
{
	switch(modem_c.rx_blf)
	{
#ifdef __FEATURE_BLF_40__				
	case BLF_40:
		EMI_WRITE(RM_AGC_Delay_LSB, 100);
		EMI_WRITE(RM_DCOC_LPF_Delay_MSB	, 0x0B);
		EMI_WRITE(RM_DCOC_LPF_Delay_LSB	, 0xB8);
		EMI_WRITE(RM_DCOC_RXM_Delay_MSB	  , 0x03);
		EMI_WRITE(RM_DCOC_RXM_Delay_LSB	  , 0xE8);
		break;
#endif

#ifdef __FEATURE_BLF_80__
	case BLF_80:
		EMI_WRITE(RM_AGC_Delay_LSB, 50);
		EMI_WRITE(RM_DCOC_LPF_Delay_MSB	, 0x05);
		EMI_WRITE(RM_DCOC_LPF_Delay_LSB	, 0xDC);
		EMI_WRITE(RM_DCOC_RXM_Delay_MSB	  , 0x01);
		EMI_WRITE(RM_DCOC_RXM_Delay_LSB	  , 0xF4);
		break;
#endif

#ifdef __FEATURE_BLF_160__
	case BLF_160:
		EMI_WRITE(RM_AGC_Delay_LSB, 25);
		EMI_WRITE(RM_DCOC_LPF_Delay_MSB	, 0x02);
		EMI_WRITE(RM_DCOC_LPF_Delay_LSB	, 0xEE);
		EMI_WRITE(RM_DCOC_RXM_Delay_MSB	  , 0x00);
		EMI_WRITE(RM_DCOC_RXM_Delay_LSB	  , 0xFA);
		break;
#endif

#ifdef __FEATURE_BLF_200__
	case BLF_200:
#endif
#ifdef __FEATURE_BLF_213__
	case BLF_213_3:
#endif
#if defined(__FEATURE_BLF_200__) || defined (__FEATURE_BLF_213__)
		EMI_WRITE(RM_AGC_Delay_LSB, 20);
		EMI_WRITE(RM_DCOC_LPF_Delay_MSB	, 0x02);
		EMI_WRITE(RM_DCOC_LPF_Delay_LSB	, 0x58);
		EMI_WRITE(RM_DCOC_RXM_Delay_MSB	  , 0x00);
		EMI_WRITE(RM_DCOC_RXM_Delay_LSB	  , 0xC8);
		break;	 
#endif

#ifdef __FEATURE_BLF_250__
	case BLF_250:
		EMI_WRITE(RM_AGC_Delay_LSB, 17);
		EMI_WRITE(RM_DCOC_LPF_Delay_MSB	, 0x01);
		EMI_WRITE(RM_DCOC_LPF_Delay_LSB	, 0xF4);
		EMI_WRITE(RM_DCOC_RXM_Delay_MSB	  , 0x00);
		EMI_WRITE(RM_DCOC_RXM_Delay_LSB	  , 0xA7);
		break;
#endif

#ifdef __FEATURE_BLF_300__
	case BLF_300:
		EMI_WRITE(RM_AGC_Delay_LSB, 14);
		EMI_WRITE(RM_DCOC_LPF_Delay_MSB	, 0x01);
		EMI_WRITE(RM_DCOC_LPF_Delay_LSB	, 0xAD);
		EMI_WRITE(RM_DCOC_RXM_Delay_MSB	  , 0x00);
		EMI_WRITE(RM_DCOC_RXM_Delay_LSB	  , 0x8F);
		break;
#endif

#ifdef __FEATURE_BLF_320__
	case BLF_320:		
		EMI_WRITE(RM_AGC_Delay_LSB, 13);
		EMI_WRITE(RM_DCOC_LPF_Delay_MSB	, 0x01);
		EMI_WRITE(RM_DCOC_LPF_Delay_LSB	, 0x77);
		EMI_WRITE(RM_DCOC_RXM_Delay_MSB	  , 0x00);
		EMI_WRITE(RM_DCOC_RXM_Delay_LSB	  , 0x7D);
		break;					 
#endif

#ifdef __FEATURE_BLF_640__
	case BLF_640:
		EMI_WRITE(RM_AGC_Delay_LSB, 6);
		EMI_WRITE(RM_DCOC_LPF_Delay_MSB	, 0x00);
		EMI_WRITE(RM_DCOC_LPF_Delay_LSB	, 0xBC);
		EMI_WRITE(RM_DCOC_RXM_Delay_MSB	  , 0x00);
		EMI_WRITE(RM_DCOC_RXM_Delay_LSB	  , 0x3F);
		break;
#endif		
	}

}


//!---------------------------------------------------------------
//!	@brief
//!		Set MODEM Tx Gain value
//!
//! @param
//!		pwr_pa: tx gain value
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void modem_set_tx_gain(int16 pwr_pa)
{
	uint16 reg;
		
	reg = (pwr_pa - 100);
	reg = LIMIT(reg,0,100);
	
	EMI_WRITE( RM_Tx_Gain, reg);
}

//!---------------------------------------------------------------
//!	@brief
//!		Get MODEM Tx Gain value
//!
//! @param
//!		pwr_pa: tx gain value
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
int16 modem_get_tx_gain(void)
{
	uint16 pwr_pa;

	pwr_pa = EMI_READ(RM_Tx_Gain);
	
	pwr_pa += 100;
		
	return pwr_pa;
}




