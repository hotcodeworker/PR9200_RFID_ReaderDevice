//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	uart.c
//! @brief	UART Device Driver
//! 
//! $Id: uart.c 1755 2012-09-14 08:33:41Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi		initial release



//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include <STRING.H>
#include "PR9200.h"
#include "config.h"
#include "sio.h"
#include "rcp.h"
#include "uart.h"
#include "hal.h"
#include "rfidfsm.h"

//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 
#define uart0_set_port()	GPIO0->AFSEL |= 0x03
#define uart0_clr_port()	GPIO0->AFSEL &= ~(0x03)

//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!------------------------------------------------------------------- 
extern sio_byte_buf_type 	sio_rx_buf;
extern sio_byte_buf_type	sio_rx_buf_temp;
extern sio_pkt_queue_type 	sio_tx_buf_queue;
extern BOOL	bt_connection_state;
extern hal_gpio_in_type uart_check_in;

//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    
uint8	temp_uart = 0;
uint8	temp_len;

//!---------------------------------------------------------------
//!	@brief
//!		Write a character to the specified UART	
//!
//! @param
//!		*inbyte : data
//!		size : data length
//! @return
//!		None
//!
//!---------------------------------------------------------------
void uart_tx_pkt (uint8 *inbyte, uint16 size)
{
	SIO_SET_IRQ();	
	while(size--)
	{
		while( UART0->FR & TXFF );
		UART0->DR = (uint32) *inbyte++;
	}
	SIO_CLR_IRQ();
}

//!---------------------------------------------------------------
//!	@brief
//!		Set the baud rate on the specified UART
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------	// 
void uart_set_baudrate(int speed)
{
	uint32	 divider, remainder, fraction, temp;
	
	temp      = 16 * speed;
	divider   = UART_REF_CLK / temp;
	remainder = UART_REF_CLK % temp;
	temp      = (128 * remainder) / temp;
	fraction  = temp / 2;
	
	if (temp & 1)
		fraction++;

	UART0->IBRD = divider;
	UART0->FBRD	= fraction;

	// Must write LCRH after LCRM and/or LCRL.
	UART0->LCRH =UART0->LCRH;
}


//!---------------------------------------------------------------
//!	@brief
//!		Initiate the specified UART0
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void uart_init()
{
	sio_byte_buf_flush(&sio_rx_buf);
	sio_byte_buf_flush(&sio_rx_buf_temp);
	SIO_INIT_IRQ();
	SIO_CLR_IRQ();
	
	uart0_set_port();
	UART0->CR = 0; 	// Disable everything
	uart_set_baudrate(UART0_BAUDRATE);
	
	// Set the UART to be 8 bits, 1 stop bit, no parity 
	UART0->LCRH = WLEN_8BIT ; // fifo flush	
	UART0->IFLS = 0;
	UART0->CR = TXE | RXE | UARTEN;	// Enable the UART
	UART0->IMSC = INT_RX ; // rx interrupt enable
											 
	NVIC_EnableIRQ(UART0_RX_IRQn);	
}

extern BOOL bt_retry_result;
extern uint8 g_msg_len;
extern uint8 g_payload[BT_DATA_MAX_LEN + BT_HEADER_LEN + BT_CHECKSUM_LEN];
//extern uint8 *g_payload;
extern sio_interface_type	rcp_io;


//!---------------------------------------------------------------
//!	@brief
//!		UART0 Rx Inetrrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None   
//!
//!---------------------------------------------------------------
void UART0_RX_IRQHandler(void)		  
{


	uint16 inbyte;
		
	inbyte = UART0->DR;

	if ( sio_rx_buf.len  >= (SIO_BYTE_BUF_MAX - 1) ) 
	{
		sio_rx_buf.len = 0;
		sio_byte_buf_flush(&sio_rx_buf);
	}

	if(uart_check_in.in_cb(&uart_check_in)) // uart connect to bluetooth
	{
		if ( (sio_rx_buf.len == 0 ) && (inbyte != BT_PKT_PRAMBL) )
		{
			return;
		}

		sio_byte_buf_enqueue(&sio_rx_buf, inbyte);

		if (sio_rx_buf.len >= sio_rx_buf.data[2] + 4)
		{
			switch(sio_rx_buf.data[1])
			{
				case BT_MSG_EVENT:
					if(sio_rx_buf.data[3] == BT_EVENT_PAIRING)
					{
						if((sio_rx_buf.data[4] == BT_DISCONNECTION_COMPLETE) || (sio_rx_buf.data[4] == BT_LINK_LOSS))
						{
							bt_connection_state = FALSE;
							sio_post_received_data(&sio_rx_buf.data[0],sio_rx_buf.len);
							FSM_EVENT(EVENT_FSM_INVENTORY_DISCONTINUE); //read stop
						}
						else if (sio_rx_buf.data[4] == BT_CONNECTION_COMPLETE)
						{
							bt_connection_state = TRUE;
							sio_byte_buf_flush(&sio_rx_buf);
						}
						else
							sio_byte_buf_flush(&sio_rx_buf);
					}
					
					else if(sio_rx_buf.data[3] == BT_EVENT_DATA)
					{
						if(sio_rx_buf.data[4] == RCP_PKT_PRAMBL) 
						{
							if(sio_rx_buf.data[2] - 1 == sio_rx_buf.data[8] + RCP_HEADEND_CRC_LEN)
							{
								sio_post_received_data(&sio_rx_buf.data[4], sio_rx_buf.len - 5);
							}
							else
							{
								memcpy((void*) &sio_rx_buf_temp.data[0], (void*) &sio_rx_buf.data[4], sio_rx_buf.len - 5);
								temp_len = sio_rx_buf.len - 5;
								sio_byte_buf_flush(&sio_rx_buf);
							}
						}
						else
						{
							memcpy((void*) &sio_rx_buf_temp.data[temp_len], (void*) &sio_rx_buf.data[4], sio_rx_buf.len - 5);
							temp_len = temp_len + (sio_rx_buf.len - 5);
							sio_byte_buf_flush(&sio_rx_buf);
							if (sio_rx_buf_temp.data[4] + RCP_HEADEND_CRC_LEN == temp_len)
								sio_post_received_data(&sio_rx_buf_temp.data[0], sio_rx_buf_temp.data[4] + RCP_HEADEND_CRC_LEN);
							
						}
								
					}
					
					break;

				case BT_MSG_RSP:
		#if (0)
					sio_byte_buf_flush(&sio_rx_buf);
					sio_byte_buf_flush(&sio_rx_buf_temp);
		#else // check if ack is sucess or fail					
					if(sio_rx_buf.data[3] == BT_EVENT_PAIRING)
					{
						if(sio_rx_buf.data[4] == 0x0b) 
						{
							if(sio_rx_buf.data[5] == 0x01) //success
							{
								bt_retry_result = TRUE;
							}
							else if(sio_rx_buf.data[5] == 0x03) //fail
							{
								bt_retry_result = FALSE;
								
								rcp_io.send((uint8*) &g_payload, (uint8) g_msg_len + BT_HEADER_LEN + BT_CHECKSUM_LEN);
								delay_ms(10);				
							}
						}
					}
					
					sio_byte_buf_flush(&sio_rx_buf);
					sio_byte_buf_flush(&sio_rx_buf_temp);
		#endif			
					break;
				default:
					sio_byte_buf_flush(&sio_rx_buf);
					sio_byte_buf_flush(&sio_rx_buf_temp);
					break;
			}
		}
	}

	else //uart connect to usb host
	{
		if ( (sio_rx_buf.len == 0 ) && (inbyte != RCP_PKT_PRAMBL) )
		{
			return;
		}

		sio_byte_buf_enqueue(&sio_rx_buf, inbyte);

		if(sio_rx_buf.len >= (sio_rx_buf.data[4] + RCP_HEADEND_CRC_LEN))
		{
			if(	sio_rx_buf.data[sio_rx_buf.data[4] + RCP_PRAMBL_LEN + RCP_HEADER_LEN] == RCP_PKT_ENDMRK) 
			{
				sio_post_received_data(&sio_rx_buf.data[0],sio_rx_buf.len);					
			}
			else
			{					
				sio_byte_buf_flush(&sio_rx_buf);			
			}	   
		}  	//to hear: 110us
	}
} 		                                           

