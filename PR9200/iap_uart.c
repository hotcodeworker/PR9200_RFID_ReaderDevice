//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file		iap_uart.c
//! @brief	
//! 
//! $Id: iap_uart.c 1763 2012-09-24 06:56:31Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/08/31	sjpark	initial release

#include "PR9200.h"
#include "iap_uart.h"
#include "iap.h"

void iap_uart_init(void)
{	
//	uint32 divider, remainder, fraction;

 	//init Uart0 Pin
	GPIO0->AFSEL |= 0x03;

	//Clear Uart Control Register 
	UART0->CR = 0;
	
	//Clear Uart Line Control Register
	UART0->LCRH = 0;
	
	//Set BaudRate : 115200
//	divider   = 0x0A;
//	remainder = UART_CLK % (16 * 115200);
//	fraction  = 0x1A;
	
//	if (fraction & 1)
//		fraction++;
 
	UART0->IBRD = 0x0A;	 //	UART_CLK / (16 * 115200);
	UART0->FBRD = 0x1A;	 //((128 * remainder) / (16 * 115200))/2;
  
	//Set Line Control : enable fifo 8bit, stop 1bit, No parity 
	UART0->LCRH = FEN | WLEN_8BIT; 
	   
	//Enable UART for Transmit and Receive
    UART0->CR = TXE | RXE | UARTEN; 

	NVIC_DisableIRQ(UART0_RX_IRQn);

}


int iap_uart_get_char(void)
{
	//Check Rx FiFo
	if(UART0->FR & RXFE) // Rx Fifo Empty
    {
		return -1;
    }
	
	return (UART0->DR & 0xff);	
}


uint8 iap_uart_get_status(void)
{
	//Check Rx FiFo
	if(UART0->FR & RXFE) // Rx Fifo Empty	 
    {
		return 0;
    }
	else
	{
		return 1;
	}
}


void iap_uart_send_frame(uint8 cmd, uint8 *data, uint8 len,  uint8 h_crc, uint8 l_crc)
{	

	//Send Preamble
	while (UART0->FR & TXFF) ;
	UART0->DR = MSG_PREAMBLE;
	
	//Send Response Message Type
	while (UART0->FR & TXFF) ;
	UART0->DR = 1;
 
	//Send Response Command
	while (UART0->FR & TXFF) ;
	UART0->DR = cmd; 

	//Send Payload Length High byte
	while (UART0->FR & TXFF) ;
	UART0->DR = (uint8)(len >> 8);
 
	//iap_uart_put_char Payload Length Low byte
	while (UART0->FR & TXFF) ;
	UART0->DR = (uint8)len;
 

	//Send Payload
	while(len-- > 0x0)
	{
		while (UART0->FR & TXFF) ;
		UART0->DR = *data++;
	}
	
	//Send endmask
	while (UART0->FR & TXFF) ;
	UART0->DR = MSG_ENDMARK;
 
	//Send crc high byte
	while (UART0->FR & TXFF) ;
	UART0->DR = h_crc;
 
	// Send crc Low byte
	while (UART0->FR & TXFF) ;
	UART0->DR = l_crc;
 
}


