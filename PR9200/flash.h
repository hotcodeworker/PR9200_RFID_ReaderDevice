//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	flash.h
//! @brief	Function for erases, read, write to flash
//! 
//! $Id: flash.h 1735 2012-08-31 03:03:46Z sjpark $			
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release


#ifndef __EFLASH_H__
#define __EFLASH_H__



//!-------------------------------------------------------------------                 
//! Definition Declaration                                                       
//!-------------------------------------------------------------------  

//--------------------------------------------------------------------                
//   SYSTEM PERI MAP
//--------------------------------------------------------------------             

#define EFLASH_CTL_BASE			  	0x40020000

//--------------------------------------------------------------------                
//   MEMORY MAP
//--------------------------------------------------------------------                
#define EFLASH_BASE               	0x00000000
#define EFLASH_SIZE               	0x00010000
#define SRAM_BASE                 	0x10004000
#define SRAM_SIZE                 	0x00004000


#define FLASH_CFG_VALUE         	(0x5A5A5A5A)
#define FLASH_PAGE_SIZE         	(1024)	      		//Page Size is 1KByte
#define FLASH_LAST_PAGE_ADDRESS 	(0x0000fC00UL)     	//flash last page start address
#define FLASH_LAST_ADDRESS 			(0x00010000UL)     	//flash last page start address


//!-------------------------------------------------------------------                 
//! Macros
//!------------------------------------------------------------------- 
#define EFLS_CTL_CHIP_ERASE (0x1 << 6 | 0x1 << 3 | 0x1 << 0)
#define EFLS_CTL_PAGE_ERASE (0x1 << 6 | 0x1 << 1 | 0x1 << 3)
#define EFLS_CTL_WRITE_DATA (0x1 << 6 | 0x1 << 2 | 0x1 << 3)

#define EFLS_SET_CFG()       ((*(volatile unsigned int *)(EFLASH_CTL_BASE)) = (FLASH_CFG_VALUE))
#define EFLS_SET_ADDR(Addr)  ((*(volatile unsigned int *)(EFLASH_CTL_BASE + 0x04)) = (Addr))
#define EFLS_SET_DATA(Data)  ((*(volatile unsigned int *)(EFLASH_CTL_BASE + 0x08)) = (Data))
#define EFLS_SET_CTL(Ctl)    ((*(volatile unsigned int *)(EFLASH_CTL_BASE + 0x0C)) = (Ctl))
#define EFLS_CLR_TEST()    	 ((*(volatile unsigned int *)(EFLASH_CTL_BASE + 0x20)) = (0))
#define EFLASH_READ(Addr) 	 ((*(volatile unsigned int *)(Addr)))

//Flash Page Erase Macro
#define EFLSH_PAGE_ERASE(PageAddr) \
 	{ \
		EFLS_CLR_TEST(); \
		EFLS_SET_ADDR(PageAddr); \
    	EFLS_SET_CFG(); \
    	EFLS_SET_CTL(EFLS_CTL_PAGE_ERASE);\
	}


//Flash chip Erase Macro
#define EFLSH_CHIP_ERASE() \
 	{ \
		EFLS_CLR_TEST(); \
    	EFLS_SET_CFG(); \
    	EFLS_SET_CTL(EFLS_CTL_CHIP_ERASE);\
	}
	
		   
//Flash Write WORD Macro
#define EFLSH_WRITE_WORD(Addr, Data) \
	{ \
		EFLS_SET_ADDR(Addr); \
		EFLS_SET_DATA(Data); \
		EFLS_SET_CFG(); \
		EFLS_SET_CTL(EFLS_CTL_WRITE_DATA); \
	}     


#endif

