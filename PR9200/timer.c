//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	timer.c
//! @brief	TIMER Device Driver
//! 
//! $Id: timer.c 1735 2012-08-31 03:03:46Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2011/07/25	sjpark	initial release
//! 2012/05/24	jsyi	removed the fsm timer

//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "PR9200.h"
#include "rfidfsm.h"
#include "rcp.h"
#include "modem.h"

//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!------------------------------------------------------------------- 
extern	rfid_fsm_ctxt_type		rfid_fsm_ctxt;

//!-------------------------------------------------------------------                 
//! Globla Data Declaration
//!-------------------------------------------------------------------
//const TIMER_Type *TIMER[4] = {(TIMER_Type*) TIMER0_BASE, (TIMER_Type*) TIMER1_BASE, (TIMER_Type*) TIMER2_BASE, (TIMER_Type*) TIMER3_BASE};

timer_pf_type			timer0_cb = NULL;
timer_pf_type			timer2_cb = NULL;
timer_pf_type			timer3_cb = NULL;

//!---------------------------------------------------------------
//!	@brief
//!		Initialize timer (Timer0, Timer1, Timer2, Timer3)	
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void timer_init(void)
{
	//Init Tiemr 0, reserved for EVENT
	TIMER0->CONTROL = TIMER_SIZE_32BIT | ONESHOT_MODE | PRESCALE_DIV_256 | TIMER_INT_EN;	// 32bit, one-shot mode	

   	//Init Timer 1, reserved for internal delay
	TIMER1->CONTROL = TIMER_SIZE_32BIT | ONESHOT_MODE ;	// 32bit, one-shot mode
	
	//Init Timer 2, reserved for future use
	TIMER2->CONTROL = TIMER_SIZE_32BIT | ONESHOT_MODE ;	// 32bit, one-shot mode
	
	//Init Timer 3
	TIMER3->CONTROL = TIMER_SIZE_32BIT | PERIODIC_MODE | TIMER_INT_EN | PRESCALE_DIV_256;	// Periodic mode, intrrupt enable
}



//!---------------------------------------------------------------
//!	@brief
//!		Busy delay
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void delay_ms(uint32 ms)
{
  uint32 cur_tick;

  cur_tick = RFID_SYSTEM_TICK;
  while ((RFID_SYSTEM_TICK - cur_tick) < (ms*(SYSTEM_TICK_FREQ / 1000)) );
}


//!---------------------------------------------------------------
//!	@brief
//!		Timer0 Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void TIMER0_IRQHandler(void) //Timer0 ISR
{
	if(timer0_cb != NULL) 
		timer0_cb();
	TIMER0->INTCLR = 0xFFFFFFFFUL; // Interrupt Clear
}

BOOL timer0_register(timer_pf_type cb) //oneshot timer
{
	if(timer0_cb != NULL)
		return FALSE;

	NVIC_EnableIRQ(TIMER0_IRQn);
	
	timer0_cb = cb;
	timer0_start();

	return TRUE;
}


//!---------------------------------------------------------------
//!	@brief
//!		Timer3 Interrupt Handler	
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void TIMER3_IRQHandler(void)//Timer3 ISR
{
	if(timer3_cb!=NULL) timer3_cb();
	
	TIMER3->INTCLR = 0xFFFFFFFFUL;		// Interrupt Clear
}

void timer3_start()
{
	TIMER3->CONTROL |= TIMER_ENABLE;
}

void timer3_stop()
{
	TIMER3->CONTROL &= ~TIMER_ENABLE;
}

void timer3_load(uint32 val)
{
	TIMER3->LOAD = val;
}

BOOL timer3_register(hal_timer_type* timer) 
{
	if(timer3_cb != NULL)
		return FALSE;

	NVIC_EnableIRQ(TIMER3_IRQn);
		
	timer->start_cb = timer3_start;
	timer->stop_cb = timer3_stop;
	timer->load_cb = timer3_load;
  	timer3_cb = timer->irq_handler_cb;
	
	return TRUE;
}

BOOL timer3_deregister(void) 
{
	timer3_cb = NULL;

	return TRUE;
}

void timer_suspend_all(BOOL suspend)
{
	static uint32 	timer_reg_backup[4];
	static uint8 	suspended = FALSE;
		
	if(suspend && !suspended)
	{
		timer_reg_backup[0] = TIMER0->CONTROL;
		timer_reg_backup[1] = TIMER1->CONTROL;
		timer_reg_backup[2] = TIMER2->CONTROL;
		timer_reg_backup[3] = TIMER3->CONTROL;

		timer0_stop();
		timer1_stop();
		timer2_stop();
		timer3_stop();	

		suspended = TRUE;
	}
	else if(!suspend && suspended)
	{
		TIMER0->CONTROL = timer_reg_backup[0];
		TIMER1->CONTROL = timer_reg_backup[1];
		TIMER2->CONTROL = timer_reg_backup[2];
		TIMER3->CONTROL = timer_reg_backup[3];

		suspended = FALSE;
	}	
	
}


