//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//! 
//! @file	rfidfsm.c
//! @brief	simple finite state machine for LBT/FH scheduling
//! 
//! $Id: rfidfsm.c 1755 2012-09-14 08:33:41Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release

//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------  
#include "PR9200.h"
#include "rfidfsm.h"
#include "config.h"
#include "rcp.h"
#include "rf.h"				
#include "hal.h"
#include "utility.h"
#include "protocol.h"
#include "registry.h"
#include "modem.h"

//!-------------------------------------------------------------------
//! Definitions
//!-------------------------------------------------------------------
#define AP_FSM_LBT_TIME_OFFSET		(1)
#define AP_FSM_TX_ON_TIME_OFFSET	(1)
#define AP_FSM_LBT_POWER_OFFSET		(50) 	// +/- 0.5 dB

#define FSM_TRAN(_s) do {\
	EVENT _e;\
	fsm_event_next.handler = _s;\
	_e.handler = fsm_dispatch_event;\
	_e.param = EVENT_ENTRY;\
	event_post(_e);} while(0)

#define FSM_EVENT_DELAYED(_eparam, delay_ms) do {\
	EVENT _e;\
	_e.handler = fsm_dispatch_event;\
	_e.param = _eparam;\
	event_post_delayed(_e,delay_ms);} while(0)

#define FSM_EVENT_FLUSH() do {\
	event_flush(fsm_dispatch_event);\
} while(0)



//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!-------------------------------------------------------------------
extern rfid_inventory_prm_type rfid_inventory_prm;
extern iso180006c_tag_buffer_type tag_buf;
extern BOOL bt_connection_state;

//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------
rfid_fsm_ctxt_type	rfid_fsm_ctxt = {0,};
EVENT fsm_event_next = {fsm_state_active,EVENT_ENTRY};
#ifdef	__FEATURE_EXT_PA__
hal_gpio_out_type	external_pa_mode_sel_ctxt = {{HAL_GPIO_DEV_EXT_PA_ENABLE, 1, 3, HAL_GPIO_POL_NORMAL},
		NULL,NULL,NULL};
#endif
int16 rfid_fsm_inventory_count;
uint16 rfid_fsm_mtime_count;

//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  

//!---------------------------------------------------------------
//!	@brief
//!		Initialize the Finite State Machine
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void fsm_init()
{		
#ifdef	__FEATURE_EXT_PA__
	hal_gpio_register(&external_pa_mode_sel_ctxt);
	external_pa_mode_sel_ctxt.off_cb(&external_pa_mode_sel_ctxt);
#endif	
 	if(reg_sys_rst.reset)		// if system_reset is true
 	{ 				
//		reg_sys_rst.active.val = REG_INACTIVE;
	#ifdef	__AJT_DEBUG__
		debug_msg_str_2("system reset");
	#endif
		reg_sys_rst.reset = FALSE;
		bt_connection_state = TRUE;
		hal_update_registry();
		protocol_set_c_sel_param(&reg_sys_rst.sel);
		protocol_set_c_qry_param(&reg_sys_rst.qry);
		protocol_perform_inventory(&reg_sys_rst.param); 				   
 	}
}

#ifdef __DEBUG_RFIDFSM__
void fsm_debug_identify_event(const EVENT e)
{
	debug_msg_str("reg_fhlbt.prm.fh_enable = %d", reg_fhlbt.prm.fh_enable);
	switch(e.param)
	{
	case EVENT_NONE:
		debug_msg_str_fsm("EVENT_NONE");
		break;
	case EVENT_ENTRY:
		debug_msg_str_fsm("EVENT_ENTRY");		
		break;
	case EVENT_EXIT:
		debug_msg_str_fsm("EVENT_EXIT");		
		break;		
	case EVENT_DONE:
		debug_msg_str_fsm("EVENT_DONE");		
		break;
	case EVENT_FSM_IDLE:
		debug_msg_str_fsm("EVENT_FSM_IDLE");
		break;		
	case EVENT_FSM_INVENTORY_REQ:
		debug_msg_str_fsm("EVENT_FSM_INVENTORY_REQ");
		break;
	case EVENT_FSM_TX_OFF:
		debug_msg_str_fsm("EVENT_FSM_TX_OFF");		
		break;		
	case EVENT_FSM_TX_OFF_EXPIRED:
		debug_msg_str_fsm("EVENT_FSM_TX_OFF_EXPIRED");		
		break;
	case EVENT_FSM_LBT:
		debug_msg_str_fsm("EVENT_FSM_LBT");		
		break;		
	case EVENT_FSM_FH:
		debug_msg_str_fsm("EVENT_FSM_FH");
		break;		
	case EVENT_FSM_RAMP_UP:
		debug_msg_str_fsm("EVENT_FSM_RAMP_UP");		
		break;		
	case EVENT_FSM_TX_ON:
		debug_msg_str_fsm("EVENT_FSM_TX_ON");		
		break;		
	case EVENT_FSM_TX_ON_EXPIRED:
		debug_msg_str_fsm("EVENT_FSM_TX_ON_EXPIRED");		
		break;		
	case EVENT_FSM_MTIME_EXPIRED:
		debug_msg_str_fsm("EVENT_FSM_MTIME_EXPIRED");		
		break;		
	case EVENT_FSM_REPORT_TAGS:
		debug_msg_str_fsm("EVENT_FSM_REPORT_TAGS");		
		break;		
	case EVENT_FSM_RAMP_DN:
		debug_msg_str_fsm("EVENT_FSM_RAMP_DN");		
		break;		
	case EVENT_FSM_INVENTORY_COMPLETED:
		debug_msg_str_fsm("EVENT_FSM_INVENTORY_COMPLETED");		
		break;
	default:
		debug_msg_str_fsm("EVENT_[%d]", e.param);		
		break;
	}
}

void fsm_debug_identify_handler(void)
{
	if(fsm_event_next.handler == fsm_state_active)
	{
		debug_msg_str_fsm("fsm_state_active [%d]",event_debug_count);
	}
	else if(fsm_event_next.handler == fsm_state_tx_off_time)
	{
		debug_msg_str_fsm("fsm_state_tx_off_time [%d]",event_debug_count);		
	}
	else if(fsm_event_next.handler == fsm_state_tx_on_time)
	{
		debug_msg_str_fsm("fsm_state_tx_on_time [%d]",event_debug_count);		
	}
	else
	{
		debug_msg_str_fsm("?");		
	}

		
}
#endif

//!---------------------------------------------------------------
//!	@brief
//!		None
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------

rfid_fsm_status_type fsm_get_status(void)
{
	return rfid_fsm_ctxt.status;
}
	
void fsm_state_tx_on_time(EVENT e)
{	

	switch(e.param)
	{
	case EVENT_ENTRY:
		{
			if(reg_fhlbt.prm.fh_enable)
			{
				FSM_EVENT(EVENT_FSM_FH);
			}
			else if(reg_fhlbt.prm.lbt_enable)
			{
				FSM_EVENT(EVENT_FSM_LBT);
			}
			else
			{
				FSM_EVENT(EVENT_FSM_RAMP_UP);
			}
		}
		break;

	case EVENT_FSM_FH:
		{
			if( rfid_fsm_ctxt.inv_discontinue)
			{
				FSM_EVENT(EVENT_EXIT);				
			}
			else
			{
				rf_set_next_ch(); // Frequency Hopping
				if(reg_fhlbt.prm.lbt_enable)
				{
					FSM_EVENT(EVENT_FSM_LBT);
				}
				else
				{
					FSM_EVENT(EVENT_FSM_RAMP_UP);
				}				
			}

		}
		break;
		
	case EVENT_FSM_LBT:										   
		{	
			if( rfid_fsm_ctxt.inv_discontinue)
			{
				FSM_EVENT(EVENT_EXIT);				
			}
			else if(rfid_fsm_ctxt.lbt_expired)
			{
				rfid_fsm_ctxt.lbt_expired = FALSE;	
				event_timer_flush(fsm_state_tx_on_time);
				FSM_EVENT(EVENT_FSM_RAMP_UP);
			}
			else
			{					
				uint8 cw_detected;
				hal_get_cw_detected(&cw_detected);

				if(cw_detected)
				{
					event_timer_flush(fsm_state_tx_on_time);
					if(reg_fhlbt.prm.fh_enable)
					{
						FSM_EVENT(EVENT_FSM_FH);
					}
					else
					{
						FSM_EVENT(EVENT_FSM_LBT);
					}								  					
				}
				else
				{			   		
					EVENT e;
					e.handler = fsm_state_tx_on_time;
					e.param = EVENT_FSM_LBT_EXPIRED;
					
					event_post_delayed(e, 
						reg_fhlbt.prm.sense_time - AP_FSM_LBT_TIME_OFFSET);

					FSM_EVENT(EVENT_FSM_LBT);													
				}	
			}																					
		}	
		break;
			  			
	case EVENT_FSM_RAMP_UP:
		{	
			rfid_fsm_ctxt.tx_on_time_expired = FALSE;			
			FSM_EVENT_DELAYED(EVENT_FSM_TX_ON_EXPIRED,
				reg_fhlbt.prm.tx_on_time - AP_FSM_TX_ON_TIME_OFFSET);
			
			if(rfid_fsm_ctxt.inv_discontinue)
			{
				FSM_EVENT(EVENT_EXIT);
			}
			else
			{
				hal_set_cw(HAL_CW_ON);
				FSM_EVENT(EVENT_FSM_TX_ON);		
			}			
		}
		break;
	
	case EVENT_FSM_TX_ON:
		{
			if( rfid_fsm_ctxt.inv_discontinue
				|| !rfid_inventory_prm.count 
				|| rfid_fsm_ctxt.tx_on_time_expired)
			{
				FSM_EVENT(EVENT_EXIT);
			}
			else
			{
				int8 round_tag;
				
				if(reg_fhlbt.prm.cw_enable)
				{
					delay_ms(5);	
				}
				else
				{					
#ifndef __FEATURE_SIM_TAG__					
					switch(rfid_inventory_prm.cmd_sel)
					{
					case CMD_INVENTORY_SINGLE:
					case CMD_INVENTORY_MULTIPLE_CYCLE:
					case CMD_INVENTORY_MULTIPLE_CYCLETIMENUM:
						round_tag = iso180006c_select_inventory();						
					break;
					}				
#else
					round_tag = 1;
					iso180006c_push_simtag();
#endif
				}

				// for test (system_reset)
// 				if(round_tag)
// 				{
// 					rfid_fsm_inventory_count = 0;
// 				}
// 				else
// 				{	
// 					rfid_fsm_inventory_count++;
// 				}
				

				if(INVENTORY_COUNT_INFINITE != rfid_inventory_prm.count)
					rfid_inventory_prm.count--;

				if((INVENTORY_MTNU_INFINITE - 1) > rfid_fsm_ctxt.tag_detected  )
					rfid_fsm_ctxt.tag_detected += round_tag;
				
				if(INVENTORY_TAG_BUF_FULL == round_tag)
				{
					rfid_fsm_ctxt.tx_on_time_expired = TRUE;
				}
				else if( (INVENTORY_MTNU_INFINITE != rfid_inventory_prm.mtnu) &&
						( rfid_fsm_ctxt.tag_detected >= rfid_inventory_prm.mtnu))
				{			
					rfid_inventory_prm.count = 0;
				}

				// for test (system_reset)
// 				if(rfid_fsm_inventory_count > 100)
// 				{
// 					protocol_c_prm_sel_type *sel;
// 					protocol_c_prm_qry_type *qry;

// 					reg_sys_rst.active.val = REG_ACTIVE;
// 					reg_sys_rst.reset = TRUE;

// 					rfid_inventory_prm.mtime = rfid_fsm_mtime_count;
// 					reg_sys_rst.param = rfid_inventory_prm;

// 					protocol_get_c_sel_param(&sel);
// 					reg_sys_rst.sel = *sel;

// 					protocol_get_c_qry_param(&qry);
// 					reg_sys_rst.qry = *qry;

// 					hal_update_registry();
// 					hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);
// 					NVIC_SystemReset();

// 				}
				
				
				FSM_EVENT(EVENT_FSM_TX_ON);				
			}
		}
		break;

	case EVENT_FSM_LBT_EXPIRED:
		rfid_fsm_ctxt.lbt_expired = TRUE;
		break;

	case EVENT_FSM_INVENTORY_DISCONTINUE:
		rfid_fsm_ctxt.inv_discontinue = TRUE;
		break;
		
	case EVENT_FSM_TX_ON_EXPIRED:
		rfid_fsm_ctxt.tx_on_time_expired = TRUE;
		break;
		
	case EVENT_FSM_TX_OFF_EXPIRED:
		rfid_fsm_ctxt.tx_off_time_expired = TRUE;
		break;

	case EVENT_FSM_MTIME_EXPIRED:
		{
			rfid_fsm_mtime_count--;
			if(rfid_fsm_mtime_count == 0)
				rfid_inventory_prm.count = 0; 
			else
			{
				if(INVENTORY_MTIME_INFINITE != rfid_inventory_prm.mtime)
				FSM_EVENT_DELAYED(EVENT_FSM_MTIME_EXPIRED, (1000));
			}
		}
		break;
		
	case EVENT_EXIT:
	case EVENT_FSM_RAMP_DN:
		{
			hal_set_cw(HAL_CW_OFF);			
			hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);

			FSM_TRAN(fsm_state_tx_off_time);			
		}
		break;


	}


}



void fsm_state_tx_off_time(EVENT e)
{
	
	switch(e.param)
	{
	case EVENT_ENTRY:			
		if(rfid_fsm_ctxt.inv_discontinue)
		{
			FSM_EVENT(EVENT_EXIT);
		}
		else
		{
			rfid_fsm_ctxt.tx_off_time_expired = FALSE;	
			FSM_EVENT_DELAYED(EVENT_FSM_TX_OFF_EXPIRED, 
				(reg_fhlbt.prm.lbt_enable ? 
				(reg_fhlbt.prm.tx_off_time - reg_fhlbt.prm.sense_time) : 
				 reg_fhlbt.prm.tx_off_time  ) );
			
			FSM_EVENT(EVENT_FSM_REPORT_TAGS);
		}
		break;
		
	case EVENT_FSM_REPORT_TAGS:
		{
			if(rfid_fsm_ctxt.inv_discontinue)				
			{
				FSM_EVENT(EVENT_EXIT);	
			}			
			else if(tag_buf.count)
			{	
				uint8 i;
				rfid_pkt_rx_type *tag_ptr;
				tag_ptr = iso180006c_pop_tag();				
				
				for(i = 0; i < rfid_fsm_ctxt.tag_report_cb_count; i++)
				{
					if(NULL != rfid_fsm_ctxt.tag_report_cb[i])
					{
						(rfid_fsm_ctxt.tag_report_cb[i])(0, tag_ptr->byte_length, &tag_ptr->air_pkt[0]);
					}
				}			
				
				FSM_EVENT(EVENT_FSM_REPORT_TAGS);
			}
			else if(!rfid_inventory_prm.count )
			{
				FSM_EVENT(EVENT_EXIT);
			}
			else
			{
				FSM_EVENT(EVENT_FSM_TX_OFF);
			}
		}
		break;
		
	case EVENT_FSM_TX_OFF:
		if(rfid_fsm_ctxt.inv_discontinue
			|| !rfid_inventory_prm.count)
		{
			FSM_EVENT(EVENT_EXIT);
		}
		else if(rfid_fsm_ctxt.tx_off_time_expired)
		{
			FSM_TRAN(fsm_state_tx_on_time);
		}
		else
		{
			FSM_EVENT(EVENT_FSM_TX_OFF);
		}
		break;
		
	case EVENT_FSM_INVENTORY_DISCONTINUE:
		rfid_fsm_ctxt.inv_discontinue = TRUE;
		break;
		
	case EVENT_FSM_TX_ON_EXPIRED:
		rfid_fsm_ctxt.tx_on_time_expired = TRUE;
		break;
		
	case EVENT_FSM_TX_OFF_EXPIRED:
		rfid_fsm_ctxt.tx_off_time_expired = TRUE;
		break;

	case EVENT_FSM_MTIME_EXPIRED:
		rfid_fsm_mtime_count--;
		if(rfid_fsm_mtime_count == 0)
			rfid_inventory_prm.count = 0; 
		else
		{
			if(INVENTORY_MTIME_INFINITE != rfid_inventory_prm.mtime)
			FSM_EVENT_DELAYED(EVENT_FSM_MTIME_EXPIRED, (1000));
		}
		break;
		
	case EVENT_EXIT:
		{
			FSM_TRAN(fsm_state_active);
		}
		break;
	}
}

void fsm_state_active(EVENT e)
{	
	switch(e.param)
	{
	case EVENT_ENTRY:		
		if(rfid_fsm_ctxt.inv_discontinue || !rfid_inventory_prm.count)
		{
			FSM_EVENT(EVENT_FSM_INVENTORY_COMPLETED);
		}
		else
		{
			FSM_EVENT(EVENT_FSM_TX_ON);
		}
		break;		
	case EVENT_FSM_TX_ON:
		{
			rfid_fsm_ctxt.status = FSM_STATUS_BUSY;
			rfid_fsm_ctxt.tx_on_time_expired = FALSE;
			rfid_fsm_ctxt.tx_off_time_expired = FALSE;

			if(INVENTORY_MTIME_INFINITE != rfid_inventory_prm.mtime)
				FSM_EVENT_DELAYED(EVENT_FSM_MTIME_EXPIRED, (1000));
				rfid_fsm_mtime_count = rfid_inventory_prm.mtime;
			FSM_TRAN(fsm_state_tx_on_time);
		}
		break;
	case EVENT_FSM_INVENTORY_DISCONTINUE:
		{
			rfid_fsm_ctxt.inv_discontinue = TRUE;
		}
		//break;
	case EVENT_FSM_INVENTORY_COMPLETED:
		{
			uint8 i;
			
			for(i = 0; i < rfid_fsm_ctxt.tag_completed_cb_count; i++)
			{
				if(NULL != rfid_fsm_ctxt.tag_completed_cb[i])
				{
					(rfid_fsm_ctxt.tag_completed_cb[i])
						(rfid_fsm_ctxt.inv_discontinue, rfid_fsm_ctxt.tag_detected);
				}
			}

			FSM_EVENT(EVENT_EXIT);
		}
		break;
	case EVENT_EXIT:	
		{	
			FSM_EVENT_FLUSH();			
			rfid_fsm_ctxt.inv_discontinue = FALSE;
			rfid_fsm_ctxt.tag_detected = 0;
			rfid_fsm_ctxt.status = FSM_STATUS_AVAILABLE;
		}
		break;
	}
}


void fsm_dispatch_event(EVENT e)
{	
#ifdef __DEBUG_RFIDFSM__
	fsm_debug_identify_handler();
	fsm_debug_identify_event(e);
#endif
	fsm_event_next.handler(e);

}



