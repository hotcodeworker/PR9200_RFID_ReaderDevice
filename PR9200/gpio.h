//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	gpio.h
//! @brief	GPIO Device Driver
//! 
//! $Id: gpio.h 1605 2012-05-10 05:36:28Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release



#ifndef GPIO_H
#define GPIO_H


//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "config.h"
#include "commontypes.h"
#include "rfidtypes.h"



//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 


#define GPIO_DIR_IN			(0)
#define GPIO_DIR_OUT		(1)
#define GPIO_DIR_NA			(2)

#define GPIO_PIN_CNT		(13)

#define GPIO_IO_SEL0_PORT 	(1)
#define GPIO_IO_SEL0_BIT 	(5)
#define GPIO_IO_SEL1_PORT 	(1)
#define GPIO_IO_SEL1_BIT 	(6)

#define GPIO_SENSE_EDGE	(0)
#define GPIO_SENSE_LEVEL	(1)

#define GPIO_EDGE_SINGLE (0)
#define GPIO_EDGE_BOTH	(1)
#define GPIO_EDGE_NONE	(2)

#define GPIO_EVENT_LOW	(0)
#define GPIO_EVENT_HIGH	(1)
#define GPIO_EVENT_NONE	(2)

//!-------------------------------------------------------------------
//! Strunctures and enumerations
//!------------------------------------------------------------------- 



//!-------------------------------------------------------------------                 
//! Macros
//!------------------------------------------------------------------- 




//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------
void gpio_init(void);
void gpio_null(uint8 port, uint8 bit);
void gpio_test_proile_load(void);
void gpio_set_dir( uint8 port, uint8 bit, uint8 dir);
void gpio_set_bit(void *io);
void gpio_clr_bit(void *io);
void gpio_toggle_bit(void *io);
void gpio_register_dev(hal_gpio_id_type* dev);	
void gpio_enable_interrupt(void *io);
void gpio_disable_interrupt(void *io);
void gpio_clear_interrupt( uint8 port, uint8 bit);
uint8 gpio_get_bit(void *io);

#endif

