//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	spi.h
//! @brief	SPI Device Driver
//! 
//! $Id: spi.h 1671 2012-07-13 00:36:02Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi	initial release


#ifndef SPI_H
#define SPI_H



//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "config.h"
#include "commontypes.h"

//Control Register 0
//SPI phase and polarity
#define SSP_IDLE_LOW_VALID_RISING		(0x00 << 6)	 	// cpol = 0, cpha = 0
#define SSP_IDLE_HIGH_VALID_FALLING		(0x01 << 6)	 	// cpol = 1, cpha = 0
#define SSP_IDLE_LOW_VALID_FALLING		(0x02 << 6)	 	// cpol = 0, cpha = 1
#define	SSP_IDLE_HIGH_VALID_RISING		(0x03 << 6)		// cpol = 1, cpha = 1

#define DSS_8Bit	0x07
#define DSS_16Bit	0x0F

//Control Register 1
#define LSBF		0x00
#define MSBF		0x10
#define SLV			0x04	
#define MST			0x00
#define SSE		 	0x02

// Status Register
#define BSY			0x10
#define RFF			0x08
#define RNE			0x04
#define TNF			0x02
#define TFE			0x01

// Interrupt Mask set or clear
#define TXIM		0x08
#define RXIM		0x04
#define RTIM		0x02
#define RORIM		0x01

// Masked Interrupt Status Register
#define TXMIS		0x08
#define RXMIS		0x04
#define RTMIS		0x02
#define RORMIS		0x01

// Interrupt Clear Register
#define RTIC		0x01
#define RORIC		0x00

#if defined(__FEATURE_SPI_SLAVE_RCP__) || defined(__FEATURE_GPIO_SIO_SEL__) 	
void spi_slave_init(void);
#endif

#endif

