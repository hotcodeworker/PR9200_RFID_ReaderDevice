//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!	
//! @file	rcp.h
//! @brief	Packet Handler for RCP (Reader Control Protocol)
//!
//! $Id: rcp.h 1763 2012-09-24 06:56:31Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release
//! 2011/07/25	sjpark	modified for PR9200

#ifndef RCP_H
#define RCP_H


#include "config.h"
#include "commontypes.h"
#include "event.h"
#include "rfidtypes.h"

//!-------------------------------------------------------------------                 
//! Definition Declaration                                                       
//!-------------------------------------------------------------------  
#define RCP_PKT_MAX_BUF_SIZ 		(512)		//! RCP BUF Size
#define RCP_PKT_PRAMBL				(0xBB)
#define RCP_PKT_ENDMRK				(0x7E)
#define RCP_PRAMBL_LEN				(1)
#define RCP_HEADER_LEN				(4)
#define RCP_ENDMRK_LEN				(1)
#define RCP_HEADEND_LEN				(RCP_PRAMBL_LEN + RCP_HEADER_LEN + RCP_ENDMRK_LEN)
#ifdef	__FEATURE_ENABLE_RCP_CRC__	
#define RCP_CRC_LEN					(2)
#else
#define RCP_CRC_LEN					(0)
#endif
#define RCP_HEADEND_CRC_LEN			(RCP_HEADEND_LEN + RCP_CRC_LEN)

#define RCP_TAG_PC_LEN				(2)
#define RCP_TAG_CRC_LEN				(2)
#define RCP_TAG_RSSI_LEN			(4)

////////////////////////// RCP Message Type
#define RCP_MSG_CMD					0x00 // Command Type
#define RCP_MSG_RSP					0x01 // Response Type
#define RCP_MSG_NOTI				0x02 // Notification Type
#define RCP_MSG_TEST				0x03 // Test Type
#define RCP_MSG_CAP					0x04 // Cap Type

////////////////////////// RCP Command Type
#define RCP_CMD_CTL_RD_PWR			0x01 // Reader Power Control
#define RCP_CMD_NIMPLMNT_00			0x02 // Reader Connection Control
#define	RCP_CMD_GET_RD_INF			0x03 // Get Reader Information
#define RCP_CMD_NIMPLMNT_01			0x04 // Get Signal Strength
#define RCP_CMD_NIMPLMNT_02			0x05 // Set Signal Strength
#define RCP_CMD_GET_REGION			0x06 // Get Region
#define RCP_CMD_SET_REGION			0x07 // Set Resion
#define RCP_CMD_CTL_RESET			0x08 // Reset Reader
#define RCP_CMD_NIMPLMNT_03			0x09 // Get Type B A/I Parameters
#define RCP_CMD_NIMPLMNT_04			0x0A // Set Type B A/I Parameters
#define RCP_CMD_GET_C_SEL_PARM 		0x0B // Get Type C A/I Select Parameters
#define RCP_CMD_SET_C_SEL_PARM		0x0C // Set Type C A/I Select Parameters
#define RCP_CMD_GET_C_QRY_PARM		0x0D // Get Type C A/I Query Related Parameters
#define RCP_CMD_SET_C_QRY_PARM		0x0E // Set Type C A/I Query Related Parameters
#define RCP_CMD_GET_AT_RD_PARM 		0x0F // Get Automatic Read Parameters
#define RCP_CMD_SET_AT_RD_PARM 		0x10 // Set Automatic Read Parameters
#define RCP_CMD_GET_CH				0x11 // Set Frequency Channel
#define RCP_CMD_SET_CH				0x12 // Set Frequency Channel
#define RCP_CMD_GET_FH_LBT  		0x13
#define RCP_CMD_SET_FH_LBT  		0x14
#define RCP_CMD_GET_TX_PWR  		0x15		    
#define RCP_CMD_SET_TX_PWR  		0x16		    
#define RCP_CMD_CTL_CW			  	0x17
#define RCP_CMD_NIMPLMNT_07			0x18 // #define RCP_CMD_SET_SECURITY_KEY	0x18
#define RCP_CMD_NIMPLMNT_08			0x19 // #define RCP_CMD_SET_FREQ_MODE  		0x19
#define RCP_CMD_NIMPLMNT_09			0x1A // #define RCP_CMD_SET_MAC_CTRL  		0x1A
#define RCP_CMD_NIMPLMNT_10			0x1B 
#define RCP_ISO_RESERVED_00			0x1C
#define RCP_ISO_RESERVED_01			0x1D // #define RCP_CMD_SET_SINGLETONE		0x1D
#define RCP_ISO_RESERVED_02			0x1E
#define RCP_ISO_RESERVED_03			0x1F
#define RCP_UNDEF_00				0x20 // #define RCP_CMD_CLR_SINGLETONE		0x20

#define RCP_CMD_NIMPLMNT_11			0x21 // #define RCP_CMD_READ_B_UII			0x21 // Read Type B UII
#define RCP_CMD_READ_C_UII			0x22 // Read Type C UII
#define RCP_CMD_NIMPLMNT_23			0x23
#define RCP_CMD_READ_C_USER_DT		0x24
#define RCP_CMD_NIMPLMNT_25			0x25
#define RCP_CMD_NIMPLMNT_26			0x26
#define RCP_CMD_STRT_AUTO_READ 		0x27 // Start Automatic Read
#define RCP_CMD_STOP_AUTO_READ 		0x28 // Stop Automatic Read

#define RCP_CMD_READ_C_DT			0x29 // Read Type C Tag Data // RCP_VENDOR_SPECIFIC_07
#define RCP_VENDOR_SPECIFIC_08  	0x2A
#define RCP_VENDOR_SPECIFIC_09  	0x2B
#define RCP_VENDOR_SPECIFIC_10  	0x2C
#define RCP_VENDOR_SPECIFIC_11  	0x2D
#define RCP_VENDOR_SPECIFIC_12  	0x2E
#define RCP_VENDOR_SPECIFIC_13  	0x2F
#define RCP_CMD_GET_HOPPING_TBL  	0x30
#define RCP_CMD_SET_HOPPING_TBL	  	0x31
#define RCP_CMD_GET_MODULATION  	0x32
#define RCP_CMD_SET_MODULATION  	0x33
#define RCP_CMD_GET_ANTICOL_MODE  	0x34
#define RCP_CMD_SET_ANTICOL_MODE  	0x35
#define RCP_CMD_STRT_AUTO_READ_EX	0x36 // Start Automatic Read2
#define RCP_CMD_STOP_AUTO_READ_EX	0x37 // Stop Automatic Read

#define RCP_CMD_NIMPLMNT_26_1		0x38
#define RCP_CMD_NIMPLMNT_26_2		0x39
#define RCP_CMD_NIMPLMNT_27			0x3A
#define RCP_CMD_NIMPLMNT_28			0x3B
#define RCP_CMD_NIMPLMNT_29			0x3C

#define RCP_ISO_RESERVED_04			0x3D
#define RCP_ISO_RESERVED_05			0x3E
#define RCP_ISO_RESERVED_06			0x3F

#define RCP_CMD_NIMPLMNT_30			0x40
#define RCP_CMD_NIMPLMNT_31			0x41
#define RCP_CMD_NIMPLMNT_32			0x42
#define RCP_CMD_NIMPLMNT_33			0x43
#define RCP_CMD_NIMPLMNT_34			0x44
#define RCP_CMD_NIMPLMNT_35			0x45

#define RCP_CMD_WRITE_C_DT  		0x46
#define RCP_CMD_BLOCKWRITE_C_DT  	0x47
#define RCP_CMD_BLOCKERASE_C_DT		0x48 

#define RCP_VENDOR_SPECIFIC_25		0x49 // Write Type C Tag Data
#define RCP_VENDOR_SPECIFIC_26  	0x4A
#define RCP_VENDOR_SPECIFIC_27  	0x4B
#define RCP_VENDOR_SPECIFIC_28  	0x4C
#define RCP_VENDOR_SPECIFIC_29  	0x4D
#define RCP_VENDOR_SPECIFIC_30  	0x4E
#define RCP_VENDOR_SPECIFIC_31  	0x4F
#define RCP_VENDOR_SPECIFIC_32		0x50 // BlockWrite
#define RCP_VENDOR_SPECIFIC_33		0x51 // BlockErase

// NXP
#define RCP_CMD_NXP_READPROTECT  		0x52
#define RCP_CMD_NXP_RESET_READPROTECT  	0x53
#define RCP_CMD_NXP_CHANGE_EAS  		0x54
#define RCP_CMD_NXP_EAS_ALARM  			0x55
#define RCP_CMD_NXP_CALIBRATE	  		0x56

#define RCP_PR9200_ISP_DATA		0x57 // HEX transfer
//#define RCP_PR9200_ISP_RESERVED_00		0x57 // HEX transfer

#define RCP_ISO_RESERVED_07			0x58
#define RCP_ISO_RESERVED_08			0x59
#define RCP_ISO_RESERVED_09			0x5A
#define RCP_ISO_RESERVED_10			0x5B
#define RCP_ISO_RESERVED_11			0x5C
#define RCP_ISO_RESERVED_12			0x5D
#define RCP_ISO_RESERVED_13			0x5E
#define RCP_ISO_RESERVED_14			0x5F

#define RCP_UNDEF_01				0x60 
#define RCP_CMD_NIMPLMNT_36			0x61
#define RCP_VENDOR_SPECIFIC_40  	0x62
#define RCP_VENDOR_SPECIFIC_41  	0x63
#define RCP_VENDOR_SPECIFIC_42  	0x64
#define RCP_CMD_KILL_RECOM_C		0x65 // Kill/Recom Type C Tag 
#define RCP_VENDOR_SPECIFIC_44  	0x66
#define RCP_VENDOR_SPECIFIC_45		0x67
#define RCP_VENDOR_SPECIFIC_46		0x68
#define RCP_VENDOR_SPECIFIC_47		0x69
#define RCP_VENDOR_SPECIFIC_48		0x6A
#define RCP_VENDOR_SPECIFIC_49		0x6B
#define RCP_CMD_CUSTOM_00 			0x6C // RCP_VENDOR_SPECIFIC_50
#define RCP_CMD_CUSTOM_01			0x6D // RCP_VENDOR_SPECIFIC_51
#define RCP_CMD_CUSTOM_02			0x6E // RCP_VENDOR_SPECIFIC_52
#define RCP_CMD_CUSTOM_03			0x6F // RCP_VENDOR_SPECIFIC_53
#define RCP_CMD_CUSTOM_04			0x70 // RCP_VENDOR_SPECIFIC_54
#define RCP_CMD_CUSTOM_05			0x71 // RCP_VENDOR_SPECIFIC_55
#define RCP_CMD_CUSTOM_06			0x72 // RCP_VENDOR_SPECIFIC_56
#define RCP_CMD_CUSTOM_07			0x73 // RCP_VENDOR_SPECIFIC_57
#define RCP_CMD_CUSTOM_08			0x74 // RCP_VENDOR_SPECIFIC_58
#define RCP_CMD_CUSTOM_09			0x75 // RCP_VENDOR_SPECIFIC_59		
#define RCP_CMD_CUSTOM_10			0x76 // RCP_VENDOR_SPECIFIC_60		
#define RCP_CMD_CUSTOM_11			0x77 // RCP_VENDOR_SPECIFIC_61		

#define RCP_ISO_RESERVED_15			0x78
#define RCP_ISO_RESERVED_16			0x79
#define RCP_ISO_RESERVED_17			0x7A
#define RCP_ISO_RESERVED_18			0x7B
#define RCP_ISO_RESERVED_19			0x7C
#define RCP_ISO_RESERVED_20			0x7D
#define RCP_ISO_RESERVED_21			0x7E
#define RCP_ISO_RESERVED_22			0x7F

#define RCP_UNDEF_02				0x80 
#define RCP_CMD_NIMPLMNT_37			0x81 // Lock Type B Tag
#define RCP_CMD_LOCK_C		 		0x82 // Lock Type C Tag

#define RCP_CMD_PERMALOCK_C			0x83
#define RCP_VENDOR_SPECIFIC_63		0x84
#define RCP_VENDOR_SPECIFIC_64		0x85
#define RCP_VENDOR_SPECIFIC_65		0x86
#define RCP_VENDOR_SPECIFIC_66		0x87
#define RCP_VENDOR_SPECIFIC_67		0x88
#define RCP_VENDOR_SPECIFIC_68		0x89
#define RCP_VENDOR_SPECIFIC_69		0x8A
#define RCP_VENDOR_SPECIFIC_70		0x8B
#define RCP_VENDOR_SPECIFIC_71		0x8C
#define RCP_VENDOR_SPECIFIC_72		0x8D
#define RCP_VENDOR_SPECIFIC_73		0x8E
#define RCP_VENDOR_SPECIFIC_74		0x8F
#define RCP_VENDOR_SPECIFIC_75		0x90
#define RCP_VENDOR_SPECIFIC_76		0x91
#define RCP_VENDOR_SPECIFIC_77		0x92
#define RCP_VENDOR_SPECIFIC_78		0x93
#define RCP_VENDOR_SPECIFIC_79		0x94
#define RCP_VENDOR_SPECIFIC_80		0x95
#define RCP_VENDOR_SPECIFIC_81		0x96

#define RCP_ISO_RESERVED_23			0x97
#define RCP_ISO_RESERVED_24			0x98
#define RCP_ISO_RESERVED_25			0x99
#define RCP_ISO_RESERVED_26			0x9A
#define RCP_ISO_RESERVED_27			0x9B
#define RCP_ISO_RESERVED_28			0x9C
#define RCP_ISO_RESERVED_29			0x9D
#define RCP_ISO_RESERVED_30			0x9E
#define RCP_ISO_RESERVED_31			0x9F

#define RCP_UNDEF_03				0xA0 

#define RCP_CMD_NIMPLMNT_38			0xA1 // #define RCP_CMD_GET_LAST_RST		0xA1 // Get Last Result
#define RCP_CMD_STRT_TEST_MOD		0xA2 // Start Test Mode
#define RCP_CMD_STOP_TEST_MOD		0xA3 // Stop Test Mode
#define RCP_CMD_STRT_RX_TEST		0xA4 // Start Receive Test
#define RCP_CMD_STOP_RX_TEST		0xA5 // Stop Receive Test

// Vendor Specific Section
#define RCP_CMD_SET_MODEM_REG		0xA6 // Set Modem Register 	// RCP_VENDOR_SPECIFIC_82
#define RCP_CMD_SET_RF_REG			0xA7 // Set RF Register		// RCP_VENDOR_SPECIFIC_83
#define RCP_CMD_GET_MODEM_REG		0xA8 // Get Modem Register	// RCP_VENDOR_SPECIFIC_84
#define RCP_CMD_GET_RF_REG			0xA9 // Get RF Register		// RCP_VENDOR_SPECIFIC_85

#define RCP_CMD_BAT_CHK				0xAA //#define RCP_VENDOR_SPECIFIC_86		0xAA
#define RCP_CMD_PWR_OFF				0xAB //#define RCP_VENDOR_SPECIFIC_87		0xAB
#define RCP_DEEP_SLEEP				0xAC //#define RCP_VENDOR_SPECIFIC_88		0xAC
#define RCP_CMD_SET_SERIAL_NO		0xAD //#define RCP_VENDOR_SPECIFIC_89		0xAD
#define RCP_CMD_GET_POWER_OFF_TIME	0xAE //#define RCP_VENDOR_SPECIFIC_90 		0xAE
#define RCP_CMD_SET_POWER_OFF_TIME 	0xAF //#define RCP_VENDOR_SPECIFIC_91 		0xAF

// Tx
#define RCP_PR9200_ISP_DOWNLOAD	0xB1 // Set Download Mode		// RCP_VENDOR_SPECIFIC_93
//#define RCP_PR9200_ISP_RESERVED_01	0xB1 // Set Download Mode		// RCP_VENDOR_SPECIFIC_93
#define RCP_CMD_SET_TX_MODULE_PWR	0xB2							// RCP_VENDOR_SPECIFIC_94
#define RCP_PR9200_ISP_RESERVED_02	0xB3 // Get Dump Data			// RCP_VENDOR_SPECIFIC_95

#define RCP_CMD_SET_ANT_PATH		0xB4 // RCP_VENDOR_SPECIFIC_96
#define RCP_VENDOR_SPECIFIC_97		0xB5 
#define RCP_VENDOR_SPECIFIC_98		0xB6
#define RCP_GET_TEMPERATURE			0xB7

// Reserved
#define RCP_CMD_SET_TX_OFFSET		0xB8
#define RCP_CMD_SET_TX_PWR_CL		0xBA

#define RCP_CMD_SET_MOD_DEPTH		0xBC
#define RCP_CMD_SET_MODULE_TABLE	0xBD
#define RCP_CMD_SET_DELI			0xBE

// Rx

#define RCP_CMD_GET_RX_GAIN			0xC0 // Get Rx Gain
#define RCP_CMD_SET_RX_GAIN			0xC1 // Set Rx Gain
#define RCP_CMD_GET_RSSI			0xC5 // Get RSSI
#define RCP_CMD_SCAN_RSSI			0xC6 // Scan RSSI

// INT PARAM
#define RCP_UPDATE_FLASH			0xD2
#define RCP_ERASE_FLASH				0xD3
#define RCP_GET_FLASH_ITEM			0xD4
#define RCP_SET_FLASH_ITEM			0xD5
#define RCP_GET_GPIO				0xD6
#define RCP_SET_GPIO				0xD7

#define RCP_MSD_DEBUG				0xDF

#define RCP_CMD_TEST_CMD			0xE0 // Test Command Ack



// USER
#define BT_PKT_PRAMBL				0x77

// BT Packet Type
#define BT_MSG_CMD					0x01
#define BT_MSG_RESERVED			0x02
#define BT_MSG_RSP					0x03
#define BT_MSG_EVENT				0x04

// BT Event OpCode
#define BT_EVENT_PAIRING		0x02
#define BT_EVENT_DATA				0x06

// BT Command OpCode
#define BT_CMD_SEND					0x0B

// BT Connection Status
#define BT_CONNECTION_COMPLETE		0x01
#define BT_DISCONNECTION_COMPLETE	0x02
#define BT_LINK_LOSS				0x03

// BT Packet Length
#define BT_HEADER_LEN				(4)
#define BT_OPCODE_LEN				(1)
#define BT_CHECKSUM_LEN			(1)
#define BT_DATA_MAX_LEN			(20)


////////////////////////// Test Command
#define RCP_TEST_MODEM_RX			0x01 // Test Modem Rx
#define RCP_TEST_SEL_REP			0x02 // Test Select Repeat
#define RCP_TEST_QRY_REP			0x04 // Test Select Query Repeat
#define RCP_TEST_QRYREP_REP			0x05 // Test Select QueryReq Repeat
#define RCP_TEST_PKT_REPEAT			0x06 // Test Select Repeat

#define RCP_TEST_PKT_CAPTURE		0x10

#define RCP_TEST_PKT_CAP_ACK		0x11
#define RCP_TEST_PKT_CAP_UII		0x12

#define RCP_TEST_EPC_QRY			0x20
#define RCP_TEST_EPC_SELMASK		0x21
#define RCP_TEST_EPC_SELQRY			0x22
#define RCP_TEST_EPC_QRYACK			0x23
#define RCP_TEST_EPC_QRYQRYREP		0x24



#define RCP_SFR_LDOCFG				0x84 // SFR Reg MAP
#define RCP_SFR_CKSEL				0x86 // SFR Reg MAP

#define	ARG_SUCCESS					0x00
#define MSG_FAILURE					0xFF

#define RF_TX_CW_ON 				0xFF
#define RF_TX_CW_OFF				0x00

#define RDR_CONNECT 				0xFF
#define RDR_DISCONNECT				0x00

#define RDR_INFO_ISO_6B				0x01
#define RDR_INFO_ISO_6C				0x02
#define RDR_INFO_ISO_6BC			0x03

#define RDR_MODEL					0x00
#define RDR_SN     					0x01
#define RDR_MANFACT 				0x02
#define RDR_FREQ					0x03
#define RDR_TAG_TYPE				0x04

// Custom Info
#define RDR_FW_TIME					0xA0
#define RDR_CHIP_VER				0xA1

#define TAG_ISO18000_6C				0x02

#define REGION_KOREA				0x01
#define REGION_USA  				0x02
#define REGION_EUROPE				0x04
#define REGION_JAPAN				0x08
#define REGION_CHINA2				0x10
#define REGION_CHINA1				0x16
//#define REGION_CHINA2				0x32


////////////////////////// RSP Param

#define RCP_RSP_CPLT_AUTO_READ		0x1F // Complete Auto Read

// RSP NACK Param
#define FAIL_NONE					0x00
#define FAIL_RDR_PWR_CTRL			0x01
#define FAIL_RDR_CON_CTRL			0x02
#define FAIL_RDR_GET_INFO			0x03
#define FAIL_GET_SIG_STREN			0x04
#define FAIL_SET_SIG_STREN			0x05
#define FAIL_RDR_FILT_CTRLN			0x06
#define FAIL_GET_REGION				0x07
#define FAIL_SET_REGION				0x08
#define FAIL_READ_TAG_MEM			0x09

#define FAIL_READ_AUTO				0x0A
#define FAIL_READ_AUTO_OP			0x0B // Automatic Read In Operation
#define FAIL_STOP_AUTO				0x0C // Cannot Stop Automatic Read
#define FAIL_NO_AUTO_MODE			0x0D // Not in Automatic Mode
#define FAIL_INVALID_PARM			0x0E

#define FAIL_WRITE_TAG				0x10
#define FAIL_ERASE_TAG				0x11
#define FAIL_KILL_TAG				0x12
#define FAIL_CTRL_LOCK				0x13
#define FAIL_GET_LAST_RST			0x14
#define FAIL_NO_TAG_DETECTED		0x15
#define FAIL_PASSWORD				0x16
#define FAIL_NOT_SUPRT_CMD			0x17 // Not Support CMD
#define FAIL_UNDEF_CMD				0x18 // Not Support CMD
#define FAIL_RDR_RESET				0x19
#define FAIL_CTRL_C_PARAM			0x1B
#define FAIL_NO_USER_DATA			0x1C
#define FAIL_CTRL_TEST				0x1E

// Vendor Specific Section
#define FAIL_VENDOR_SPECIFIC_TOP	0x30
#define FAIL_VENDOR_SPECIFIC_BOT	0xDF

#define FAIL_RESERVED_TOP			0xE0
#define FAIL_RESERVED_BOT			0xFE

#define	FAIL_CRC_ERROR				0xFF
#define FAIL_CSTM_CMD				0xFF
#define FAIL_UKNOWN_ERROR			0x99

#define FAIL_MEM_OVERRUN			0x20
#define FAIL_MEM_LOCKED				0x21
#define FAIL_TAG_LOW_POWER			0x22
#define FAIL_TAG_KNOWN				0x23

//!-------------------------------------------------------------------                 
//! Strunctures and enumerations
//!-------------------------------------------------------------------
typedef enum
{
	AUTO_TYPE1 = 1,
	AUTO_TYPE2 = 2,
	AUTO_TYPE3 = 3
} autoread_type;

typedef	struct
{
	uint8	preamble;
	uint8	msg_type;
	uint8	cmd_code;
	uint8	pl_length[2];
	uint8   payload[RCP_PKT_MAX_BUF_SIZ];
} rcp_pkt_type;

typedef union
{
	rcp_pkt_type pkt;
} rcp_rsp_type;

typedef rcp_rsp_type rcp_req_type;
typedef void(*rcp_cb_prm_type)(void *p);


//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------
void rcp_init(void); // RCP Packet Parsing
void rcp_register_req_cb(rcp_cb_prm_type cb);
void rcp_dispatch_event(EVENT e);
void rcp_clear_req_buf(void);
void rcp_write_rsp(void);
void rcp_read_req(uint8* data, uint16 len);
void rcp_write_msg_nack(const uint8 msg_type, const uint8 fail_code);
void rcp_write_msg_ack(const uint8 msg_type, const uint8 cmd_code);
void rcp_write_string(const uint8 msg_type, const uint8 cmd_code, const uint8 *payload, const uint16 payload_size);

void rcp_report_tags(uint8 rcp_msg_type, uint8 len, uint8* data);
void rcp_complete_inventory(uint8 discontinued, uint8 detected);

void rcp_set_pd(void);
void rcp_get_reader_info(void); // Get Reader Information
void rcp_set_system_reset(void);

void rcp_get_tx_pwr_lvl(void);
void rcp_set_tx_pwr_lvl(void);

void rcp_set_rf_cw(void); 
void rcp_get_rf_freq_hopping_tbl(void);
void rcp_set_rf_freq_hopping_tbl(void);

void rcp_get_rf_ch(void);
void rcp_set_rf_ch(void);
void rcp_get_region(void); // Get Region
void rcp_set_region(void); // Set Resion
void rcp_get_modulation(void);
void rcp_set_modulation(void);

void rcp_get_fh_lbt_param(void);	
void rcp_set_fh_lbt_param(void);
void rcp_get_rssi(void);

void rcp_get_c_sel_parm(void); // Get Type C A/I Select Parameters
void rcp_set_c_sel_parm(void); // Set Type C A/I Select Parameters
void rcp_get_c_qry_parm(void); // Get Type C A/I Query Related Parameters
void rcp_set_c_qry_parm(void); // Set Type C A/I Query Related Parameters

void rcp_get_auto_read_parm(void); // Get Automatic Read Parameters
void rcp_set_auto_read_parm(void); // Set Automatic Read Parameters
void rcp_get_anticol_mode(void);
void rcp_set_anticol_mode(void); 

void rcp_read_c_uii(void); // Read Type C UII
void rcp_read_or_block_erase_data(void); // Read Type C User Data
void rcp_start_auto_read(void); // Start Automatic Read
void rcp_stop_auto_read(void); // Stop Automatic Read
void rcp_start_auto_read2(void);
void rcp_write_c_data(void); // Write Type C Data
void rcp_kill_c_tag(void); // Kill Type C Tag
void rcp_lock_c_tag(void); // Lock Type C Tag

void rcp_get_temperature(void);

void rcp_get_registry(void);
void rcp_erase_registry(void);
void rcp_update_registry(void);

void rcp_start_downlaod(void);
void rcp_get_bat_status(void);
void rcp_pwr_off(void);
void rcp_set_serial_no(void);
void rcp_bt_discovery(void);
void rcp_get_pwr_off_time(void);
void rcp_set_pwr_off_time(void);


#ifdef __FEATURE_NXP_UCODE__
void rcp_readprotect(void);
void rcp_reset_readprotect(void);
void rcp_change_eas(void);
void rcp_eas_alarm(void);
void rcp_calibrate(void);
#endif


#endif
