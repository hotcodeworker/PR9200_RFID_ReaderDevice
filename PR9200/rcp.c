//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//! 
//! @file	rcp.c
//! @brief	Packet Handler for RCP (Reader Control Protocol)
//! 
//! $Id: rcp.c 1761 2012-09-21 08:12:40Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release
//! 2011/07/25	sjpark	modified for PR9200
//! 2012/03/23	jsyi	revised

//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------  
#include <STRING.H>
#include "config.h"
#include "PR9200.h"
#include "sio.h"
#include "rfidfsm.h"
#include "crc.h"
#include "sio.h"
#include "hal.h"
#include "rfidtypes.h"
#include "protocol.h"
#include "rcp.h"
#include "custom.h"

//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 
extern iso180006c_tag_buffer_type tag_buf;
extern rfid_fsm_ctxt_type rfid_fsm_ctxt;
extern rfid_inventory_prm_type rfid_inventory_prm;
extern int16 rfid_fsm_inventory_count;
extern uint32 pwr_off_time;
extern BOOL	bt_connection_state;

//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    
rcp_req_type 		rcp_req_pkt_c = {0,};
rcp_rsp_type 		rcp_rsp_pkt_c = {0,};
sio_interface_type	rcp_io = {NULL,};

rcp_cb_prm_type		rcp_req_cb = NULL;

#ifdef	__FEATURE_EXT_PA__
hal_gpio_out_type	external_pa_mode_sel_ctxt_cw = {{HAL_GPIO_DEV_EXT_PA_ENABLE, 1, 3, HAL_GPIO_POL_NORMAL},
		NULL,NULL,NULL};
#endif
hal_gpio_in_type bat_status_1_in = {{HAL_GPIO_DEV_IN, 0, 4, HAL_GPIO_POL_NORMAL}, NULL};
hal_gpio_in_type bat_status_2_in = {{HAL_GPIO_DEV_IN, 1, 4, HAL_GPIO_POL_NORMAL}, NULL};
hal_gpio_in_type uart_check_in = {{HAL_GPIO_DEV_IN, 0, 3, HAL_GPIO_POL_NORMAL}, NULL};

//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  

//!---------------------------------------------------------------
//!	@brief
//!		Initialize RCP paramerters
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_init()
{	
	rcp_io.receive_cb = rcp_read_req;
	sio_register_io(&rcp_io);
	rcp_io.init();
	
	protocol_register_tag_report_cb(rcp_report_tags);
	protocol_register_tag_completed_cb(rcp_complete_inventory);

#ifdef	__FEATURE_EXT_PA__
	hal_gpio_register(&external_pa_mode_sel_ctxt_cw);	
#endif	
	hal_gpio_register(&bat_status_1_in);
	hal_gpio_register(&bat_status_2_in);
	hal_gpio_register(&uart_check_in);
}

//!---------------------------------------------------------------
//!	@brief
//!		Parses the RCP Packet
//!
//! @param
//!		EVENT e
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_dispatch_event(EVENT e)
{

#ifdef __FEATURE_ENABLE_RCP_CRC__
	if (rcp_req_pkt_c.pkt.preamble == BT_PKT_PRAMBL)
	{
		rcp_bt_discovery();
		rcp_clear_req_buf();
		return;
	}
	else if( crc_verify( &(((uint8*) &rcp_req_pkt_c)[1]), rcp_req_pkt_c.pkt.pl_length[1]+RCP_HEADEND_CRC_LEN-1) )
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_CRC_ERROR);
		return;
	} 
#endif	

	bt_connection_state = TRUE;

	switch(rcp_req_pkt_c.pkt.cmd_code)
	{
		case RCP_CMD_CTL_RD_PWR:
			rcp_set_pd();
			break;			
		case RCP_CMD_GET_RD_INF:
			rcp_get_reader_info();
			break;
			
		case RCP_CMD_CTL_RESET:
			rcp_set_system_reset();
			break;		

		case RCP_CMD_GET_REGION:
			rcp_get_region();
			break;
	    case RCP_CMD_SET_REGION:
			rcp_set_region();
			break;

		case RCP_CMD_GET_CH:
			rcp_get_rf_ch();
			break;	
		case RCP_CMD_SET_CH:
			rcp_set_rf_ch();			
			break;
		case RCP_CMD_GET_TX_PWR:
			rcp_get_tx_pwr_lvl();
			break;			
		case RCP_CMD_SET_TX_PWR:
			rcp_set_tx_pwr_lvl();
			break;
		case RCP_CMD_GET_FH_LBT:
			rcp_get_fh_lbt_param();	
			break;
		case RCP_CMD_SET_FH_LBT:
			rcp_set_fh_lbt_param();
			break;

			
			
		case RCP_CMD_GET_C_SEL_PARM:
			rcp_get_c_sel_parm();
			break;
		case RCP_CMD_SET_C_SEL_PARM:	
			rcp_set_c_sel_parm();
			break;
		case RCP_CMD_GET_C_QRY_PARM:
			rcp_get_c_qry_parm();
			break;
		case RCP_CMD_SET_C_QRY_PARM:
			rcp_set_c_qry_parm();
			break;

		case RCP_CMD_STRT_AUTO_READ:
			rcp_start_auto_read();
			break;
		case RCP_CMD_STOP_AUTO_READ:
		case RCP_CMD_STOP_AUTO_READ_EX:
			rcp_stop_auto_read();
			break;
		case RCP_CMD_STRT_AUTO_READ_EX:
			rcp_start_auto_read2();
			break;

		case RCP_CMD_GET_HOPPING_TBL:
			rcp_get_rf_freq_hopping_tbl();
			break;
		case RCP_CMD_SET_HOPPING_TBL:
			rcp_set_rf_freq_hopping_tbl();
			break;	
			
		case RCP_CMD_GET_MODULATION:
			rcp_get_modulation();
			break;			
		case RCP_CMD_SET_MODULATION:
			rcp_set_modulation();
			break;
			
		case RCP_CMD_GET_ANTICOL_MODE:
			rcp_get_anticol_mode();
			break;
		case RCP_CMD_SET_ANTICOL_MODE:
			rcp_set_anticol_mode();
			break;
			
		case RCP_CMD_READ_C_UII:
			rcp_read_c_uii();
			break;		

		case RCP_CMD_READ_C_DT:
		case RCP_CMD_BLOCKERASE_C_DT:
		case RCP_CMD_READ_C_USER_DT:
			rcp_read_or_block_erase_data();
			break;
 		case RCP_CMD_WRITE_C_DT:
 		case RCP_CMD_BLOCKWRITE_C_DT:
			rcp_write_c_data();
			break;
		case RCP_CMD_KILL_RECOM_C:
			rcp_kill_c_tag();
			break;						
		case RCP_CMD_LOCK_C:
			rcp_lock_c_tag();
			break;				

		case RCP_CMD_CTL_CW:
			rcp_set_rf_cw();
			break;

		case RCP_GET_TEMPERATURE:
			rcp_get_temperature();
			break;


		case RCP_CMD_GET_RSSI:
			rcp_get_rssi();
			break;
					
 		case RCP_UPDATE_FLASH:
			rcp_update_registry();
			break;
		case RCP_ERASE_FLASH:
			rcp_erase_registry();
			break;
		case RCP_GET_FLASH_ITEM:
			rcp_get_registry();
			break;		
		case RCP_PR9200_ISP_DOWNLOAD:
			rcp_start_downlaod();
			break;
		case RCP_CMD_BAT_CHK:
			rcp_get_bat_status();
			break;
		case RCP_CMD_PWR_OFF:
			rcp_pwr_off();
			break;
		case RCP_CMD_SET_SERIAL_NO:
			rcp_set_serial_no();
			break;
		case RCP_CMD_GET_POWER_OFF_TIME:
			rcp_get_pwr_off_time();
			break;
		case RCP_CMD_SET_POWER_OFF_TIME:
			rcp_set_pwr_off_time();
			break;
									
#ifdef __FEATURE_NXP_UCODE__
		case RCP_CMD_NXP_READPROTECT:
			rcp_readprotect();
			break;
		case RCP_CMD_NXP_RESET_READPROTECT:
			rcp_reset_readprotect();
			break;
		case RCP_CMD_NXP_CHANGE_EAS:
			rcp_change_eas();
			break;
		case RCP_CMD_NXP_EAS_ALARM:
			rcp_eas_alarm();
			break;
		case RCP_CMD_NXP_CALIBRATE:
			rcp_calibrate();
			break;
#endif
		default:
			if(rcp_req_cb != NULL)
			{
				rcp_req_cb(&rcp_req_pkt_c);
			}
			else
			{
				rcp_write_msg_nack(RCP_MSG_RSP,FAIL_UNDEF_CMD);
			}
			break;
	}

	rcp_clear_req_buf();		
}



//!---------------------------------------------------------------
//!	@brief
//!		Clear the received rcp packet
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_clear_req_buf()
{
	memset((void *) &rcp_req_pkt_c, 0x00, sizeof(rcp_req_pkt_c));
}

//!---------------------------------------------------------------
//!	@brief
//!		Register callback function to 
//!
//! @param
//!		rcp_cb_prm_type cb
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_register_req_cb(rcp_cb_prm_type cb)
{
	rcp_req_cb = cb;
}

//!---------------------------------------------------------------
//!	@brief
//!		Create the NACK message and send the message
//!
//! @param
//!		const uint8 msg_type
//!		const uint8 fail_code
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_write_msg_nack(const uint8 msg_type, const uint8 fail_code)
{
	memset((void *) &rcp_rsp_pkt_c, 0x00, sizeof(rcp_rsp_pkt_c));
	rcp_rsp_pkt_c.pkt.preamble = RCP_PKT_PRAMBL;
	rcp_rsp_pkt_c.pkt.msg_type = msg_type;
	rcp_rsp_pkt_c.pkt.cmd_code = 0xFF;
	rcp_rsp_pkt_c.pkt.pl_length[0] = 0x00;
	rcp_rsp_pkt_c.pkt.pl_length[1] = 0x01;	
	rcp_rsp_pkt_c.pkt.payload[0] = fail_code;
	rcp_rsp_pkt_c.pkt.payload[1] = RCP_PKT_ENDMRK;

#ifdef	__FEATURE_ENABLE_RCP_CRC__	
	crc_append( &(((uint8*) &rcp_rsp_pkt_c)[1]), RCP_HEADEND_LEN);	
#endif
	rcp_write_rsp();
}


//!---------------------------------------------------------------
//!	@brief
//!		Create the ACK message and send the message
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_write_msg_ack(const uint8 msg_type, const uint8 cmd_code)
{
	memset((void *) &rcp_rsp_pkt_c, 0x00, sizeof(rcp_rsp_pkt_c));
	rcp_rsp_pkt_c.pkt.preamble = RCP_PKT_PRAMBL;
	rcp_rsp_pkt_c.pkt.msg_type = msg_type;
	rcp_rsp_pkt_c.pkt.cmd_code = cmd_code;
	rcp_rsp_pkt_c.pkt.pl_length[0] = 0x00;
	rcp_rsp_pkt_c.pkt.pl_length[1] = 0x01;
	rcp_rsp_pkt_c.pkt.payload[0] = 0x00;
	rcp_rsp_pkt_c.pkt.payload[1] = RCP_PKT_ENDMRK;

#ifdef	__FEATURE_ENABLE_RCP_CRC__
	crc_append( &(((uint8*) &rcp_rsp_pkt_c)[1]), RCP_HEADEND_LEN);	
#endif
	rcp_write_rsp();
}


//!---------------------------------------------------------------
//!	@brief
//!		Careate the string type message and send the message
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_write_string(const uint8 msg_type, const uint8 cmd_code, const uint8 *payload, const uint16 payload_size)
{
	int i = 0;
	const uint8 *ptr;
	
	memset((void *) &rcp_rsp_pkt_c, 0x00, sizeof(rcp_rsp_pkt_c));
	rcp_rsp_pkt_c.pkt.preamble = RCP_PKT_PRAMBL;
	rcp_rsp_pkt_c.pkt.msg_type = msg_type;
	rcp_rsp_pkt_c.pkt.cmd_code = cmd_code;
	rcp_rsp_pkt_c.pkt.pl_length[0] = MSB(payload_size);
	rcp_rsp_pkt_c.pkt.pl_length[1] = LSB(payload_size);

	ptr = &payload[0];
	for(i = 0; i < payload_size; i++)
	{
		rcp_rsp_pkt_c.pkt.payload[i] = *ptr++;
	}

	rcp_rsp_pkt_c.pkt.payload[payload_size] = RCP_PKT_ENDMRK;

#ifdef	__FEATURE_ENABLE_RCP_CRC__
	crc_append( &(((uint8*) &rcp_rsp_pkt_c)[1]), payload_size+RCP_HEADEND_LEN-1);
#endif
	rcp_write_rsp();
}

BOOL bt_retry_result = TRUE;
uint8 g_msg_len = 0;
uint8 g_payload[BT_DATA_MAX_LEN + BT_HEADER_LEN + BT_CHECKSUM_LEN] ={0,};
//uint8 *g_payload = 0;

BOOL lock = FALSE;
#define INT_LOCK() 		{lock = TRUE;}
#define INT_UNLOCK()	{lock = FALSE;}

//!---------------------------------------------------------------
//!	@brief
//!		Send all the response message to the specified rcp_io
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_write_rsp()
{
	uint16 len;
	uint8 msg_len;
	uint8 checksum;
	uint8 i,j;
	uint8 count;
	uint8 payload[RCP_PKT_MAX_BUF_SIZ];
	uint8 temp_payload[RCP_PKT_MAX_BUF_SIZ];

	len = (rcp_rsp_pkt_c.pkt.pl_length[0]<<8) +rcp_rsp_pkt_c.pkt.pl_length[1] + RCP_HEADEND_CRC_LEN;	
	count = len/BT_DATA_MAX_LEN;
	
	if(uart_check_in.in_cb(&uart_check_in)) // uart connect to bluetooth
	{
		memset((void *) &temp_payload, 0x00, sizeof(temp_payload));
		memcpy((void*) &temp_payload[0], (void*) &rcp_rsp_pkt_c, len);
		
		for(i=0; i<=count; i++)
		{
			memset((void *) &payload, 0x00, sizeof(payload));

			if((i == count) && (len % BT_DATA_MAX_LEN == 0))
				break;
			else if(i == count)
				msg_len = len % BT_DATA_MAX_LEN;
			else
				msg_len = BT_DATA_MAX_LEN;

			payload[0] = BT_PKT_PRAMBL;
			payload[1] = BT_MSG_CMD;
			payload[2] = msg_len + BT_OPCODE_LEN;
			payload[3] = BT_CMD_SEND;

			memcpy((void*) &payload[4], (void*) &temp_payload[BT_DATA_MAX_LEN*i], msg_len);

			checksum = payload[0];
			for(j = 1; j < msg_len + BT_HEADER_LEN; j++)
			{
				checksum = checksum^payload[j];
			}
			payload[msg_len + BT_HEADER_LEN] = checksum;

			if(rcp_io.send!=NULL)
			{
	#if (1)
				g_msg_len = msg_len;			 
				memcpy((uint8*) &g_payload, (uint8*) &payload, (uint8) msg_len + BT_HEADER_LEN + BT_CHECKSUM_LEN);
	#endif			
				rcp_io.send((uint8*) &payload, (uint8) msg_len + BT_HEADER_LEN + BT_CHECKSUM_LEN);
				delay_ms(10);				

		#if (0)
				if (bt_retry_result == FALSE)
				{
					rcp_io.send((uint8*) &payload, (uint8) msg_len + BT_HEADER_LEN + BT_CHECKSUM_LEN);
					delay_ms(10);
					
					bt_retry_result = TRUE;
				}
		#endif
			}

		}
	}

	else // uart connect to usb host
	{
		if(rcp_io.send!=NULL)
			rcp_io.send((uint8*) &rcp_rsp_pkt_c, len);
	}
}

void rcp_read_req(uint8* data, uint16 len)
{
	EVENT e;
	
	memcpy((void*) &rcp_req_pkt_c, (void*) data, len);

	e.handler = rcp_dispatch_event;
	e.param = EVENT_ENTRY;
	event_send(e);
}





//!---------------------------------------------------------------
//!	@brief
//!		Report tag ID of all inventoried tags
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_report_tags(uint8 rssi, uint8 len, uint8* data)
{	
	uint8 type;

	if(rfid_inventory_prm.cmd_sel != CMD_INVENTORY_SINGLE)
	{
		type = RCP_MSG_NOTI;
	}
	else
	{
		type = RCP_MSG_RSP;					
	}

//	if(rfid_inventory_prm.cmd_sel != CMD_INVENTORY_MULTIPLE_CYCLETIMENUM)
	{
	#if defined(__FEATURE_PREPEND_RCP_TAG_PC__) && defined (__FEATURE_APPEND_RCP_TAG_CRC__) // PC + EPC + CRC
		rcp_write_string(type, RCP_CMD_READ_C_UII, (uint8*) &(data[0]), len );
	#elif !defined(__FEATURE_PREPEND_RCP_TAG_PC__) 
		rcp_write_string(type, RCP_CMD_READ_C_UII, (uint8*) &(data[2]), len - RCP_TAG_PC_LEN); // EPC + CRC
	#elif !defined(__FEATURE_APPEND_RCP_TAG_CRC__)

	#if defined(__FEATURE_TAG_RSSI__)
		len += RCP_TAG_RSSI_LEN;
	#endif
		rcp_write_string(type, RCP_CMD_READ_C_UII, (uint8*) &(data[0]), len - RCP_TAG_CRC_LEN); // PC + EPC, default
	#else
		rcp_write_string(type, RCP_CMD_READ_C_UII, (uint8*) &(data[2]), len - RCP_TAG_PC_LEN - RCP_TAG_CRC_LEN); // EPC
	#endif		
	}
	
#if (0)
	else
	{
		uint8 payload[38] = {0,};

		if( (len - RCP_TAG_CRC_LEN + 2) > 38)
			len = len - RCP_TAG_CRC_LEN + 2;
			
		payload[0] = 0x01; // TNUM
		payload[1] = 0x40 | (len - RCP_TAG_CRC_LEN); // TTSZ

		memcpy((void*) &payload[2], &data[0],(len - RCP_TAG_CRC_LEN));
			
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_STRT_AUTO_READ_EX, (uint8*) &(payload[0]), len - RCP_TAG_CRC_LEN);
	}
#endif

}


void rcp_complete_inventory(uint8 discontinued, uint8 detected)
{
#ifndef	__TARGET_USIM__
	if(discontinued)
	{
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_STOP_AUTO_READ);
	}
	else
	{		
//		green_led.on_cb(&green_led);
//		debug_msg_str("GREEN LED ON");
		switch(rfid_inventory_prm.cmd_sel)
		{
		case CMD_INVENTORY_SINGLE:
			{
				if(!detected) rcp_write_msg_nack(RCP_MSG_NOTI, FAIL_NO_TAG_DETECTED);
			}
			break;
		case CMD_INVENTORY_MULTIPLE_CYCLE:
			{
				uint8 param = RCP_RSP_CPLT_AUTO_READ;
				rcp_write_string(RCP_MSG_NOTI, RCP_CMD_STRT_AUTO_READ, &param, 1);
			}
			break;
		case CMD_INVENTORY_MULTIPLE_CYCLETIMENUM:
			{
				uint8 param = RCP_RSP_CPLT_AUTO_READ;
				rcp_write_string(RCP_MSG_NOTI, RCP_CMD_STRT_AUTO_READ_EX, &param, 1);
				rfid_fsm_inventory_count = 0;
			}
			break;	
		}		
	}
#endif	
}
	

//!---------------------------------------------------------------
//!	@brief
//!		Get basic information from the reader	
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_get_reader_info()
{
	switch(rcp_req_pkt_c.pkt.payload[0])
	{
	case RDR_MODEL:
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_RD_INF, (uint8*) STR_MODEL, (uint8) sizeof(STR_MODEL));
		break;
	case RDR_SN:
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_RD_INF, (uint8*) STR_SN, (uint8) sizeof(STR_SN));	
		break;
	case RDR_MANFACT:
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_RD_INF, (uint8*) STR_MANFACT, (uint8) sizeof(STR_MANFACT));
		break;
	case RDR_FREQ:
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_RD_INF, (uint8*) STR_FREQ, (uint8) sizeof(STR_FREQ));
		break;
	case RDR_TAG_TYPE:
		{
			uint8 ret = TAG_ISO18000_6C;
			rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_RD_INF, &ret, sizeof(ret));
		}
		break;
	case RDR_FW_TIME:	  
		{
			uint8 param[14];
				
			param[0] = __DATE__[7];
			param[1] = __DATE__[8];
			param[2] = __DATE__[9];
			param[3] = __DATE__[10];

			if(FW_MONTH< 10)
			{
				param[4] = '0';
				param[5] = FW_MONTH + 49;
			}
			else
			{
				param[4] = '1';
				param[5] = FW_MONTH + 39;
			}
			
			param[6] = __DATE__[4];
			param[7] = __DATE__[5];
			
			param[8] = __TIME__[0];
			param[9] = __TIME__[1];
			param[10] = __TIME__[3];
			param[11] = __TIME__[4];
			param[12] = __TIME__[6];
			param[13] = __TIME__[7];
			
			rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_RD_INF, (uint8*) &param[0], (uint8) sizeof(param));
		}	
		break;		
	default:
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
		break;
	}
}



//!---------------------------------------------------------------
//!	@brief
//!		Set reader power mode
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_set_pd()
{
	if( (rcp_req_pkt_c.pkt.payload[0] > 0x03) )
	{
		rcp_write_msg_nack(RCP_MSG_RSP, RCP_CMD_CTL_RD_PWR);	
		return;
	}
	
	//debug_msg_str("RCP_SET_PD");

	rcp_write_msg_ack(RCP_MSG_RSP, RCP_CMD_CTL_RD_PWR);

	delay_ms(10); // wait for sending ack
		
	if(rcp_req_pkt_c.pkt.payload[0] != 0x00) hal_set_sys_deep_sleep();	
	else hal_set_sys_sleep();	
 

}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_set_system_reset()
{
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);
	rcp_write_msg_ack(RCP_MSG_RSP, RCP_CMD_CTL_RESET);
	delay_ms(10); // wait for sending ack
	NVIC_SystemReset();
}


void rcp_get_tx_pwr_lvl()
{
	int16 pwr_pa;
	uint8 param[2];
		
	hal_get_tx_pwr_lvl(&pwr_pa);

	param[0] = ((uint8*) &pwr_pa)[1];
	param[1] = ((uint8*) &pwr_pa)[0];

	rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_TX_PWR, &param[0], sizeof(param));
}

//!---------------------------------------------------------------
//!	@brief
//!		Set the Tx Power of Reader
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_set_tx_pwr_lvl()
{
	int16 pwr_pa;
	
	pwr_pa = TO_U16(rcp_req_pkt_c.pkt.payload[0], rcp_req_pkt_c.pkt.payload[1]);

	hal_set_tx_pwr_lvl(pwr_pa);

	rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_SET_TX_PWR);
}

//!---------------------------------------------------------------
//!	@brief
//!		Turn on/off RF block.
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_set_rf_cw()
{

	if(rcp_req_pkt_c.pkt.pl_length[1] != 0x14)
	{
		switch(rcp_req_pkt_c.pkt.payload[0])
		{
		case RF_TX_CW_ON:
#ifdef	__FEATURE_EXT_PA__	
		external_pa_mode_sel_ctxt_cw.on_cb(&external_pa_mode_sel_ctxt_cw);
#endif
			hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
			hal_set_cw(HAL_CW_ON);
			rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_CTL_CW);
			break;
		case RF_TX_CW_OFF:
#ifdef	__FEATURE_EXT_PA__	
			external_pa_mode_sel_ctxt_cw.off_cb(&external_pa_mode_sel_ctxt_cw);
#endif
			hal_set_cw(HAL_CW_OFF);
			hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);
			rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_CTL_CW);
			break;
		default:
			//! PKT Error Return
			rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
			break;
		}
		
	}
	else // USIM legacy
	{
		rcp_write_msg_ack(RCP_MSG_RSP,FAIL_NOT_SUPRT_CMD);
	}
	
}

//!---------------------------------------------------------------
//!	@brief
//!		Get cutrrent RF channel
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_get_rf_ch()
{
	uint8 param[2];
	
	hal_get_ch(&param[0], &param[1]);
		
	rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_CH, &param[0], sizeof(param));
}


//!---------------------------------------------------------------
//!	@brief
//!		Set current RF channel
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_set_rf_ch(void)
{		
	if(hal_set_ch(rcp_req_pkt_c.pkt.payload[0],rcp_req_pkt_c.pkt.payload[1]) != SYS_S_OK)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,RCP_CMD_SET_CH);
	}	
	else
	{				
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_SET_CH);
	}
}	

//!---------------------------------------------------------------
//!	@brief
//!		Get current region
//!						  
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_get_region()
{
	uint8 ret;

	hal_get_region(&ret);
	
	rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_REGION, &ret, sizeof(ret));
}

//!---------------------------------------------------------------
//!	@brief
//!		Set Current Region
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_set_region()
{
 	if( hal_set_region(rcp_req_pkt_c.pkt.payload[0]) != SYS_S_OK)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
		return;
	}

	rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_SET_REGION);
}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_get_rf_freq_hopping_tbl()
{
	uint8 param[HAL_FREQ_HOPPING_TBL_MAX+1];
	
	hal_rf_freq_hopping_table_type tbl;

	hal_get_freq_hopping_table(&tbl);
	
	param[0] = tbl.size;
	memcpy( (void*) &param[1], (void*) tbl.table_ptr, tbl.size);
	
	rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_HOPPING_TBL, &param[0], tbl.size + 1 );
}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_set_rf_freq_hopping_tbl()
{

	hal_rf_freq_hopping_table_type tbl;

	tbl.size = rcp_req_pkt_c.pkt.payload[0];
	tbl.table_ptr = &rcp_req_pkt_c.pkt.payload[1];
	
	hal_set_freq_hopping_table(&tbl);
	
	rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_SET_HOPPING_TBL);
}


//!---------------------------------------------------------------
//!	@brief
//!		Configures MODEM and RF block register values to change modulation type.
//!		Implicitly reference to rcp_req_pkt_c.
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_get_modulation()
{ 
	uint8 mode;
	
	protocol_get_modulation(&mode);
		
	rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_MODULATION, &mode, sizeof(mode));
}

//!---------------------------------------------------------------
//!	@brief
//!		Configures MODEM and RF block register values to change modulation type.
//!		Implicitly reference to rcp_req_pkt_c.
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_set_modulation()
{ 
	protocol_modulation_type param;

	if( (rcp_req_pkt_c.pkt.payload[0] == 0xFF) && (rcp_req_pkt_c.pkt.payload[1] == 0xFF))
	{
		param.rx_blf = TO_U16(rcp_req_pkt_c.pkt.payload[2],rcp_req_pkt_c.pkt.payload[3]);

		if(protocol_set_modulation_raw(&param)!= SYS_S_OK)
		{
			rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
			return;
		}
	}
	else
	{
		if(protocol_set_modulation(rcp_req_pkt_c.pkt.payload[0])!= SYS_S_OK)
		{
			rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
			return;
		}
	}
	
	rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_SET_MODULATION);
}



void rcp_get_temperature()
{
	int8 ret;

	hal_get_temperature(&ret);
	
	rcp_write_string(RCP_MSG_RSP, RCP_GET_TEMPERATURE, (uint8*) &ret, sizeof(ret));
}



//!---------------------------------------------------------------
//!	@brief
//!		Get FH and LBT Parameter
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------


void rcp_get_fh_lbt_param()
{
	uint8 param[11];
	hal_fhlbt_prm_type prm;

	hal_get_fhlbt_prm(&prm);
		
	param[0] = MSB(prm.tx_on_time);
	param[1] = LSB(prm.tx_on_time);
	param[2] = MSB(prm.tx_off_time);
	param[3] = LSB(prm.tx_off_time);
	param[4] = MSB(prm.sense_time);
	param[5] = LSB(prm.sense_time);
	param[6] = MSB(prm.lbt_rf_level);
	param[7] = LSB(prm.lbt_rf_level);
	param[8] = LSB(prm.fh_enable);
	param[9] = LSB(prm.lbt_enable);
	param[10] = LSB(prm.cw_enable);	
	
	rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_FH_LBT, &param[0], (uint8) sizeof(param) );
}

//!---------------------------------------------------------------
//!	@brief
//!		Set FH and LBT Parameter
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_set_fh_lbt_param()
{	
	hal_fhlbt_prm_type prm;
	
	prm.tx_on_time = TO_U16(rcp_req_pkt_c.pkt.payload[0], rcp_req_pkt_c.pkt.payload[1]);
	prm.tx_off_time = TO_U16(rcp_req_pkt_c.pkt.payload[2], rcp_req_pkt_c.pkt.payload[3]);
	prm.sense_time = TO_U16(rcp_req_pkt_c.pkt.payload[4], rcp_req_pkt_c.pkt.payload[5]);
	prm.lbt_rf_level = TO_I16(rcp_req_pkt_c.pkt.payload[6], rcp_req_pkt_c.pkt.payload[7]);
	prm.fh_enable = rcp_req_pkt_c.pkt.payload[8];
	prm.lbt_enable = rcp_req_pkt_c.pkt.payload[9];
	prm.cw_enable = rcp_req_pkt_c.pkt.payload[10];

	hal_set_fhlbt_prm(&prm);

	
	rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_SET_FH_LBT); 
}

//!---------------------------------------------------------------
//!	@brief
//!		Pass RSSI value to host
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_get_rssi()
{
	uint8 rssi[2] = {0, 0};
	uint16 rssi16 = 0;

	if(hal_get_rssi(&rssi16) == SYS_S_OK)
	{
		rssi[0] = MSB(rssi16);
		rssi[1] = LSB(rssi16);
		
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_RSSI, &rssi[0], 2);
	}
	else
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_GET_SIG_STREN);
	}	
}

//!---------------------------------------------------------------
//!	@brief
//!		Get Type C A/I Select Parameters
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_get_c_sel_parm()
{
	protocol_c_prm_sel_type *sel;		
	uint8 param[sizeof(protocol_c_prm_sel_type)];
	uint8 i;
	uint8 *ptr;

	protocol_get_c_sel_param(&sel);
	
	param[0] = (sel->target<<5 ) + (sel->action<<2) + (sel->mem_bank);
	param[1] = 0;
	param[2] = 0;
	param[3] = MSB(sel->pointer);
	param[4] = LSB(sel->pointer);
	param[5] = sel->len;
	param[6] = sel->truncate<<7;

	ptr = &param[7];
	for(i = 0; i < ((sel->len + 7) >> 3); i++)
	{
		*ptr++ = sel->mask[i];
	}

	rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_C_SEL_PARM, &param[0], 7 + ((sel->len + 7) >> 3));
}

//!---------------------------------------------------------------
//!	@brief
//!		Set Type C A/I Select Parameters
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_set_c_sel_parm()
{
	protocol_c_prm_sel_type sel = {0,};
	uint8 i;
	uint8 *ptr;

	sel.target = ((rcp_req_pkt_c.pkt.payload[0]>>5) & 0x07);
	sel.action = ((rcp_req_pkt_c.pkt.payload[0]>>2) & 0x07);
	sel.mem_bank = ((rcp_req_pkt_c.pkt.payload[0]) & 0x03);
	MSB(sel.pointer) = rcp_req_pkt_c.pkt.payload[3];
	LSB(sel.pointer) = rcp_req_pkt_c.pkt.payload[4];
	sel.len = rcp_req_pkt_c.pkt.payload[5];
	sel.truncate = 0; //reserved, rcp_req_pkt_c.pkt.payload[6]>>7;

	ptr = &rcp_req_pkt_c.pkt.payload[7];
	for(i = 0; i < ((sel.len + 7) >> 3); i++)
	{
		sel.mask[i] = *ptr++;
	}

	protocol_set_c_sel_param(&sel);
	
	rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_SET_C_SEL_PARM);
	
}
 

//!---------------------------------------------------------------
//!	@brief
//!		Get Type C A/I Query Related Parameters
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_get_c_qry_parm()
{
	protocol_c_prm_qry_type *qry;
	uint8 param[2];
	
	protocol_get_c_qry_param(&qry);
	
	param[0]  = (qry->dr <<7 ) + (qry->m<<5) + (qry->trext<<4)+ (qry->sel<<2)+ (qry->session);
	param[1]  = (qry->target <<7 ) + (qry->q <<3);

	rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_C_QRY_PARM, &param[0], 2);	
}


//!---------------------------------------------------------------
//!	@brief
//!		Set Type C A/I Query Related Parameters
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_set_c_qry_parm()
{
	protocol_c_prm_qry_type qry = {0,};
	
	qry.dr = (rcp_req_pkt_c.pkt.payload[0] >> 7) & 0x01 ;
    qry.m	= (rcp_req_pkt_c.pkt.payload[0] >> 5) & 0x03 ;
	//qry.trext = (iso180006c_ctxt.qry.m & 0x02) ? 0 : 1;
	qry.sel = (rcp_req_pkt_c.pkt.payload[0] >> 2) & 0x03 ;
	qry.session = (rcp_req_pkt_c.pkt.payload[0]) & 0x03 ;
    qry.target = (rcp_req_pkt_c.pkt.payload[1] >> 7) & 0x01 ;   //! 1bit
	qry.q = (rcp_req_pkt_c.pkt.payload[1] >> 3) & 0x0f ;   //! 4bit
	
	protocol_set_c_qry_param(&qry);
		
	rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_SET_C_QRY_PARM);	
}

//!---------------------------------------------------------------
//!	@brief
//!		Get Automatic Read Parameter
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_get_anticol_mode()
{
	uint8 mode;
	
	if(SYS_S_OK != protocol_get_anticol_mode(&mode))
	{
		rcp_write_msg_nack(RCP_MSG_RSP, FAIL_INVALID_PARM);		
	}
	else
	{
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_ANTICOL_MODE, &mode, 1);
	}
}

//!---------------------------------------------------------------
//!	@brief
//!		Set Automatic Read Parameter
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_set_anticol_mode()
{
	if(SYS_S_OK != protocol_set_anticol_mode(rcp_req_pkt_c.pkt.payload[0]))
	{
		rcp_write_msg_nack(RCP_MSG_RSP, FAIL_INVALID_PARM);		
	}
	else
	{
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_SET_ANTICOL_MODE);
	}
}

//!---------------------------------------------------------------
//!	@brief
//!		 Read Type C UII
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!--------------------------------------------------------------- 
void rcp_read_c_uii()
{
	rfid_inventory_prm_type param;
	SYS_S ret;
		
	param.cmd_sel = CMD_INVENTORY_SINGLE;
	param.count = 1;
	param.mtnu = 0;
	param.mtime = 0;

	ret = protocol_perform_inventory(&param);
		
	if(ret == SYS_S_PRT_ERR_BUSY)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_READ_AUTO_OP);	// Automatic Read In Operation
	}
}

//!---------------------------------------------------------------
//!	@brief
//!		Start Automatic Read
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_start_auto_read()
{
	rfid_inventory_prm_type param;
	SYS_S ret;	

	param.cmd_sel = CMD_INVENTORY_MULTIPLE_CYCLE;
	param.count = (rcp_req_pkt_c.pkt.payload[1] << 8) + rcp_req_pkt_c.pkt.payload[2];
	param.mtnu = 0;
	param.mtime = 0;

	ret = protocol_perform_inventory(&param);
	switch(ret)
	{
	case SYS_S_OK:
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_STRT_AUTO_READ);
		break;
	case SYS_S_PRT_ERR_BUSY:
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_READ_AUTO_OP);
		break;
	default:
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
		break;
	}	
}

//!---------------------------------------------------------------
//!	@brief
//!		Stop Automatic Read
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_stop_auto_read()
{
	protocol_discontinue_inventory();	
}


//!---------------------------------------------------------------
//!	@brief
//!		Start Automatic Read2
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------	
void rcp_start_auto_read2()
{
	rfid_inventory_prm_type param;
	SYS_S ret;	

	param.cmd_sel = CMD_INVENTORY_MULTIPLE_CYCLETIMENUM;
	param.mtnu = rcp_req_pkt_c.pkt.payload[1];
	param.mtime = rcp_req_pkt_c.pkt.payload[2];
	param.count = (rcp_req_pkt_c.pkt.payload[3] << 8) + rcp_req_pkt_c.pkt.payload[4];

	ret = protocol_perform_inventory(&param);
	switch(ret)
	{
	case SYS_S_OK:
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_STRT_AUTO_READ_EX);
		break;
	case SYS_S_PRT_ERR_BUSY:
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_READ_AUTO_OP);
		break;
	default:
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
		break;
	}

}				 



uint8 rcp_convert_syserrcode(int8 sys_err)
{
	switch(sys_err)
	{
		case SYS_S_PRT_ERR_BUSY:
			return FAIL_READ_AUTO_OP;
		case SYS_S_PRT_ERR_NO_TAG:
			return FAIL_NO_TAG_DETECTED;
		case SYS_S_PRT_ERR_ACCESS_TAG:
			return FAIL_PASSWORD;

		case SYS_S_PRT_ERR_READ_MEM:
			return FAIL_READ_TAG_MEM;
		case SYS_S_PRT_ERR_WRITE_MEM:
			return FAIL_WRITE_TAG;
		case SYS_S_PRT_ERR_KILL_TAG:
			return FAIL_KILL_TAG;
		case SYS_S_PRT_ERR_LOCK_TAG:
			return FAIL_CTRL_LOCK;

		//SYS_S_PRT_ERR_READ_MEM
		case SYS_S_PRT_ERR_MEM_OVERRUN:
			return FAIL_MEM_OVERRUN;
		case SYS_S_PRT_ERR_MEM_LOCK:
			return FAIL_MEM_LOCKED;
		case SYS_S_PRT_ERR_TAG_LOW_POWER:
			return FAIL_TAG_LOW_POWER;
		case SYS_S_PRT_ERR_TAG_KNOWN:
			return FAIL_UKNOWN_ERROR;
			
		default:
			return FAIL_UKNOWN_ERROR;		
	}
}

//!---------------------------------------------------------------
//!	@brief
//!		Read Type C Tag Data
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_read_or_block_erase_data()
{
	protocol_c_prm_read_erase_type param;
	rfid_data_type ret_data;
	SYS_S ret;
	uint8 access_pw_offset;
	uint8 membank_offset;

	if(rcp_req_pkt_c.pkt.cmd_code == RCP_CMD_READ_C_USER_DT)
	{
		access_pw_offset = 1;
		param.access_pw = 0;
	}
	else
	{
		access_pw_offset = 5;
		param.access_pw = 
			(rcp_req_pkt_c.pkt.payload[0] << 24) |
			(rcp_req_pkt_c.pkt.payload[1] << 16) |
			(rcp_req_pkt_c.pkt.payload[2] << 8 ) |
			(rcp_req_pkt_c.pkt.payload[3]) ;
	}

	param.target_id_len = 
	(rcp_req_pkt_c.pkt.payload[access_pw_offset]);
	//rcp_req_pkt_c.pkt.payload[5] - 2;

	param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[access_pw_offset+1];
	//param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[8];

	if(rcp_req_pkt_c.pkt.cmd_code == RCP_CMD_READ_C_USER_DT)
	{
		membank_offset = 0;
		param.mem_bank = MEM_USER;
	}
	else
	{
		membank_offset = 1;
		param.mem_bank = (protocol_c_membank_type)
			rcp_req_pkt_c.pkt.payload[access_pw_offset + rcp_req_pkt_c.pkt.payload[access_pw_offset] + 1];		
	}
	
	param.start_add =  ((rcp_req_pkt_c.pkt.payload[access_pw_offset + rcp_req_pkt_c.pkt.payload[access_pw_offset] + membank_offset + 1]) << 8) |
		(rcp_req_pkt_c.pkt.payload[access_pw_offset + rcp_req_pkt_c.pkt.payload[access_pw_offset] + membank_offset + 2]);

	if(rcp_req_pkt_c.pkt.cmd_code == RCP_CMD_READ_C_USER_DT)
	{
		param.word_count = (1 + 
			(((rcp_req_pkt_c.pkt.payload[access_pw_offset + rcp_req_pkt_c.pkt.payload[access_pw_offset] + membank_offset + 3]) << 8) | 
			(rcp_req_pkt_c.pkt.payload[access_pw_offset + rcp_req_pkt_c.pkt.payload[access_pw_offset] + membank_offset + 4]) ) >> 1);
	}
	else
	{
		param.word_count = 	((rcp_req_pkt_c.pkt.payload[access_pw_offset + rcp_req_pkt_c.pkt.payload[access_pw_offset] + membank_offset + 3]) << 8) |
			(rcp_req_pkt_c.pkt.payload[access_pw_offset + rcp_req_pkt_c.pkt.payload[access_pw_offset] + membank_offset + 4]);		
	}
	
	param.retry = 30;

	switch(rcp_req_pkt_c.pkt.cmd_code)
	{
	case RCP_CMD_READ_C_DT:
	case RCP_CMD_READ_C_USER_DT:
		param.cmd_sel = CMD_READ;
		break;
	case RCP_CMD_BLOCKERASE_C_DT:
		param.cmd_sel = CMD_BLOCK_ERASE;
		break;
	}
	
	ret = protocol_read_or_block_erase_memory(&param,&ret_data);

	if(ret != SYS_S_OK)
	{
		rcp_write_msg_nack(RCP_MSG_RSP, rcp_convert_syserrcode(ret));
		return;
	}

	switch(rcp_req_pkt_c.pkt.cmd_code)
	{
	case RCP_CMD_READ_C_DT:
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_READ_C_DT, ret_data.ptr_data, ret_data.len);
		break;
	case RCP_CMD_READ_C_USER_DT:
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_READ_C_USER_DT, ret_data.ptr_data, ret_data.len);
		break;
	case RCP_CMD_BLOCKERASE_C_DT:
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_BLOCKERASE_C_DT);
		break;
	}
	
}


//!---------------------------------------------------------------
//!	@brief
//!		Write Type C Tag Data
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_write_c_data()
{		
	protocol_c_prm_write_type param;
	SYS_S ret;

	param.access_pw = 
		(rcp_req_pkt_c.pkt.payload[0] << 24) |
		(rcp_req_pkt_c.pkt.payload[1] << 16) |
		(rcp_req_pkt_c.pkt.payload[2] << 8 ) |
		(rcp_req_pkt_c.pkt.payload[3]) ;

	param.target_id_len = 
	(rcp_req_pkt_c.pkt.payload[5]);
	//rcp_req_pkt_c.pkt.payload[5] - 2;

	param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[6];
	//param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[8];

	param.mem_bank = (protocol_c_membank_type)
		rcp_req_pkt_c.pkt.payload[5 + param.target_id_len + 1];

	param.start_add =
		((rcp_req_pkt_c.pkt.payload[5 + param.target_id_len + 2] << 8) |
		(rcp_req_pkt_c.pkt.payload[5 + param.target_id_len + 3]));

	param.word_count = 		
//		(rcp_req_pkt_c.pkt.payload[5 + param.target_id_len + 5] + 1) >> 1; // byte to word
		(rcp_req_pkt_c.pkt.payload[5 + param.target_id_len + 5]); 
	
	param.retry = 0;

	param.word_data_ptr = (uint16*) &rcp_req_pkt_c.pkt.payload[5 + (uint8) param.target_id_len + 6];

	switch(rcp_req_pkt_c.pkt.cmd_code)
	{
	case RCP_CMD_WRITE_C_DT:
		param.cmd_sel = CMD_WRITE;
		break;
	case RCP_CMD_BLOCKWRITE_C_DT:
		param.cmd_sel = CMD_BLOCK_WRITE;
		break;
	}

	ret = protocol_write_memory(&param);
		
	if(ret != SYS_S_OK)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,rcp_convert_syserrcode(ret));
		return;
	}

	switch(rcp_req_pkt_c.pkt.cmd_code)
	{
	case RCP_CMD_WRITE_C_DT:
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_WRITE_C_DT);
		break;
	case RCP_CMD_BLOCKWRITE_C_DT:
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_BLOCKWRITE_C_DT);
		break;
	}
}


//!---------------------------------------------------------------
//!	@brief
//!		Kill Type C Tag
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_kill_c_tag()
{
	protocol_c_prm_kill_type param;
	SYS_S ret;

	param.kill_pw = 
		(rcp_req_pkt_c.pkt.payload[0] << 24) |
		(rcp_req_pkt_c.pkt.payload[1] << 16) |
		(rcp_req_pkt_c.pkt.payload[2] << 8 ) |
		(rcp_req_pkt_c.pkt.payload[3]) ;

	param.target_id_len = 
		(rcp_req_pkt_c.pkt.payload[5]);
		//rcp_req_pkt_c.pkt.payload[5] - 2;
	
	param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[6];
	//param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[8];

	param.recom = 0; // TBD
	
	param.retry = 0;

	ret = protocol_kill_memory(&param);
		
	if(ret != SYS_S_OK)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,rcp_convert_syserrcode(ret));
	}
	else
	{		
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_KILL_RECOM_C);
	}	
}



//!---------------------------------------------------------------
//!	@brief
//!		Lock Type C Tag
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_lock_c_tag()
{
	protocol_c_prm_lock_type param;
	SYS_S ret;

	param.access_pw = 
		(rcp_req_pkt_c.pkt.payload[0] << 24) |
		(rcp_req_pkt_c.pkt.payload[1] << 16) |
		(rcp_req_pkt_c.pkt.payload[2] << 8 ) |
		(rcp_req_pkt_c.pkt.payload[3]) ;

	param.target_id_len = 
		(rcp_req_pkt_c.pkt.payload[5]);
		//rcp_req_pkt_c.pkt.payload[5] - 2;

	param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[6];
	//param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[8];

	param.mask_action = 	
		(rcp_req_pkt_c.pkt.payload[6 + param.target_id_len] << 16)    |
		(rcp_req_pkt_c.pkt.payload[6 + param.target_id_len + 1] << 8) |
		(rcp_req_pkt_c.pkt.payload[6 + param.target_id_len + 2]);
	
	param.retry = 0;

	ret = protocol_lock_memory(&param);
		
	if(ret != SYS_S_OK)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,rcp_convert_syserrcode(ret));
	}
	else
	{		
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_LOCK_C);
	}

}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_get_registry(void)
{	
	uint16 add;
	uint8 len;
	uint8 rarray[128];
		
	add = 
		(rcp_req_pkt_c.pkt.payload[0] << 8 ) |
		(rcp_req_pkt_c.pkt.payload[1]) ;

			
	if(hal_get_registry(add,&len,&rarray[0]) != SYS_S_OK)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
		return;			
	}
		
	rcp_write_string(RCP_MSG_RSP, RCP_GET_FLASH_ITEM, &rarray[0], len);

}

//!---------------------------------------------------------------
//!	@brief
//!		Update the registry
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_update_registry(void)
{
	if(hal_update_registry() == SYS_S_OK)
	{
		rcp_write_msg_ack(RCP_MSG_RSP, RCP_UPDATE_FLASH);
	}
	else
	{
		rcp_write_msg_nack(RCP_MSG_RSP, FAIL_CSTM_CMD);
	}	
}


//!---------------------------------------------------------------
//!	@brief
//!		Erase the registry
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_erase_registry(void)
{
	if(hal_erase_registry() == SYS_S_OK)
	{
		rcp_write_msg_ack(RCP_MSG_RSP, RCP_ERASE_FLASH);
	}
	else
	{
		rcp_write_msg_nack(RCP_MSG_RSP, FAIL_CSTM_CMD);
	}	
}

void rcp_start_downlaod()
{
	rcp_write_msg_ack(RCP_MSG_RSP, RCP_PR9200_ISP_DOWNLOAD);
	delay_ms(10);

	hal_start_download();

}

//! added by ajantech. start
//!----------------------------------------------------------------
void rcp_get_bat_status()
{
	uint8 status;
	if((bat_status_1_in.in_cb(&bat_status_1_in)) && (bat_status_2_in.in_cb(&bat_status_2_in)))// 3.6V (High, High)
		status = 0;
	else if((bat_status_1_in.in_cb(&bat_status_1_in)) && (!bat_status_2_in.in_cb(&bat_status_2_in)))// 3.8V (High, Low)
		status = 1;
	else // 4.1V (Low, Low)
		status = 2;
	rcp_write_string(RCP_MSG_RSP, RCP_CMD_BAT_CHK, &status, 1);
}

void rcp_pwr_off()
{
	hal_power_off();
}

void rcp_set_serial_no()
{

	hal_serial_no_type tbl;

	tbl.size = rcp_req_pkt_c.pkt.payload[0];
	tbl.ptr = &rcp_req_pkt_c.pkt.payload[1];
	
	hal_set_serial_no(&tbl);
	
	rcp_write_msg_ack(RCP_MSG_RSP, RCP_CMD_SET_SERIAL_NO);
}

void rcp_bt_discovery()
{
	bt_discoverable();
}

void rcp_get_pwr_off_time()
{
	uint16 ret;
	uint8 param[2];
	uint16 time;

	hal_get_power_off_time(&ret);
	
	param[0] = ((uint8*) &ret)[1];
	param[1] = ((uint8*) &ret)[0];
	time = TO_U16(param[0], param[1]);

	if(time == 65535)
	{
		param[0] = 0x00;
		param[1] = 0x00;
	}
	
	rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_POWER_OFF_TIME, &param[0], sizeof(param));
}

void rcp_set_pwr_off_time()
{
	uint16 time;
	uint16 length;

	length = TO_U16(rcp_req_pkt_c.pkt.pl_length[0], rcp_req_pkt_c.pkt.pl_length[1]);
	time = TO_U16(rcp_req_pkt_c.pkt.payload[0], rcp_req_pkt_c.pkt.payload[1]);

	if((time > 0 && time < 10) || length == 0)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
	}
	else
	{
		if(time == 0)
			time = 65535;
		if(hal_set_power_off_time(time) == SYS_S_OK)
		{
			pwr_off_time = time*1000;
			rcp_write_msg_ack(RCP_MSG_RSP, RCP_CMD_SET_POWER_OFF_TIME);
		}
	}
}

//!----------------------------------------------------------------
//! added by ajantech. end

#ifdef __FEATURE_NXP_UCODE__

//!---------------------------------------------------------------
//!	@brief
//!		Read Protect for NXP Tag
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_readprotect()
{
	protocol_c_prm_readprotect_type param;
	SYS_S ret;
	
	param.access_pw = 
		(rcp_req_pkt_c.pkt.payload[0] << 24) |
		(rcp_req_pkt_c.pkt.payload[1] << 16) |
		(rcp_req_pkt_c.pkt.payload[2] << 8 ) |
		(rcp_req_pkt_c.pkt.payload[3]) ;

	param.target_id_len = 
	(rcp_req_pkt_c.pkt.payload[5]);
	//rcp_req_pkt_c.pkt.payload[5] - 2;

	param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[6];
	//param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[8];

	param.retry = 0;

	ret = protocol_readprotect(&param);
		
	if(ret != SYS_S_OK)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,rcp_convert_syserrcode(ret));
	}
	else
	{		
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_NXP_READPROTECT);
	}

}



//!---------------------------------------------------------------
//!	@brief
//!		Reset Read Protect for NXP Tag
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_reset_readprotect()
{
 	protocol_c_prm_reset_readprotect_type param;
	SYS_S ret;
	 
	param.readprotect = 
		(rcp_req_pkt_c.pkt.payload[0] << 24) |
		(rcp_req_pkt_c.pkt.payload[1] << 16) |
		(rcp_req_pkt_c.pkt.payload[2] << 8 ) |
		(rcp_req_pkt_c.pkt.payload[3]) ;
	
	param.retry = 0;

	ret = protocol_reset_readprotect(&param);
		

	if(ret != SYS_S_OK)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,rcp_convert_syserrcode(ret));
	}
	else
	{		
		rcp_write_msg_ack(RCP_MSG_RSP, RCP_CMD_NXP_RESET_READPROTECT);
	}
}



//!---------------------------------------------------------------
//!	@brief
//!		Chanse EAS for NXP Tag
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_change_eas()
{
	protocol_c_prm_change_eas_type param;
	SYS_S ret;
		
	param.access_pw = 
		(rcp_req_pkt_c.pkt.payload[0] << 24) |
		(rcp_req_pkt_c.pkt.payload[1] << 16) |
		(rcp_req_pkt_c.pkt.payload[2] << 8 ) |
		(rcp_req_pkt_c.pkt.payload[3]) ;

	param.target_id_len = 
	(rcp_req_pkt_c.pkt.payload[5]);
	//rcp_req_pkt_c.pkt.payload[5] - 2;

	param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[6];
	//param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[8];

	param.change = rcp_req_pkt_c.pkt.payload[6 + (uint8)param.target_id_len];

	param.retry = 0;

	ret = protocol_change_eas(&param);

	if(ret != SYS_S_OK)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,rcp_convert_syserrcode(ret));
	}
	else
	{		
		rcp_write_msg_ack(RCP_MSG_RSP, RCP_CMD_NXP_CHANGE_EAS);
	}
}



//!---------------------------------------------------------------
//!	@brief
//!		EAS Alarm fo NXP Tag
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_eas_alarm()
{
	protocol_c_prm_eas_alarm_type param;
	rfid_data_type ret_data;

	SYS_S	ret = SYS_S_OK;

	// 1) RCP PKT Data Store
	//param.dr = rcp_req_pkt_c.pkt.payload[0];
	//param.m = rcp_req_pkt_c.pkt.payload[1];
	//param.trext = rcp_req_pkt_c.pkt.payload[2];		

	param.retry = 0;

	
	//ret = protocol_eas_alarm(&param,&ret_data);
	ret = protocol_eas_alarm(&param);

	
	if(ret != SYS_S_OK)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,rcp_convert_syserrcode(ret));
	}
	else
	{		
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_NXP_EAS_ALARM, &ret_data.ptr_data[0], ret_data.len);
	}	
}

//!---------------------------------------------------------------
//!	@brief
//!		Calbirate for NXP Tag
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rcp_calibrate()
{
	protocol_c_prm_calibrate_type param;
	rfid_data_type ret_data;

	SYS_S	ret = SYS_S_OK;

	param.access_pw = 
		(rcp_req_pkt_c.pkt.payload[0] << 24) |
		(rcp_req_pkt_c.pkt.payload[1] << 16) |
		(rcp_req_pkt_c.pkt.payload[2] << 8 ) |
		(rcp_req_pkt_c.pkt.payload[3]) ;

	param.target_id_len = 
	(rcp_req_pkt_c.pkt.payload[5]);
	//rcp_req_pkt_c.pkt.payload[5] - 2;

	param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[6];
	//param.target_id_ptr = &rcp_req_pkt_c.pkt.payload[8];

	param.retry = 0;


	ret = protocol_calibrate(&param, &ret_data);
	
	if(ret != SYS_S_OK)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,rcp_convert_syserrcode(ret));
	}
	else
	{		
		
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_NXP_CALIBRATE, &ret_data.ptr_data[0], ret_data.len);
		
	}
}
//</modify>
#endif


