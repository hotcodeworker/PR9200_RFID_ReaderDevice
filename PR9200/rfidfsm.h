//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//! 
//! @file	rfidfsm.h
//! @brief	simple finite state machine for LBT/FH scheduling
//! 
//! $Id: rfidfsm.h 1664 2012-07-09 07:58:40Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release


#ifndef     RFID_FSM_H
#define     RFID_FSM_H  

#include "config.h"
#include "commontypes.h"
#include "event.h"
#include "iso180006c.h"

//!-------------------------------------------------------------------                 
//! Strunctures and enumerations
//!-------------------------------------------------------------------
typedef enum
{
	EVENT_FSM_IDLE = EVENT_CUSTOM_BASE, //4
	EVENT_FSM_INVENTORY_REQ, //5
	EVENT_FSM_TX_OFF, //6
	EVENT_FSM_TX_OFF_EXPIRED, //7
	EVENT_FSM_LBT, //8
	EVENT_FSM_LBT_EXPIRED, //9
	EVENT_FSM_FH, //10 or A
	EVENT_FSM_RAMP_UP, // 11 or B
	EVENT_FSM_TX_ON, // 12 or C
	EVENT_FSM_TX_ON_EXPIRED, // 13 or D
	EVENT_FSM_MTIME_EXPIRED, // 14 or E
	EVENT_FSM_RAMP_DN, // 15 or F
	EVENT_FSM_REPORT_TAGS,	// 16 or 10
	EVENT_FSM_INVENTORY_COMPLETED, // 17 or 11
	EVENT_FSM_INVENTORY_DISCONTINUE // 18 or 12
} rfid_fsm_event_type;

typedef enum
{
	FSM_STATUS_AVAILABLE,
	FSM_STATUS_BUSY
} rfid_fsm_status_type;

typedef struct
{
	/*
	uint16	tx_on_time;
	uint16	tx_off_time;
	uint16	sense_time;
	int16  	lbt_rf_level;
	
	uint8	fh_enable;
	uint8	lbt_enable;
	uint8	cw_enable;
	*/

	uint8	inv_discontinue;
	
	uint8	tx_on_time_expired;	
	uint8	tx_off_time_expired;
	
	uint32	lbt_expired;
	uint16	tag_detected;

	rfid_fsm_status_type 	status;

	uint8 	tag_report_cb_count;	
	protocol_tag_report_cb_type	tag_report_cb[TAG_CB_COUNT_MAX];	
	
	uint8 	tag_completed_cb_count;
	protocol_tag_completed_cb_type	tag_completed_cb[TAG_CB_COUNT_MAX];
	
} rfid_fsm_ctxt_type;

#ifdef	__DEBUG_RFIDFSM__
#define	debug_msg_str_fsm(a,...)	debug_msg_str(a,##__VA_ARGS__)
#else
#define	debug_msg_str_fsm(a,...)
#endif

#define FSM_EVENT(_eparam) do {\
	EVENT _e;\
	_e.handler = fsm_dispatch_event;\
	_e.param = _eparam;\
	event_post(_e);} while(0)
	

void fsm_init(void);
rfid_fsm_status_type fsm_get_status(void);
void fsm_dispatch_event(EVENT e);
void fsm_state_active(EVENT e);
void fsm_state_tx_off_time(EVENT e);
void fsm_state_tx_on_time(EVENT e);

#endif
