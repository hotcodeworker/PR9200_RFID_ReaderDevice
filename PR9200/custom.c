//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file 	custom.c
//! @brief	Customer Module
//! 
//! $Id: custom.c 1626 2012-06-08 02:51:56Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/03/16	jsyi		initial release


//!-------------------------------------------------------------------
//! Include Files
//!------------------------------------------------------------------- 
#include "config.h"
#include "app_nxp.h"
#ifdef __FEATURE_INTERNAL__
#include "internal.h"
#endif
#include "custom.h"
#include "hal.h"
#include "timer.h"
#include "PR9200.h"
#include "registry.h"
#include "sio.h"

//!-------------------------------------------------------------------
//! External Data References                                          
//!-------------------------------------------------------------------
extern sio_interface_type rcp_io;
//!-------------------------------------------------------------------
//! Global Data Declaration                                           
//!-------------------------------------------------------------------
hal_gpio_ext_in_type power_btn_press 	= {	{HAL_GPIO_DEV_EXT_INT, 0, 2},
		HAL_GPIO_EXTINT_LEVEL_LOW,
		NULL, NULL, NULL, NULL};

uint8 bt_discovery_mode[5] = {0x77, 0x01, 0x01, 0x01, 0x76};

//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------	


//!---------------------------------------------------------------
//!	@brief
//!		Initialize parameters for customer
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void custom_init(void)
{						
#ifdef __FEATURE_INTERNAL__
	internal_init();
#endif

	hal_gpio_register(&power_btn_press);
	power_btn_press.enint_cb(&power_btn_press);

	if(!reg_sys_rst.reset)		// if system_reset is true
 	{ 				
		delay_ms(500);// wait for bt init
		bt_discoverable();
	}
	
}

// send discovery cmd
void bt_discoverable()
{
	if(rcp_io.send!=NULL)
	rcp_io.send((uint8*) &bt_discovery_mode, sizeof(bt_discovery_mode));
}
