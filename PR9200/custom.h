//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file 	custom.h
//! @brief	Customer Module
//! 
//! $Id: custom.h 1605 2012-05-10 05:36:28Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/03/16	jsyi		initial release


#ifndef CUSTOM_H
#define CUSTOM_H

#include "commontypes.h"

void custom_init(void);
void custom_rcp(void);
void bt_discoverable(void);


#endif
