//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	anti_col.c
//! @brief  Anti-collision algorithm
//! 
//! $Id: anti_col.c 1632 2012-06-26 06:57:30Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/05/14	jsyi		initial release

#include "config.h"
#include "rfidtypes.h"
#include "debug.h"
#include "rf.h"
#include "anti_col.h"

#define ANTI_COL_ALOHA_C 	(030) 	// slotted ALOHA C value 0.3
#define ANTI_COL_ALOHA_QMIN	(2) 	// slotted ALOHA minimum Q value
#define ANTI_COL_ALOHA_QMAX	(7) 	// slotted ALOHA maximum Q value


typedef struct
{
	uint8 mode;
	int16 qft_delta;	
} anticol_ctxt_type;

anticol_ctxt_type anticol_ctxt = {0,};

void anticol_set_mode(uint8 mode)
{
	anticol_ctxt.mode = mode;
	anticol_reset_mode();
}

void anticol_reset_mode()
{
	anticol_ctxt.qft_delta = 0;	
}

void anticol_update_round(uint8 slot_tag, uint8 slot_col, uint8 slot_no_tag)
{
	if(!slot_tag) 
	{
#ifdef	__FEATURE_PHASE_ROTATE__
		rf_rotate_phase();	 		
#endif
	}
	
	switch(anticol_ctxt.mode)
	{
	case ANTI_COL_MODE0: // Fixed Q
	case ANTI_COL_MODE1: 
	case ANTI_COL_MODE2: 
		// do nothing
		break;
	case ANTI_COL_MODE3: // Lower Bound
		anticol_ctxt.qft_delta += slot_col * 200;
		break;
	case ANTI_COL_MODE4: // EPC Gen 2 Annex D
		{
			if(!slot_tag)
			{
				anticol_ctxt.qft_delta = anticol_ctxt.qft_delta - ANTI_COL_ALOHA_C;
			}
			else if(slot_tag > 1)
			{
				anticol_ctxt.qft_delta = anticol_ctxt.qft_delta + ANTI_COL_ALOHA_C;
			}
		}
		break;
	case ANTI_COL_MODE5: // Collision consideration 1
		{
			if(slot_col > slot_no_tag)
			{
				anticol_ctxt.qft_delta = anticol_ctxt.qft_delta + 100;
			}
			else
			{
				anticol_ctxt.qft_delta = anticol_ctxt.qft_delta - 100;
			}
		}	
		break;
	case ANTI_COL_MODE6: // Collision consideration 2
		{
			uint8 slot_sum;

			slot_sum = slot_tag + slot_col + slot_no_tag;

			if( (slot_sum >> 1) > slot_col )
			{
				anticol_ctxt.qft_delta = anticol_ctxt.qft_delta + 100;				
			}
			else
			{
				anticol_ctxt.qft_delta = anticol_ctxt.qft_delta - 100;				
			}			
		}		
		break;
	}

	debug_msg_str_anticol("slot_tag = %d, slot_col = %d, slot_no_tag = %d, qft_delta = %d", slot_tag,slot_col,slot_no_tag, anticol_ctxt.qft_delta);

}

uint8 anticol_get_qval(uint8 q)
{	
	switch(anticol_ctxt.mode)
	{
	case ANTI_COL_MODE0:
	case ANTI_COL_MODE1:
	case ANTI_COL_MODE2:		
		return q;
	default:
		{
			int16 ret_q;
			
			ret_q = q + (anticol_ctxt.qft_delta/100);
			ret_q = LIMIT(ret_q, ANTI_COL_ALOHA_QMIN, ANTI_COL_ALOHA_QMAX);
			
			return (uint8) ret_q;
		}
	}
}



