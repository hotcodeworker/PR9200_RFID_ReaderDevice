//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//! 
//! @file	rcp_io.c
//! @brief	SIO Device Driver
//! 
//! $Id: sio.c 1755 2012-09-14 08:33:41Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release


//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include <STRING.H>

#include "commontypes.h"
#include "uart.h"
#include "spi.h"
#include "i2c.h"
#include "PR9200.h"
#include "sio.h"
#include "gpio.h"

//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------  
sio_byte_buf_type 	sio_rx_buf;
sio_byte_buf_type 	sio_rx_buf_temp;
sio_pkt_queue_type 	sio_tx_buf_queue;
sio_param_pf		sio_rx_cb;


//!---------------------------------------------------------------
//!	@brief
//!		Initiates the SIO type (UART, SPI, I2C)
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void sio_init(void)
{	

}

//!---------------------------------------------------------------
//!	@brief
//!		Gets the serial i/o type through the GPIO
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
sio_dev_type sio_get_io_sel(void)
{
	sio_dev_type sio_dev;
	
#ifdef 	__FEATURE_GPIO_SIO_SEL__
	gpio_set_dir(GPIO_IO_SEL0_PORT,GPIO_IO_SEL0_BIT,GPIO_DIR_IN);
	gpio_set_dir(GPIO_IO_SEL1_PORT,GPIO_IO_SEL1_BIT,GPIO_DIR_IN);

	sio_dev = (sio_dev_type)((GPIO1->DATA & (BIT5 | BIT6)) >> GPIO_IO_SEL0_BIT);
#else

#if defined(__FEATURE_UART_RCP__) || defined(__FEATURE_GPIO_SIO_SEL__) 
	sio_dev = SIO_UART;
#endif

#if defined(__FEATURE_SPI_SLAVE_RCP__) || defined(__FEATURE_GPIO_SIO_SEL__) 
	sio_dev = SIO_SPI;
#endif

#if defined(__FEATURE_I2C_SLAVE_RCP__) || defined(__FEATURE_GPIO_SIO_SEL__) 
	sio_dev = SIO_I2C;
#endif

#endif

	return sio_dev;
}	

//!---------------------------------------------------------------
//!	@brief
//!		Configure the serial interface  
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void sio_register_io(sio_interface_type *io)
{

	switch(sio_get_io_sel())
	{
#if defined(__FEATURE_UART_RCP__) || defined(__FEATURE_GPIO_SIO_SEL__) 	
	case SIO_UART:
		io->init = uart_init;
		io->send = uart_tx_pkt;
		break;
#endif
#if defined(__FEATURE_SPI_SLAVE_RCP__) || defined(__FEATURE_GPIO_SIO_SEL__) 	
	case SIO_SPI:
		io->init = spi_slave_init;
		io->send = sio_enqueue_tx_pkt;		
		break;
#endif		
#if defined(__FEATURE_I2C_SLAVE_RCP__) || defined(__FEATURE_GPIO_SIO_SEL__) 	
	case SIO_I2C:	
		io->init = i2c_slave_init;
		io->send = sio_enqueue_tx_pkt;		
		break;
#endif	
	}	

	sio_rx_cb = io->receive_cb;
	
}

//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void sio_post_received_data(uint8* data, uint8 len)
{
	SIO_CLR_IRQ();

	sio_rx_cb(data,len);
	
	sio_byte_buf_flush(&sio_rx_buf);
	sio_byte_buf_flush(&sio_rx_buf_temp);
	sio_pkt_queue_flush(&sio_tx_buf_queue);	
}


//!---------------------------------------------------------------
//!	@brief
//!		flushes the sio buffer
//!
//! @param
//!		sio_byte_buf_type* ptr_self
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void sio_byte_buf_flush(sio_byte_buf_type* ptr_self)
{
	memset((void*) ptr_self, 0x00, sizeof(sio_byte_buf_type));
}

//!---------------------------------------------------------------
//!	@brief
//!		Enqueues a byte data into the sio buffer
//!
//! @param
//!		sio_byte_buf_type* ptr_self
//!		uint8 inbyte
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void sio_byte_buf_enqueue(sio_byte_buf_type* ptr_self, uint8 inbyte)
{
	ptr_self->data[ptr_self->len] = inbyte;
	ptr_self->len++;	
}

//!---------------------------------------------------------------
//!	@brief
//!		Flushes the sio queue
//!
//! @param
//!		sio_pkt_queue_type* ptr_self
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void sio_pkt_queue_flush(sio_pkt_queue_type* ptr_self)
{
	ptr_self->count = ptr_self->head = ptr_self->tail = 0;	
}

//!---------------------------------------------------------------
//!	@brief
//!		Push a packet into the sio queue	
//!
//! @param
//!		sio_pkt_queue_type* ptr_self
//!		const uint8 *buf
//!		const uint8 len
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void sio_pkt_enqueue(sio_pkt_queue_type* ptr_self, const uint8 *buf, const uint8 len)
{
	memcpy((void*) &(ptr_self->pkt[ptr_self->head].data[0]), buf, len);
	ptr_self->pkt[ptr_self->head].len = len;
	ptr_self->head++;
	ptr_self->head %= SIO_PKT_QUEUE_MAX;
	ptr_self->count++;
}

//!---------------------------------------------------------------
//!	@brief
//!		Enqueue the tx packet 
//!
//! @param
//!		uint8 *inbyte
//!		uint8 size
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void sio_enqueue_tx_pkt(uint8 *inbyte, uint8 size)
{	
	if(size > SIO_BYTE_BUF_MAX) return;
	if(sio_tx_buf_queue.count >= SIO_PKT_QUEUE_MAX) return;	

	sio_pkt_enqueue(&sio_tx_buf_queue,inbyte,size);
	SIO_SET_IRQ();
	while( SIO_TST_IRQ() );	

}

//!---------------------------------------------------------------
//!	@brief
//!		Pop a packet from the sio queue
//!
//! @param
//!		sio_pkt_queue_type* ptr_self
//!
//! @return
//!		sio_byte_buf_type*
//!
//!---------------------------------------------------------------
sio_byte_buf_type* sio_pkt_dequeue(sio_pkt_queue_type* ptr_self)
{
	sio_byte_buf_type* ret;
	
	if(ptr_self->count == 0)
		return NULL;

	ret = &(ptr_self->pkt[ptr_self->tail]);

	ptr_self->tail++;
	ptr_self->tail %= SIO_PKT_QUEUE_MAX;
	ptr_self->count--;

	return ret;
}

