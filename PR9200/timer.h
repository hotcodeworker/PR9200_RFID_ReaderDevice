//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	timer.h
//! @brief	TIMER Device Driver
//! 
//! $Id: timer.h 1755 2012-09-14 08:33:41Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2011/07/25	sjpark	initial release
//! 2012/01/27	jsyi	modified fsm timer

#ifndef TIMER_H
#define TIMER_H

#include "commontypes.h"
#include "rfidtypes.h"
#include "event.h"

#define TIM_CLK					(SYS_DIG_CLK)


// Timer Control Register
#define	TIMER_ENABLE			(0x80)

#define FREE_MODE				(0x00)
#define PERIODIC_MODE			(0x40)

#define TIMER_INT_EN			(0x20)

#define PRESCALE_DIV_1			(0x00)
#define PRESCALE_DIV_16			(0x04)
#define PRESCALE_DIV_256		(0x08)


#define TIMER_SIZE_16BIT		(0x00)
#define TIMER_SIZE_32BIT		(0x02)

#define WRAPPING_MODE			(0x00)
#define ONESHOT_MODE			(0x01)

#define	T5US			96			// delay(96) = 5us
#define	T10US			192			// delay(192) = 10us
#define	T25US			480			// delay(480) = 25us
#define	T50US			960			// delay(960) = 50us
#define	T100US			1920		// delay(1920) = 100us

#define delay(i)  		do {\
	TIMER1->CONTROL |= TIMER_ENABLE;\
	TIMER1->LOAD = i; while(!TIMER1->RIS);\
	TIMER1->INTCLR = 0xFFFFFFFF;\
	TIMER1->CONTROL &= ~TIMER_ENABLE;\
	} while(0)


#define delay_t1(i)  		do {\
	TIMER2->CONTROL |= TIMER_ENABLE;\
	TIMER2->LOAD = ((i)*192); while(!TIMER2->RIS);\
	TIMER2->INTCLR = 0xFFFFFFFF;\
	TIMER2->CONTROL &= ~TIMER_ENABLE;\
	} while(0)

#define delay_10us(i)		delay(((i)*192))
//#define delay_irq_10us(i)	delay_irq(((i)*192))

#define set_t1_20ms() do {\
		TIMER1->CONTROL |= TIMER_ENABLE;\
		TIMER1->LOAD =(192*2000);\
		} while(0)

#define get_t1_20ms_expired() (!TIMER1->RIS)

#define clear_t1_20ms() do {\
		TIMER1->INTCLR = 0xFFFFFFFF;\
		TIMER1->CONTROL &= ~TIMER_ENABLE;\
		} while(0)


extern uint64 			RFID_SYSTEM_TICK;

typedef void(*timer_pf_type) (void);
typedef void(*timer_pf_type2) (uint8 prm);

#define timer0_start() 	TIMER0->CONTROL |= TIMER_ENABLE
#define timer0_stop()	TIMER0->CONTROL &= ~TIMER_ENABLE

#define timer1_start() 	TIMER1->CONTROL |= TIMER_ENABLE
#define timer1_stop()	TIMER1->CONTROL &= ~TIMER_ENABLE

#define timer2_start()	TIMER2->CONTROL |= TIMER_ENABLE
#define timer2_stop()	TIMER2->CONTROL &= ~TIMER_ENABLE

void timer3_start(void);
void timer3_stop(void);

void timer_init(void);
void delay_ms(uint32 ms);

BOOL timer0_register(timer_pf_type cb); // reserved for EVENT
BOOL timer3_register(hal_timer_type* timer); // user timer
BOOL timer3_deregister(void);
void timer_suspend_all(BOOL suspend);
void timer_event_delayed(EVENT e, uint16 time);
void timer_event_clear(void);
#endif

