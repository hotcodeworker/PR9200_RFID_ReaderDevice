//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	iap_spi.c
//! @brief	SPI Device Driver for IAP
//! 
//! $Id: iap_spi.c 1763 2012-09-24 06:56:31Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/08/31	sjpark	initial release

#include "PR9200.h"
#include "iap.h"
#include "iap_spi.h"


void iap_spi_flush_rxfifo(void)
{
	uint32 __attribute__((unused)) temp;
	
	while((SSP->SR & 0x04))
	{
		temp = SSP->DR;	
	}  
}


void iap_spi_slave_init( void ) 
{
	//Init SSP Pin
	GPIO0->AFSEL |= 0xF0;
	GPIO0->AFSEL &= ~0x1;	 // Init IRQ Pin
	 
	GPIO0->DIR |= 0x1;		// IRQ output
    GPIO0->DATA |= 0x1;		// IRQ High		

	//setup SSP 
	SSP->CR0 = SSP_IDLE_HIGH_VALID_FALLING | DSS_8Bit; 
	SSP->CR1 = LSBF | SLV;
	SSP->CPSR = 2; 
  
	//clear interrupt flag
	SSP->ICR = RTIC | RORIC;	

	//disable all interrupt
	SSP->IMSC = 0x0;
  
	//enable SSP
	SSP->CR1 |= SSE;

	if(SSP->SR & TFE)		
	{
		SSP->DR = 0xff;
	}

	iap_spi_flush_rxfifo();

	NVIC_EnableIRQ(SPI0_ERR_IRQn);
	NVIC_EnableIRQ(SPI_RX_IRQn);

}


void iap_spi_send_frame(uint8 cmd, uint8 *data, uint8 len, uint8 h_crc, uint8 l_crc)
{
	uint32 i = 0;
	uint8 hlen, llen, c;
  
	hlen = (uint8)(len >> 8);
	llen = (uint8)(len & 0xff);
  
	//wait fifo half empty  status
	while(!(SSP->RIS & TXMIS));  
  
	//wait busy
	while(SSP->SR & BSY);		

	//Write Dummy 2Byte Data
	while(!(SSP->SR & TNF));  	
	SSP->DR = 0xff;  //set dumy;  
  
	while(!(SSP->SR & TNF));  
	SSP->DR = 0xff;  //set dumy;      

	while(!(SSP->SR & TNF));  
	SSP->DR = MSG_PREAMBLE;

	while(!(SSP->SR & TNF));
	SSP->DR = MSG_RESPONSE;

	GPIO0->DATA &= ~0x1;
										  
	//check mode change (0xBB, 0x0A)
	while(1)
	{
		if(SSP->SR & RNE)	
		{
			c = (uint8)SSP->DR ;	  
			if(c == MSG_PREAMBLE)   
			{
				while(!(SSP->SR & RNE));  //wait RX
				c = (uint8)SSP->DR;		
		
				if(c == SPI_MODE_CHANGE)  //if change mode, send response
				{
					break;
				}   
			}
		}
	}
  
	//send cmd
	while(!(SSP->SR & TNF));
	SSP->DR = cmd;
  
	//send high len
	while(!(SSP->SR & TNF));
	SSP->DR = hlen;
   
	//send low len
	while(!(SSP->SR & TNF));
	SSP->DR = llen;
  
	for(i = 0; i < len; i++)
	{
		while(!(SSP->SR & TNF));
		SSP->DR = *data++;      
	}
  
	//send endmask
	while(!(SSP->SR & TNF));
	SSP->DR = MSG_ENDMARK;

	//send hcrc
	while(!(SSP->SR & TNF));
	SSP->DR = h_crc;

	//send lcrc
	while(!(SSP->SR & TNF));
	SSP->DR = l_crc;

	//wait tx end
	while(!(SSP->SR & TFE));
  
	iap_spi_flush_rxfifo(); 

	GPIO0->DATA |= 0x1;
	
}


int32 iap_spi_get_char(void)
{
	//Check Rx FiFo 
	if(SSP->SR & RNE)	
	{
		return (SSP->DR & 0xFF);	
	}
	
	return -1;
}


uint8 iap_spi_get_status(void)
{
	if(SSP->SR & RNE)
		return 1;
 	else
		return 0;
}





