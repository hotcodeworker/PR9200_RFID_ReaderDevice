//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	iap_i2c.c
//! @brief	I2C Device Driver for iap
//! 
//! $Id: iap_i2c.c 1763 2012-09-24 06:56:31Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/08/31	sjpark	initial release

#include "PR9200.h"
#include "iap.h"
#include "iap_i2c.h"


#define I2C_SLAVE_ADD		(0x92) // 1001001x (0x49)
#define I2C_CLR_PENDING()	(I2C->ICCR &= ~(I2C_INT_PEND))
#define I2C_WAIT_PENDING()	while(!(I2C->ICCR & I2C_INT_PEND))


void iap_i2c_slave_init( void ) 
{
	//Init SSP Pin
	GPIO1->AFSEL |= 0x06;
	GPIO0->AFSEL &= ~0x1;	 // Init IRQ Pin
	 
	GPIO0->DIR |= 0x1;		// IRQ output
    GPIO0->DATA |= 0x1;		// IRQ High		

	I2C->ICCR 	= 0;
	I2C->ICSR 	= 0;
	I2C->ICCR 	= I2C_ACK_GEN_EN | I2C_INT_EN;
	I2C->IAR 	= I2C_SLAVE_ADD; 
 	I2C->ICSR 	= I2C_SLV_RX_MODE | I2C_DATA_OUT_EN; 

	NVIC_DisableIRQ(I2C_IRQn);
}

uint8 iap_i2c_put_char(uint8 data)
{
	I2C->IDSR = data;
	I2C_CLR_PENDING();
	I2C_WAIT_PENDING();

	if((I2C->ICSR) & 0x01)	 	// NAK
		return 0;
	else						// ACK
		return 1;

}


void iap_i2c_send_frame(uint8 cmd, uint8 *data, uint8 len, uint8 h_crc, uint8 l_crc)
{
	uint8 inbyte;
	uint32 i;
	uint8 hlen, llen;
  
	hlen = (uint8)(len >> 8);
	llen = (uint8)(len & 0xff);

	GPIO0->DATA &= ~0x1;		// IRQ Low		 

	I2C_CLR_PENDING();
	I2C_WAIT_PENDING();
	inbyte = I2C->IDSR;

	if((I2C->ICSR & I2C_IAR_MATCH) && (inbyte & I2C_HOST_REQ_R))
	{
		I2C->ICSR &= 0x3F; // SLV_RX_MODE
		I2C->ICSR |= I2C_SLV_TX_MODE;
		I2C->ICSR |= I2C_DATA_OUT_EN;

		if(!iap_i2c_put_char(MSG_PREAMBLE))
			goto TX_END;			

		if(!iap_i2c_put_char(MSG_RESPONSE))
			goto TX_END;	

		if(!iap_i2c_put_char(cmd))
			goto TX_END;	

		if(!iap_i2c_put_char(hlen))
			goto TX_END;	

		if(!iap_i2c_put_char(llen))
			goto TX_END;	

		for(i = 0; i < len ; i++)
		{
			if(!iap_i2c_put_char(*data++))
				goto TX_END;	
		}

		if(!iap_i2c_put_char(MSG_ENDMARK))
			goto TX_END;	

		if(!iap_i2c_put_char(h_crc))
			goto TX_END;	

		if(!iap_i2c_put_char(l_crc))
			goto TX_END;	
		
	}

TX_END:
	GPIO0->DATA |= 0x1;		// IRQ High	

//	I2C->ICSR &= ~I2C_DATA_OUT_EN;	//Serial Output Disable
	I2C->ICSR &= 0x3F; // SLV_RX_MODE
		
}

int32 iap_i2c_get_char(void)
{
	I2C_CLR_PENDING();
	I2C_WAIT_PENDING();

	if(I2C->ICSR & I2C_IAR_MATCH)
	{
		I2C_CLR_PENDING();
		I2C_WAIT_PENDING();

		return I2C->IDSR & 0xFF;
	}
	return -1;

}


uint8 iap_i2c_get_status(void)
{
	I2C_CLR_PENDING();
	I2C_WAIT_PENDING();
	if(I2C->ICCR & I2C_INT_PEND)
		return 1;
	else							  
		return 0;					
}
