//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	debug.h
//! @brief	Implementation of debug service
//! 
//! $Id: debug.h 1525 2012-03-27 07:32:27Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release


#ifndef DEBUG_H
#define DEBUG_H


#include "commontypes.h"
#include "config.h"												  

#ifdef __DEBUG__
void debug_msg_str_(const char *Format, ...);
#define debug_msg_str(a,...)	debug_msg_str_(a,##__VA_ARGS__)
#else
#define debug_msg_str(a,...)
#endif

#ifdef	__AJT_DEBUG__
void debug_msg_str_2(const char *Format, ...);
#endif

#endif /*DEBUG_H*/

