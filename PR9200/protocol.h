//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file 	protocol.h
//! @brief	Protol interface layer
//! 
//! $Id: protocol.h 1763 2012-09-24 06:56:31Z sjpark $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/03/16	jsyi		initial release

#ifndef PROTOCOL_H
#define PROTOCOL_H
//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------  

#include "commontypes.h"
#include "rfidtypes.h"

//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 



//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------
SYS_S protocol_init(void);  
void protocol_register_tag_report_cb(protocol_tag_report_cb_type cb);
void protocol_register_tag_completed_cb(protocol_tag_completed_cb_type cb);
SYS_S protocol_get_c_sel_param(protocol_c_prm_sel_type **param);
SYS_S protocol_set_c_sel_param(const protocol_c_prm_sel_type *param);
SYS_S protocol_get_c_qry_param(protocol_c_prm_qry_type **param);
SYS_S protocol_set_c_qry_param(const protocol_c_prm_qry_type *param);
SYS_S protocol_get_anticol_mode(uint8 *mode);
SYS_S protocol_get_modulation(uint8 *mode);
SYS_S protocol_set_modulation(const uint8 mode);
SYS_S protocol_set_modulation_raw(protocol_modulation_type *param);
SYS_S protocol_set_anticol_mode(const uint8 mode);
SYS_S protocol_perform_inventory(const rfid_inventory_prm_type *param);
SYS_S protocol_discontinue_inventory(void);
SYS_S protocol_read_or_block_erase_memory(const protocol_c_prm_read_erase_type *param, rfid_data_type *ret_data);
SYS_S protocol_write_memory(const protocol_c_prm_write_type *param);
SYS_S protocol_kill_memory(const protocol_c_prm_kill_type *param);
SYS_S protocol_lock_memory(const protocol_c_prm_lock_type *param);
#ifdef __FEATURE_NXP_UCODE__
SYS_S protocol_readprotect(const protocol_c_prm_readprotect_type *param);
SYS_S protocol_reset_readprotect(const protocol_c_prm_reset_readprotect_type *param);
SYS_S protocol_change_eas(const protocol_c_prm_change_eas_type *param);
SYS_S protocol_eas_alarm(const protocol_c_prm_eas_alarm_type *param);
SYS_S protocol_calibrate(const protocol_c_prm_calibrate_type *param, rfid_data_type *ret_data);
#endif


#endif


