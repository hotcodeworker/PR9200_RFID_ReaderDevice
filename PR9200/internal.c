//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file 	internal.c
//! @brief	Misc. library for internal use
//! 
//! $Id: internal.c 1678 2012-07-13 04:28:57Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/03/16	jsyi		initial release

//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------  
#include <STRING.H>
#include "rcp.h"
#include "modem.h"
#include "rf.h"
#include "hal.h"
#include "iso180006c.h"
#include "rfidfsm.h"
#include "timer.h"
#include "internal.h"

//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 

//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!------------------------------------------------------------------- 
extern rcp_req_type rcp_req_pkt_c;
extern rssi_scan_type rssi_scan_c;
extern iso180006c_prm_type iso180006c_ctxt;
extern rfid_fsm_ctxt_type rfid_fsm_ctxt;
extern iso180006c_inventory_prm_type inventory_prm;
extern uint8 iso180006c_handle[2];

//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    

//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  
void internal_init(void)
{
	rcp_register_req_cb(&internal_rcp_parse);
}

//!---------------------------------------------------------------
//!	@brief
//!		Parses internal-use rcp packets.
//!		Implicitly reference to rcp_req_pkt_c.
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void internal_rcp_parse(void *p)
{
	rcp_req_type *req;

	req = (rcp_req_type *)p;

	switch(req->pkt.cmd_code)
	{
	case RCP_CMD_GET_MODEM_REG:
		internal_modem_get_reg();
		break;
	case RCP_CMD_SET_MODEM_REG:
		internal_modem_set_reg();
		break;		  
	case RCP_CMD_GET_RF_REG:
		internal_rf_get_reg();
		break;
	case RCP_CMD_SET_RF_REG:
		internal_rf_set_reg();
		break;

	case RCP_CMD_SCAN_RSSI:
		internal_scan_rssi();
		break;	
#if(0)
	case RCP_CMD_BLOCKWRITE_C_DT:
		internal_blockwrite_c_data();
		break;		
	case RCP_CMD_BLOCKERASE_C_DT:
		internal_blockerase_c_data();
		break;	
#endif

	
	default:
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_UNDEF_CMD);
		break;
	}	
}

//!---------------------------------------------------------------
//!	@brief
//!		Configures MODEM block register values.
//!		Implicitly reference to rcp_req_pkt_c.
//!
//! @param
//!		Implicitly rcp_req_pkt_c
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void internal_modem_set_reg()
{
	uint32 add;
	uint16 len;
	uint16 pl;
	uint8 i;
	uint8 *ptr;

	if(rcp_req_pkt_c.pkt.payload[1] < rcp_req_pkt_c.pkt.payload[0]) 
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
		return;
	}

	switch(rcp_req_pkt_c.pkt.payload[2])
	{
		case 0x00:
		case 0x01:
			add = rcp_req_pkt_c.pkt.payload[0] + MODEM_Base;
			len = (rcp_req_pkt_c.pkt.payload[1] - rcp_req_pkt_c.pkt.payload[0]) + 1;
			break;
		case 0x02:
		case 0x03:
			add = rcp_req_pkt_c.pkt.payload[0] + MODEM_Base + 0x100;
			len = (rcp_req_pkt_c.pkt.payload[1] - rcp_req_pkt_c.pkt.payload[0]) + 1;
			break;
	}		

	pl = rcp_req_pkt_c.pkt.pl_length[0] << 8;
	pl += rcp_req_pkt_c.pkt.pl_length[1];

	if((pl-3) != len)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
		return;
	}

	ptr = &rcp_req_pkt_c.pkt.payload[3];

	if ( (RM_ADD_TOP <= add) && (RM_ADD_TOP <= add+len-1) && (RM_ADD_BOT >= add) && (RM_ADD_BOT >= add+len-1) )
	{
		for(i = 0; i < len; i++)
		{
			EMI_WRITE(add + i, *ptr++);								

		}
		
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_SET_MODEM_REG);

	}
	else
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_NOT_SUPRT_CMD);
	}
}

//!---------------------------------------------------------------
//!	@brief
//!		Configures RF block register values.
//!		Implicitly reference to rcp_req_pkt_c.
//!
//! @param
//!		Implicitly rcp_req_pkt_c
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void internal_rf_set_reg()
{

	uint32 add;
	uint16 len;
	uint16 pl;
	uint8 i;
	uint8 *ptr;

	if(rcp_req_pkt_c.pkt.payload[1] < rcp_req_pkt_c.pkt.payload[0]) 
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
		return;
	}

	add = rcp_req_pkt_c.pkt.payload[0] + RF_BASE;
	len = (rcp_req_pkt_c.pkt.payload[1] - rcp_req_pkt_c.pkt.payload[0]) + 1;

	pl = rcp_req_pkt_c.pkt.pl_length[0] << 8;
	pl += rcp_req_pkt_c.pkt.pl_length[1];

	if((pl-2) != len)
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
		return;
	}

	ptr = &rcp_req_pkt_c.pkt.payload[2];

	if ( (RR_TOP_ADD <= add) && (RR_TOP_ADD <= add+len-1) && (RR_BOT_ADD >= add) && (RR_BOT_ADD >= add+len-1) )
	{
		for(i = 0; i < len; i++)
		{
			EMI_WRITE(add + i, *ptr++);
		}
	
		rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_SET_RF_REG); 
	}
	else
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_NOT_SUPRT_CMD);
	}														 
}

//!---------------------------------------------------------------
//!	@brief
//!		Pass MODEM block register values to the host.
//!		Implicitly reference to rcp_req_pkt_c.
//!
//! @param
//!		Implicitly rcp_req_pkt_c
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void internal_modem_get_reg()
{
	uint32 add;
	uint8 len;
	uint8 rarray[RCP_PKT_MAX_BUF_SIZ];
	uint8 i;
		
	if(rcp_req_pkt_c.pkt.payload[1] < rcp_req_pkt_c.pkt.payload[0]) 
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
		return;
	}

	switch(rcp_req_pkt_c.pkt.payload[2])
	{	
		case 0:
		case 1:
			add = rcp_req_pkt_c.pkt.payload[0] + MODEM_Base;
			len = (rcp_req_pkt_c.pkt.payload[1] - rcp_req_pkt_c.pkt.payload[0]) + 1;
			break;
		case 2:
		case 3:
			add = rcp_req_pkt_c.pkt.payload[0] + MODEM_Base + 0x100;
			len = (rcp_req_pkt_c.pkt.payload[1] - rcp_req_pkt_c.pkt.payload[0]) + 1;
			break;
	}
	

	if ( (RM_ADD_TOP <= add) && (RM_ADD_TOP <= add+len-1) && (RM_ADD_BOT >= add) && (RM_ADD_BOT >= add+len-1) )
	{
		rarray[0] = rcp_req_pkt_c.pkt.payload[0];
		rarray[1] = rcp_req_pkt_c.pkt.payload[1];
		rarray[2] =	rcp_req_pkt_c.pkt.payload[2];
			
		for(i = 0; i < len; i++)
		{
			rarray[3+i] = EMI_READ(add+i);
		}
		
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_MODEM_REG, &rarray[0], 3+len);
	}
	else
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_NOT_SUPRT_CMD);
	}
}

//!---------------------------------------------------------------
//!	@brief
//!		Pass RF block register values to the host.
//!		Implicitly reference to rcp_req_pkt_c.
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void internal_rf_get_reg()
{
 	uint32 add;
	uint8 len;
	uint8 rarray[RCP_PKT_MAX_BUF_SIZ];
	uint16 i;
	
	if(rcp_req_pkt_c.pkt.payload[1] < rcp_req_pkt_c.pkt.payload[0]) 
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_INVALID_PARM);
		return;
	}

	add = rcp_req_pkt_c.pkt.payload[0] + RF_BASE;
	len = (rcp_req_pkt_c.pkt.payload[1] - rcp_req_pkt_c.pkt.payload[0]) + 1;

	if ( (RR_TOP_ADD <= add) && (RR_TOP_ADD <= add+len-1) && (RR_BOT_ADD >= add) && (RR_BOT_ADD >= add+len-1) )
	{
		rarray[0] = rcp_req_pkt_c.pkt.payload[0];
		rarray[1] = rcp_req_pkt_c.pkt.payload[1];
		
		for(i = 0; i < len; i++)
		{
			rarray[2+i] = EMI_READ(add+i);
		}
				
		rcp_write_string(RCP_MSG_RSP, RCP_CMD_GET_RF_REG, &rarray[0], 2+len);
	}
	else
	{
		rcp_write_msg_nack(RCP_MSG_RSP,FAIL_NOT_SUPRT_CMD);
	} 
}



//!---------------------------------------------------------------
//!	@brief
//!		Scan the channel having lowest rssi of interferer.
//!		Implicitly returns rssi_scan_c: stores rssi values of each channel.
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
 void internal_scan_rssi()
{
	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
	modem_scan_rssi(); 	// modify !!!
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);
	
	rcp_write_string(RCP_MSG_RSP, RCP_CMD_SCAN_RSSI, (uint8*) &rssi_scan_c, (rssi_scan_c.stop_ch - rssi_scan_c.start_ch + 4) );
}

