//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	modem.h
//! @brief	Implementation of Interface for MODEM registers
//! 
//! $Id: modem.h 1763 2012-09-24 06:56:31Z sjpark $			
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release
//! 2011/07/25	sjpark	modified for PR9200



#ifndef MODEM_H
#define MODEM_H


//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "config.h"
#include "commontypes.h"
#include "rfidtypes.h"


//!-------------------------------------------------------------------
//! External Memory Interface Address define
//! ( Modem Register Address )
//!-------------------------------------------------------------------

#define 	MODEM_Base							0x40018000

#define 	RM_ADD_TOP							(MODEM_Base + 0x0000)
#define 	RM_ADD_BOT							(MODEM_Base + 0x01FF)

//-----------------------------------------------------------------------------
// Tx Wave Control Register
//-----------------------------------------------------------------------------
#define		RM_Tari_MSB							( MODEM_Base + 0x000 )
#define		RM_Tari_LSB							( MODEM_Base + 0x001 )

#define		RM_Data1_MSB						( MODEM_Base + 0x002 )
#define		RM_Data1_LSB						( MODEM_Base + 0x003 )

#define		RM_RTcal_MSB						( MODEM_Base + 0x004 )
#define		RM_RTcal_LSB						( MODEM_Base + 0x005 )

#define		RM_TRcal_MSB						( MODEM_Base + 0x006 )
#define		RM_TRcal_LSB						( MODEM_Base + 0x007 )

#define		RM_Delimiter_MSB					( MODEM_Base + 0x008 )
#define		RM_Delimiter_LSB					( MODEM_Base + 0x009 )
											   
#define		RM_delimiter_oddsymbol_ctrl_MSB		( MODEM_Base + 0x00A )
#define		RM_delimiter_oddsymbol_ctrl_LSB		( MODEM_Base + 0x00B )

#define		RM_Waveform_Downsample				( MODEM_Base + 0x00C )
#define		RM_DelimiterWaveform_Downsample		( MODEM_Base + 0x00D )


//-----------------------------------------------------------------------------
// Tx Data Control
//-----------------------------------------------------------------------------
#define		RM_Tx_Data_Length_MSB				( MODEM_Base + 0x00E )
#define		RM_Tx_Data_Length_LSB				( MODEM_Base + 0x00F )
#define		RM_Tx_Data							( MODEM_Base + 0x010 )

//-----------------------------------------------------------------------------
// Tx Status
//-----------------------------------------------------------------------------
#define		RM_Tx_STATE							( MODEM_Base + 0x011 )


//-----------------------------------------------------------------------------
// Tx Modem Control
//-----------------------------------------------------------------------------	
#define		RM_Tx_Enable_Ctrl					( MODEM_Base + 0x012 )
#define		RM_Tx_Modem_Config					( MODEM_Base + 0x013 )
#define		RM_Tx_Predistortion					( MODEM_Base + 0x014 )
#define		RM_Tx_phase_delay_MSB				( MODEM_Base + 0x015 )
#define		RM_Tx_phase_delay_LSB				( MODEM_Base + 0x016 )	  
#define		RM_Tx_Gain							( MODEM_Base + 0x017 )
#define		RM_Tx_TwosAmp_4CW					( MODEM_Base + 0x018 )

//-----------------------------------------------------------------------------
// Ramp Control
//-----------------------------------------------------------------------------
#define		RM_Ramp_control						( MODEM_Base + 0x019 )
#define		RM_Ramp_Time_control				( MODEM_Base + 0x01A )


//-----------------------------------------------------------------------------
// test pattern Control
//-----------------------------------------------------------------------------
#define		RM_Tx_test_pattern_ctrl				( MODEM_Base + 0x01B )
#define		RM_NCO_Scale						( MODEM_Base + 0x01C )
#define		RM_NCO_Period						( MODEM_Base + 0x01D )


//-----------------------------------------------------------------------------
// Rx Status
//-----------------------------------------------------------------------------
#define		RM_RxStatus							( MODEM_Base + 0x020 )
#define		RM_Rx_N_AGC_STATE					( MODEM_Base + 0x021 )

//-----------------------------------------------------------------------------
// Rx Data Control
//-----------------------------------------------------------------------------
#define		RM_Rx_FIFO_Select					( MODEM_Base + 0x022 )
#define		RM_Rx_Data_Length_MSB				( MODEM_Base + 0x023 )
#define		RM_Rx_Data_Length_LSB				( MODEM_Base + 0x024 )
#define		RM_Rx_Data_0						( MODEM_Base + 0x025 )
#define		RM_Rx_Data_1						( MODEM_Base + 0x026 )

//-----------------------------------------------------------------------------
// Rx Modem Control
//-----------------------------------------------------------------------------
#define		RM_Rx_Enable_Ctrl					( MODEM_Base + 0x027 )
#define		RM_LF_rate_index					( MODEM_Base + 0x028 )
#define		RM_Rx_Modem_Config					( MODEM_Base + 0x029 )
#define 	RM_Rx_CLK_type						( MODEM_Base + 0x02A )	
#define		RM_QryRep_session					( MODEM_Base + 0x02B )
#define	 	RM_IQ_select_Ctrl					( MODEM_Base + 0x02C )

//-----------------------------------------------------------------------------
// SFD Fail control
//-----------------------------------------------------------------------------
#define		RM_RxFail_value						( MODEM_Base + 0x02D )
#define		RM_RxFail_Power_Ctrl				( MODEM_Base + 0x02E )
#define		RM_Fail_Threshold_MSB				( MODEM_Base + 0x02F )
#define		RM_Fail_Threshold_LSB				( MODEM_Base + 0x030 )

//-----------------------------------------------------------------------------
// Signal Sensing
//-----------------------------------------------------------------------------
#define		RM_SENSING_THRESHOLD_16b_MSB		( MODEM_Base + 0x031 )
#define		RM_SENSING_THRESHOLD_16b_LSB		( MODEM_Base + 0x032 )
#define		RM_shift_bit_scaling				( MODEM_Base + 0x033 )
#define		RM_Detect_Sample_Count				( MODEM_Base + 0x034 )


//-----------------------------------------------------------------------------
// Fisrt Timing
//----------------------------------------------------------------------------- 
#define		RM_first_check_count				( MODEM_Base + 0x035 )
#define		RM_Tcontrol_1st_threshold			( MODEM_Base + 0x036 )
#define		RM_Ref_peaktype						( MODEM_Base + 0x037 )		 
#define		RM_Miller_peak_detect_Offset		( MODEM_Base + 0x038 )
												
//-----------------------------------------------------------------------------
// preamble
//-----------------------------------------------------------------------------
#define		RM_FM0_Preamble_Detect_Ctrl			( MODEM_Base + 0x039 )
#define		RM_Miller_Premable_Detect_Ctrl		( MODEM_Base + 0x03A )
												
//-----------------------------------------------------------------------------
// TED Control
//-----------------------------------------------------------------------------
#define 	RM_TED_Control						( MODEM_Base + 0x03B )
#define		RM_TED_Threshold	    			( MODEM_Base + 0x03C )
#define		RM_TED_Pterm_scale	    			( MODEM_Base + 0x03D )
#define		RM_TED_Iterm_scale	    			( MODEM_Base + 0x03E )
#define		RM_DCOC_Interval_sample	    		( MODEM_Base + 0x03F )

//-----------------------------------------------------------------------------
// DCOC Control
//-----------------------------------------------------------------------------
#define		RM_DCOC_control	    				( MODEM_Base + 0x040 )

//-----------------------------------------------------------------------------
// TxWave delimiter
//-----------------------------------------------------------------------------
#define		RM_Table_delimiter(offset)			( MODEM_Base + 0x050 + (offset) )

//-----------------------------------------------------------------------------
// TxWave
//-----------------------------------------------------------------------------
#define		RM_Table_waveform(offset)			( MODEM_Base + 0x090 + (offset) )

//-------------------------------------------------------------------
// predistortion
//-------------------------------------------------------------------
#define		RM_Table_predistortion(offset)		( MODEM_Base + 0x100 + (offset) )

//-----------------------------------------------------------------------------
// RSSI Control
//-----------------------------------------------------------------------------
#define		RM_RSSI_control						( MODEM_Base + 0x190 )
#define		RM_RSSI_I							( MODEM_Base + 0x191 )
#define		RM_RSSI_Q							( MODEM_Base + 0x192 )
#define		RM_LBT_threshold_MSB				( MODEM_Base + 0x193 )
#define		RM_LBT_threshold_LSB				( MODEM_Base + 0x194 )
#define		RM_LBT_time_MSB						( MODEM_Base + 0x195 )
#define		RM_LBT_time_LSB						( MODEM_Base + 0x196 )
												 
//-----------------------------------------------------------------------------
//  DCOC for analog
//-----------------------------------------------------------------------------
#define		RM_DCOC_AN_control					( MODEM_Base + 0x1B0 )
#define		RM_DCOC_mode1_Tpri_Wait             ( MODEM_Base + 0x1B1 )
#define		RM_DCOC_mode2_PULSE					( MODEM_Base + 0x1B2 )
#define 	RM_DCOC_LPF_Delay_MSB				( MODEM_Base + 0x1B3 )
#define 	RM_DCOC_LPF_Delay_LSB				( MODEM_Base + 0x1B4 )
#define 	RM_DCOC_RXM_Delay_MSB				( MODEM_Base + 0x1B5 )
#define 	RM_DCOC_RXM_Delay_LSB				( MODEM_Base + 0x1B6 )

//-----------------------------------------------------------------------------
// Collision Control
//-----------------------------------------------------------------------------
#define		RM_VBD_FreqTolerance_Type			( MODEM_Base + 0x1C0 )
#define		RM_VBD_FreqTolerance_value			( MODEM_Base + 0x1C1 )
#define		RM_ICD_RSSI_Threshold				( MODEM_Base + 0x1C2 )
#define		RM_VBD_NonContinuous_Valid_Bit		( MODEM_Base + 0x1C3 )
#define		RM_VBD_Continuous_Valid_Bit			( MODEM_Base + 0x1C4 )
												

//-----------------------------------------------------------------------------
// timer Control
//-----------------------------------------------------------------------------
#define		RM_Timer_control					( MODEM_Base + 0x1D0 )
#define		RM_manual_T1_time					( MODEM_Base + 0x1D1 )
#define		RM_manual_T2_time					( MODEM_Base + 0x1D2 )
												
//-----------------------------------------------------------------------------
// AGC Control
//-----------------------------------------------------------------------------
#define		RM_AGC_control						( MODEM_Base + 0x1E0 )
#define		RM_AGC_Init							( MODEM_Base + 0x1E1 )
#define		RM_AGC_Ref_Power					( MODEM_Base + 0x1E2 )
#define		RM_AGC_Pterm_scale					( MODEM_Base + 0x1E3 )
#define		RM_AGC_Iterm_scale					( MODEM_Base + 0x1E4 )
#define		RM_AGC_complete_Threshold			( MODEM_Base + 0x1E5 )
#define		RM_AGC_complete_LF					( MODEM_Base + 0x1E6 )
#define		RM_Ich_Gain_Control_MSB				( MODEM_Base + 0x1E7 )
#define		RM_Ich_Gain_Control_LSB				( MODEM_Base + 0x1E8 )
#define		RM_Qch_Gain_Control_MSB				( MODEM_Base + 0x1E9 )
#define		RM_Qch_Gain_Control_LSB				( MODEM_Base + 0x1EA )
#define		RM_AGC_Delay_MSB					( MODEM_Base + 0x1EB )
#define		RM_AGC_Delay_LSB					( MODEM_Base + 0x1EC )
#define		RM_Ich_Gain_Value_MSB				( MODEM_Base + 0x1ED )
#define		RM_Ich_Gain_Value_LSB				( MODEM_Base + 0x1EE )
#define		RM_Qch_Gain_Value_MSB				( MODEM_Base + 0x1EF )
#define		RM_Qch_Gain_Value_LSB				( MODEM_Base + 0x1F0 )


//-------------------------------------------------------------------
// Interrupt
//-------------------------------------------------------------------
#define		RM_INT_VECTOR						( MODEM_Base + 0x1FB )

//-------------------------------------------------------------------
// Type_B
//-------------------------------------------------------------------
#define		RM_Type_B_contorl					( MODEM_Base + 0x1FD )


//-------------------------------------------------------------------
// MODEM tp select
//-------------------------------------------------------------------
#define		RM_MODEM_TP_select					( MODEM_Base + 0x1FF )




#define 	MODEM_RAMP_TIME		(255)

	
//!-------------------------------------------------------------------                 
//! Strunctures and enumerations
//!------------------------------------------------------------------- 

//-----------------------------------------------------------------------------
// MODEM common type
//-----------------------------------------------------------------------------
typedef enum {
	CRC_16	=	BIT5_SET	,
	CRC_5	=	BIT4_SET	,
	NO_CRC	=	BIT5_CLR | BIT4_CLR
} CRC_type;


typedef enum {
	Twos_Complete	=	BIT0_SET	,
	Offset_Binary	=	BIT0_CLR	
} Data_Form_type;

typedef enum {
	CPOL_H	=	BIT7_SET	,
	CPOL_L	=	0	
} CLK_Polarity_type ;




//-----------------------------------------------------------------------------
// Reg_Tx_Enable_Ctrl : 0x012
//-----------------------------------------------------------------------------
typedef enum {
	TX_DSBASK	=	BIT7_SET,
	TX_PRASK	=	BIT6_SET
} Tx_ASKMod_type ;

typedef CRC_type	 tx_crc_type;

typedef enum {
	Preamble	=	BIT3_SET,
	Frame_Sync	=	BIT3_CLR	
} tx_header_type ;

typedef enum {
	Tx_Enable		=	BIT0_SET,
	Tx_Disable		=	BIT0_CLR	
} Tx_Enable_type ;




//-----------------------------------------------------------------------------
// Reg_Tx_Modem_Config : 0x013
//-----------------------------------------------------------------------------
typedef CLK_Polarity_type		DAC_CLK_Polarity_type;	

typedef enum {
	Symbol_1		=	BIT5_SET | BIT4_SET	,
	Symbol_0		=	BIT4_SET	,
	Auto_Symbol		=	BIT5_CLR | BIT4_CLR
} PRASK_Odd_Symbol_type ;

typedef enum {
	Phase_0		=	BIT3_SET	,
	Phase_90	=	BIT3_CLR
} Tx_Polar_mode ;

typedef Data_Form_type Tx_DAC_type; 	




//-----------------------------------------------------------------------------
// Reg_Predistortion : 0x014
//-----------------------------------------------------------------------------
typedef enum {
	Pre_dis_5		=	BIT6_SET			| BIT4_SET | BIT0_SET	,
	Pre_dis_4		=	BIT6_SET 					   | BIT0_SET	,
	Pre_dis_3		=			   BIT5_SET | BIT4_SET | BIT0_SET	,
	Pre_dis_2		=			   BIT5_SET 		   | BIT0_SET	,
	Pre_dis_1		=						  BIT4_SET | BIT0_SET	,
	Pre_dis_0		=			 						 BIT0_SET	,
	Pre_dis_Disable	=	0	
} Predistortion_type ;



//-----------------------------------------------------------------------------
// Reg_Ramp_control : 0x019
//-----------------------------------------------------------------------------
typedef enum {
	Ramp_Up		=	BIT4_SET	,
	Ramp_Down	=	BIT0_SET	
} Ramp_Control_type ;




//-----------------------------------------------------------------------------
// Reg_Tx_test_pattern_ctrl : 0x01B
//-----------------------------------------------------------------------------
typedef enum {
	Single_Tone		=	BIT5_SET | BIT4_SET | BIT0_SET	,
	Pattern_RND		=	BIT5_SET			| BIT0_SET	,
	Pattern_1			=		   BIT4_SET | BIT0_SET	,
	Pattern_0			=					  BIT0_SET	,
	Pattern_Disable	=	0	
} Test_Pattern_type ;




//-----------------------------------------------------------------------------
// Reg_Rx_FIFO_Select : 0x022
//-----------------------------------------------------------------------------
typedef enum {
	FIFO_0	,
	FIFO_1		
} Rx_FIFO_type ;	  

//-----------------------------------------------------------------------------
// Reg_Rx_Enable_Ctrl : 0x027
//-----------------------------------------------------------------------------
typedef enum {
#ifdef __FEATURE_RX_MOD_FM0__
	FM0		=	BIT6_CLR | BIT7_CLR	,
#endif
#ifdef __FEATURE_RX_MOD_M2__
	MI2		=	BIT6_SET		,
#endif
#ifdef __FEATURE_RX_MOD_M4__
	MI4		=	BIT7_SET			,
#endif	
#ifdef __FEATURE_RX_MOD_M8__
	MI8		=	BIT7_SET | BIT6_SET
#endif	
} modem_rx_mod_type ;

typedef CRC_type rx_crc_type		;

typedef enum {
	Auto_ACK_En		=	BIT3_SET	,
	Auto_ReqRn_En	=	BIT2_SET	,
	Auto_QryRep_En		=	BIT1_SET	,
	Auto_Tx_Disable	=	0
} auto_transmit_type ;

typedef enum {
	Rx_Enable		=	BIT0_SET	,
	Rx_disable		=	BIT0_CLR
} Rx_Enable_type ;

typedef enum {
	T1_TYPICAL,
	T1_20MS,
	T1_20MS_OFFSET
} rx_t1_type ;



//-----------------------------------------------------------------------------
// Reg_LF_rate_index : 0x028
//-----------------------------------------------------------------------------
typedef enum {
	LF_40		,
	LF_80		,
	LF_160		,
	LF_200		,
	LF_213_3	,
	LF_250		,
	LF_256		,
	LF_300		,
	LF_320		,
	LF_640 
} LF_Rate_Index_type ;




//-----------------------------------------------------------------------------
// Reg_Rx_Modem_Config : 0x029
//-----------------------------------------------------------------------------
typedef enum {
	LPF_Enable		=	BIT7_SET	,
	LPF_Disable		=	0
} Digital_LPF_type ;

typedef enum {
	First_LF_Enable		=	BIT5_SET	,
	First_LF_Disable	=	BIT5_CLR
} First_LF_Estimation_Enable_type ;

typedef enum {
	TED_Disable		=	BIT4_SET	,
	TED_Enable		=	BIT4_CLR
} Timing_Error_Detect_Enable_type ;

typedef enum {
	Unknown_Length		=	BIT2_SET	,
	Known_Length		=	BIT2_CLR
} Unknown_Length_type ;

typedef enum {
	Always_Enable		=	BIT1_SET	,
	Always_Disable		=	BIT1_CLR
} Rx_Decimation_type ;

typedef Data_Form_type Rx_ADC_type 	;



//-----------------------------------------------------------------------------
// Reg_QryRep_session : 0x02A
//-----------------------------------------------------------------------------
typedef CLK_Polarity_type		ADC_CLK_Polarity_type;

typedef enum {
	Feq_19_2MHz	,
	Feq_9_6MHz	,
	Feq_4_8KHz	
} ADC_CLK_Div_type ;




//-----------------------------------------------------------------------------
// Reg_QryRep_session : 0x02B
//-----------------------------------------------------------------------------
typedef enum {
	S0		,
	S1		,
	S2		,
	S3 
} Sesstion_type ;




//-----------------------------------------------------------------------------
// Reg_IQ_select_Ctrl : 0x02C
//-----------------------------------------------------------------------------
typedef enum {
	Auto_Select		=	BIT7_SET	,
	Qch_Select		=	BIT6_SET	,
	Ich_Select		=	0	,
} IQch_Select_type ;


//-----------------------------------------------------------------------------
// Reg_RxFail_Power_Ctrl : 0x02E
//-----------------------------------------------------------------------------
typedef enum {
	LFx1	,
	LFx2	,
	LFx4	,
	LFx8	
} FailPower_Period_type ;




//-----------------------------------------------------------------------------
// Reg_Miller_Premable_Detect_Ctrl : 0x03A
//-----------------------------------------------------------------------------
typedef enum {
	Fixed_Value		=	BIT7_SET	,
	Flexible_Value	=	BIT7_CLR
} Miller_Premable_Detect_type ;




//-----------------------------------------------------------------------------
// Reg_TED_Ctrl : 0x03B
//-----------------------------------------------------------------------------
typedef enum {
	TEDin_mod_select	=	BIT7_SET	,
	TEDin_mod_No_select	=	0	
} TEDin_mod_select_type ;

typedef enum {
	TEDin_1ov8_select		=	BIT6_SET	,
	TEDin_1ov8_No_select	=	0	
} TEDin_1ov8_select_type ;

typedef enum {
	TED_Cancel		=	BIT3_SET	,
	TED_Maintain	=	0	
} TED_Cancellation_type ;

typedef enum {
	TED_1	,
	TED_2	,
	TED_PI	
} TED_Mode_type ;




//-----------------------------------------------------------------------------
// Reg_DCOC_control : 0x04A
//-----------------------------------------------------------------------------
typedef enum {
	Eighth_Scale	=	BIT7_SET | BIT6_SET	,
	Quarter_Scale	=	BIT7_SET			,
	Half_Scale		=			   BIT6_SET ,
	Full_Scale		=	0
} DCOC_Scale_type ;

typedef enum {
	DCOC_Cancel		=	BIT4_SET	,
	DCOC_Maintain	=	0	
} DCOC_Cancellation_type ;

typedef enum {
	DCremove_1		,
	DCremove_2		,
	DCremove_3		,
	DCremove_4		
} DCremove_type ;	




//-----------------------------------------------------------------------------
// Reg_RSSI_control : 0x190
//-----------------------------------------------------------------------------
typedef enum {
	Period_852_us	=	BIT5_SET | BIT4_SET	,
	Period_426_us	=	BIT5_SET			,
	Period_213_us	=			   BIT4_SET ,
	Period_106_us	=	BIT5_CLR | BIT4_CLR
} RSSI_Period_type ;

typedef enum {
	MovingWindow	=	BIT3_SET	,
	Interval		=	0	
} RSSI_calibrate_type ;

typedef enum {
	RSSI_Disable		,	
	Auto_RSSI_Enable	,
	RSSI_Enable			,
	LBT_Enable			
}
 RSSI_Enable_type ;





//-----------------------------------------------------------------------------
// Reg_DCOC_control : 0x1B0
//-----------------------------------------------------------------------------
typedef enum {
	DCOC_1			=	BIT5_SET	,
	DCOC_0			=	BIT4_SET	,
	DCOC_control	=	0			,
} forced_DCOC_control_type ;

typedef enum {
	Mode_0	,
	Mode_1	,
	Mode_2	,
	Mode_3	
} DCOC_control_type ;





//-----------------------------------------------------------------------------
// Reg_Tx_test_pattern_ctrl : 0x1D0
//-----------------------------------------------------------------------------
typedef enum {
	Done	=	BIT7_SET	,
	Trig	=	0	
} T2_Trg_type ;

typedef enum {
	Flexible_Value_for_T2	=	BIT4_SET	,
	Fixed_20_Tpri			=	0	
} T2_Time_Mode_type ;

typedef enum {
	Fixed_20_ms		=	BIT3_SET | BIT2_SET	,
	Maximum			=	BIT3_SET			,
	Minimum			=	BIT2_SET			,
	Typical			=	0	
} T1_time_type ;

typedef enum {
	Flexible_Value_for_T1	=	BIT1_SET	,
	Fixed_10_Tpri_RTCal		=	0	
} T1_Time_Mode_type ;

typedef enum {
	T1_Timer_Disable	,
	T1_Timer_Enable		
} T1_Timer_Enable_type ;



//-----------------------------------------------------------------------------
// Reg_INT_VECTOR  : 0x1FE
//-----------------------------------------------------------------------------
typedef enum {
	ALL_IN_ONE		= 0x80,
	RSSI_INT		= 0x40,
	T2_INT			= 0x20,
	T1_INT			= 0x10,
	LBT_INT			= 0x08,
	TX_DONE_INT		= 0x04,
	RX_FAIL_INT		= 0x02,
	RX_SUCCESS_INT	= 0x01,	 
}Interrupt_Vector_type;


//-----------------------------------------------------------------------------
// Reg_AGC_control : 0x1E0
//-----------------------------------------------------------------------------
typedef enum {
	AGC_Result_Hold_Enable		=	BIT3_SET	,
	AGC_Result_Hold_Disable		=	0	
} AGC_Result_Hold_type ;

typedef enum {
	AGC_in_DataReceive_Enable	=	BIT3_SET	,
	AGC_in_DataReceive_Disable	=	0	
} AGC_en_in_DataReceive_type ;

typedef enum {
	AGC_Disable		,
	AGC_Enable		
} AGC_Enable_type ;


//-----------------------------------------------------------------------------
// Reg_Type_B_contorl : 0x1FD
//-----------------------------------------------------------------------------
typedef enum {
	RX_RAW_Ich		=	BIT4_SET	,
	RX_RAW_Qch		=	BIT4_CLR	
} RX_RAW_type ;

typedef enum {
	Type_B		=	BIT0_SET	,
	Type_C		=	BIT0_CLR	
} Reg_Type_B_Mode ;

typedef enum
{
#ifdef __FEATURE_TARI_6P25__	
	TARI_6_25U = 62,
#endif	
#ifdef __FEATURE_TARI_12P5__	
	TARI_12_5U = 125,
#endif
#ifdef __FEATURE_TARI_25__	
	TARI_25U = 250
#endif	
} modem_tx_tari_type;

typedef enum
{
	DR_8 = 0,
	DR_64_DIV_3 = 1	
} modem_rx_dr_type;

typedef enum
{		
#ifdef __FEATURE_BLF_040__	
	BLF_40 = 40,
#endif
#ifdef __FEATURE_BLF_080__	
	BLF_80 = 80,
#endif
#ifdef __FEATURE_BLF_160__
	BLF_160 = 160,
#endif
#ifdef __FEATURE_BLF_200__
	BLF_200 = 200,
#endif
#ifdef __FEATURE_BLF_213__
	BLF_213_3 = 213,
#endif
#ifdef __FEATURE_BLF_250__
	BLF_250 = 250,
#endif
#ifdef __FEATURE_BLF_300__
	BLF_300 = 300,
#endif
#ifdef __FEATURE_BLF_320__
	BLF_320 = 320,
#endif
#ifdef __FEATURE_BLF_640__
	BLF_640 = 640
#endif
} modem_rx_blf_type;

typedef enum
{ 
	DSB = 0,
	PRASK = 1	
} modem_tx_mod_type;

typedef enum
{
	RX_EN_DELAY0 = (0x00 << 4),
	RX_EN_DELAY1 = (0x01 << 4),
	RX_EN_DELAY2 = (0x02 << 4),
	RX_EN_DELAY3 = (0x03 << 4),
	RX_EN_DELAY4 = (0x04 << 4),
	RX_EN_DELAY5 = (0x05 << 4),
	RX_EN_DELAY6 = (0x06 << 4),
	RX_EN_DELAY7 = (0x07 << 4)
} rx_enable_delay_type;

typedef enum
{
	TYP_T2_TIMER = 0x00,// MAX(RTCAL, 10*TARI)
	MIN_T2_TIMER = 0x01,// MAX(RTCAL, 10*TARI) - 2usec
	MAX_T2_TIMER = 0x02,// MAX(RTCAL, 10*TARI) + 2usec
	CS20MS_TIMER = 0x03 // 20msec Timer
} t2_timer_ext_type;

// rfid protocol packet build
typedef enum
{
	TX_AP_NONE = 0x00,
	TX_AP_START = 0x01,
	TX_AP_DONE  = 0x02
} tx_transfer_type;

typedef enum
{
	RX_AP_NONE = 0x00,
	RX_AP_START = 0x01,
	RX_AP_DONE  = 0x02
} rx_transfer_type;

typedef enum
{
	T1_NONE = 0x00,
	T1_START = 0x01,
	T1_DONE = 0x02
} t1_transfer_type;

typedef enum
{
	LBT_NONE = 0x00,
	LBT_START = 0x01, 
	LBT_DONE = 0x02
}lbt_transfer_type;

typedef enum
{
	RSSI_NONE = 0x00,
	RSSI_START = 0x01, 
	RSSI_DONE = 0x02
}rssi_transfer_type;

typedef enum
{
	RX_S_NONE = 0x00,
	TIME_OUT = 0x01,
	COLLISION = 0x0E,
	CRC_OK = 0x10,
	RCV_STATE = 0x60
} rx_crc_status_type;
				 
typedef struct
{
	uint8 status;
} tx_status_type; 

typedef struct
{
	uint8 err;	
	uint8 status;
} rx_status_type;

typedef struct 
{
	//t1_transfer_type status;
	uint8 status;
}t1_status_type;


typedef struct
{
	uint8 status;
}lbt_status_type;

typedef struct
{
	uint8 status;
}rssi_status_type;

typedef struct
{
	tx_status_type tx;
	t1_status_type t1;
	rx_status_type rx;
	lbt_status_type lbt;
	rssi_status_type rssi;
} modem_status_type;

typedef enum
{
	READ_PWR_TMR = 0x00,
	WRITE_PWR_TMR = 0x01
} modem_pwr_timer_type;

typedef struct
{
	uint16 bit_length;
	uint8 air_pkt[36];
} rfid_packet_type;

typedef struct
{
	uint16 bit_length;
	uint8 air_pkt[512];	
} rfid_packet_long_type;

typedef struct
{
	uint8 start_ch;
	uint8 stop_ch;
	uint8 best_ch;
	uint8 scan_val[50];
} rssi_scan_type;


typedef struct
{
	modem_tx_tari_type	tx_tari;
	modem_rx_blf_type	rx_blf;	// Back Link Frequency
	modem_rx_mod_type	rx_mod;	// Rx Modulation Type
} modem_prm_type;

typedef struct modem_t2_20ms_type
{
	uint8 val;
	uint8 cnt;
} modem_t2_20ms_type;



//!-------------------------------------------------------------------                 
//! Macros
//!------------------------------------------------------------------- 
#define modem_get_rx_cmn_status() 	\
	(EMI_READ(RM_RxStatus))

#define modem_set_rxpkt_len(bit_length) \
	EMI_WRITE(RM_Rx_Data_Length_MSB, ( bit_length >> 8)); EMI_WRITE(RM_Rx_Data_Length_LSB,(uint8)( bit_length &0x00FF))
	
#define modem_get_rxpkt_len() 	\
	(TO_U16(EMI_READ(RM_Rx_Data_Length_MSB), EMI_READ(RM_Rx_Data_Length_LSB)))

////#define modem_wait_txpkt_send()	\
	//while(modem_s.tx.status == TX_AP_START) // __WFI(); // Wait MODEM interrupt when a Tx packet was sent out

#define modem_wait_txpkt_send() \
	do { uint16 _i = 1600; \
	for(;_i>0; _i--) { if(modem_s.tx.status != TX_AP_START	) break;}\
	while(EMI_READ(RM_Tx_Enable_Ctrl) & Tx_Enable); \
	} while(0) // 1600 = 500us

//#define modem_wait_t1_timeout() \
//	while(modem_s.t1.status == T1_START);// __WFI(); // Wait MODEM interrupt when a T1 Time out

#define modem_wait_t1_timeout() \
	do { uint16 _i = 1600; \
	for(;_i>0; _i--) { if(modem_s.t1.status != T1_START	) break; } \
	} while(0) // 1600 = 500us

#define modem_wait_rxpkt_receive() \
	while(EMI_READ(RM_Rx_Enable_Ctrl) & Rx_Enable)
//	while(modem_s.rx.status == RX_AP_START); //__WFI(); // Wait MODEM interrupt when receiving an incoming packet

#define modem_set_delimiter(val) \
		EMI_WRITE(RM_Delimiter_MSB, ( val >> 8) );	 	EMI_WRITE(RM_Delimiter_LSB,  val &0x00FF );	 

#define modem_change_rxfifo()	\
		EMI_WRITE(RM_Rx_FIFO_Select, EMI_READ(RM_Rx_FIFO_Select) ^ 0x01)

#define MODEM_R_ENABLE()	EMI_SET_BIT(RM_Rx_Modem_Config, Always_Enable)
#define MODEM_R_DISABLE()	EMI_CLR_BIT(RM_Rx_Modem_Config, Always_Enable)

#define MODEM_TAG_RSSI_ENABLE() EMI_WRITE(RM_RSSI_control, Period_852_us | Auto_RSSI_Enable)
#define MODEM_TAG_RSSI_DISABLE() EMI_WRITE(RM_RSSI_control, 0x00)

#define MODEM_TX_RAW(data)	SYSCON->TXRAW = data;

#define MODEM_AGC_ENABLE()	EMI_SET_BIT(RM_AGC_control, AGC_Enable);
#define MODEM_AGC_DISABLE()	EMI_CLR_BIT(RM_AGC_control, AGC_Enable);

//!-------------------------------------------------------------------
//! Global Data
//!-------------------------------------------------------------------
extern modem_prm_type modem_c;

//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------
void modem_init(void);
void modem_reset(void);	
void modem_init_register(void);
void modem_set_ted(modem_rx_blf_type blf, modem_rx_mod_type rx_mod);
void modem_set_m(modem_rx_mod_type m);

void modem_transmit_pkt(tx_header_type header,
							tx_crc_type	 crc,
							const uint16 bit_len, 
							const uint8 *byte_array	);

BOOL modem_receive_pkt(auto_transmit_type auto_rx, 
						rx_crc_type crc,
						rx_t1_type t1,
						uint16 bit_len,
						rfid_pkt_rx_type *data);

uint16 modem_get_rssi(void);
void modem_get_rx_byte(uint16 *bit_len, uint8 *adata);
BOOL modem_set_blf(modem_rx_blf_type blf, modem_rx_dr_type dr);
void modem_set_dac_ramp_up(uint8 cnt);
void modem_set_dac_ramp_dn(uint8 cnt);
void modem_scan_rssi(void);

void modem_set_agc(void);
void modem_set_dcoc(void);
void modem_set_ask_wave_table(void);

int16 modem_get_tx_gain(void);
void modem_set_tx_gain(int16 pwr_pa);
BOOL modem_get_cw_detected(void);
#if(0)
void modem_set_pwr_timer(modem_pwr_timer_type type);  
#endif

#endif
