//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	testmode.c
//! @brief	Implementation of test service
//! 
//! $Id: testmode.c 1702 2012-08-02 02:05:39Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release


//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------
#include <STRING.H>  
#include "PR9200.h"
#include "testmode.h"
#include "modem.h"
#include "iso180006c.h"
#include "rcp.h"
#include "rf.h"
#include "utility.h"

#ifdef __FEATURE_LAB_TEST__

//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 
#define 	CERTI_SEL_MASK_BIT_SIZE 	252

//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    


const uint8 certi_sel_mask[]
	= {	0x0A, 0xCB, 0xCD, 0x21, 0x14, 0xDA, 
	 	0xE1, 0x57, 0x7C, 0x6D, 0xBF, 0x4C, 0x91, 0xA3, 0xCD, 0xA2, 0xF1, 
		0x69, 0xB3, 0x40, 0x98, 0x9C, 0x1D, 0x32, 0xC2, 0x90, 0x46, 0x5E, 
		0x5C, 0x14, 0x23, 0xCC };


iso180006c_prm_type backup_cparam = {0,};

//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!-------------------------------------------------------------------
extern modem_status_type 		modem_s;
extern volatile rcp_status_type rcp_s;
extern rcp_req_type 			rcp_req_pkt_c;
extern rcp_rsp_type 			rcp_rsp_pkt_c;
extern iso180006c_rfid_pkt_type 			iso180006c_rfid_pkt_c;
extern iso180006c_prm_type		iso180006c_ctxt;
extern protocol_anti_col_type 	inventory_mode;


//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  


//!---------------------------------------------------------------
//!	@brief
//!		Parse the RCP test command	
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void test_parse_rcp_cmd()
{		
	uint16 i;
	
	switch(rcp_req_pkt_c.pkt.cmd_code)
	{	
/*
		case RCP_TEST_SEL_REP:
			test_sel_repeat();
			break;
		case RCP_TEST_QRY_REP:
			test_qry_repeat();
			break;
		case RCP_TEST_QRYREP_REP:
			test_qryrep_repeat();
			break;
*/
		case RCP_TEST_MODEM_RX:
			test_modem_rx_fer();
            break;
		
		case RCP_TEST_SEL_REP:
			memcpy((void*) &backup_cparam, (void*) &iso180006c_ctxt, sizeof(iso180006c_prm_type));
			iso180006c_ctxt.sel.len = (uint8) CERTI_SEL_MASK_BIT_SIZE;
		
			for(i = 0; i < ((iso180006c_ctxt.sel.len + 7) >> 3); i++)
			{
				iso180006c_ctxt.sel.mask[i] = certi_sel_mask[i];
			}

			iso180006c_rfid_pkt_c.sel.bit_length = 10; // test

			iso180006c_pktbuild_sel(&iso180006c_rfid_pkt_c.sel); // Select Packet Build
			modem_set_tx_fifo((uint16) iso180006c_rfid_pkt_c.sel.bit_length, &iso180006c_rfid_pkt_c.sel.air_pkt[0]);// Set FIFO
			modem_transmit_pkt(Frame_Sync, CRC_16, Tx_Enable); // Send Select >
			
 			rcp_param_c.test_cmd = RCP_TEST_SEL_REP;			
			rcp_param_c.test_mode = TRUE;
			rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_TEST_CMD);

//			debug_msg_str("RCP_TEST_SEL_REP");
			break;
			
		case RCP_TEST_QRY_REP:			
			iso180006c_pktbuild_qry(&iso180006c_rfid_pkt_c.qry); // Query Packet Build
			modem_set_tx_fifo((uint16) iso180006c_rfid_pkt_c.qry.bit_length, &iso180006c_rfid_pkt_c.qry.air_pkt[0]);// Set FIFO			
			modem_transmit_pkt(Preamble, CRC_5, Tx_Enable); // Send Query >

			rcp_param_c.test_cmd = RCP_TEST_QRY_REP;			
			rcp_param_c.test_mode = TRUE;
			rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_TEST_CMD);
			break;

		case RCP_TEST_QRYREP_REP:
			
			iso180006c_pktbuild_qryrep(&iso180006c_rfid_pkt_c.qryrep); //QueryRep Packet Build 
			modem_set_tx_fifo((uint16) iso180006c_rfid_pkt_c.qryrep.bit_length, &iso180006c_rfid_pkt_c.qryrep.air_pkt[0]);//! Set FIFO
			modem_transmit_pkt(FRM_SYSC,NO_CRC,TX_ENABLE); 

			rcp_param_c.test_cmd = RCP_TEST_QRYREP_REP;			
			rcp_param_c.test_mode = TRUE;
			rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_TEST_CMD);
			break;
	
		case RCP_TEST_EPC_SELMASK:
			test_epc_set_sel_mask();
			rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_TEST_CMD);
			break;
			
		case RCP_TEST_EPC_QRY:
			test_epc_qry();
			rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_TEST_CMD);
			break;
			
		case RCP_TEST_EPC_SELQRY:
			test_epc_selqry();
			rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_TEST_CMD);
			break;
			
		case RCP_TEST_EPC_QRYACK:
			test_epc_qry_ack();
			rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_TEST_CMD);
			break;
			
		case RCP_TEST_EPC_QRYQRYREP:
			test_epc_qry_qryrep();
			rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_TEST_CMD);
			break;
		case RCP_CMD_STOP_TEST_MOD:
			memcpy((void*) &iso180006c_ctxt, (void*) &backup_cparam, sizeof(iso180006c_prm_type));			
			rcp_param_c.test_mode = FALSE;
			rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_TEST_CMD);
			break;
	
		case RCP_RESERVED_00:			
			test_set_singletone();			
			break;					  
		case RCP_RESERVED_04:
			test_clr_singletone();			
			break;
			
        default:
            rcp_write_msg_nack(RCP_MSG_RSP,FAIL_UNDEF_CMD);
            break;

    }
}




//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void test_set_singletone()
{
 	EMI_WRITE(RM_Tx_test_pattern_ctrl, 	Test_Pattern_Enable | Single_Tone);
	EMI_WRITE(RM_NCO_Scale,			63 );
	EMI_WRITE(RM_NCO_Period	,		27 );
	rcp_write_msg_ack(RCP_MSG_RSP,RCP_RESERVED_00);
}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void test_clr_singletone()
{
	EMI_WRITE(RM_Tx_test_pattern_ctrl, 	Test_Pattern_Disable);	
	rcp_write_msg_ack(RCP_MSG_RSP,RCP_RESERVED_04);
}


//!---------------------------------------------------------------
//!	@brief
//!		Send the Select command repeatedly 
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void test_sel_repeat()
{
	uint8 temp = EMI_READ(RM_Tx_Enable_Ctrl) & 0xFE;
	EMI_WRITE(RM_Tx_Enable_Ctrl, temp |  Tx_Enable);
	while( EMI_TST_BIT(RM_Tx_Enable_Ctrl,Tx_Enable) == Tx_Enable );
	delay_10us(5);	// 960 clk or 50 us delay required, do not remove this
}


//!---------------------------------------------------------------
//!	@brief
//!		Send the Query command repeatedly	
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void test_qry_repeat()
{

	EMI_SET_BIT(RM_Tx_Enable_Ctrl,Tx_Enable);
	while( EMI_TST_BIT(RM_Tx_Enable_Ctrl,Tx_Enable) == Tx_Enable );
	delay_10us(5);	// 960 clk or 50 us delay required, do not remove this
}

void test_qryrep_repeat()
{   	
    modem_transmit_pkt(FRM_SYSC,NO_CRC,TX_ENABLE); 
}

//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void test_modem_rx_fer()
{
	uint16 i = 0;
	uint8 param[30] = {0,};
	uint8 ok;
	uint8 fail;
	uint8 fail_time_out;
	rfid_packet_type Sel = {0,}; 

	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON); //ON
	hal_set_cw(HAL_CW_ON);	
	
	//modem_set_rxpkt_len(144);

	if( (rcp_req_pkt_c.pkt.pl_length[0] + rcp_req_pkt_c.pkt.pl_length[1]) == 0x00)
	{
		ok = 0;
		fail = 0;
		fail_time_out =0;
		
		for(i = 0; i< 100; i++)
		{	
		
			if(modem_receive_pkt(Auto_Tx_Disable, CRC_16, T1_TYPICAL, 144, NULL))
			{
				ok++;
			}
			else
			{
				fail++;
				if(modem_s.rx.err == TIME_OUT)
				{
					fail_time_out++;
					delay_10us(50); // 500us
//					delay_long(500);		
				}
				delay_10us(50); // 500us
				//delay_long(500);				
			}


		}

		param[0]  = 0xA0;
		param[1]  = ok;
		param[2]  = fail;
		param[3]  = fail_time_out;

		rcp_write_string(RCP_MSG_RSP, RCP_CMD_STRT_RX_TEST, &param[0], 4);
	}
	else	
	{
		iso180006c_pktbuild_sel(&Sel); //! Select Packet Build

		//! 2) Send Select
		modem_set_tx_fifo((uint16) Sel.bit_length, &Sel.air_pkt[0]);//! Set FIFO
		modem_transmit_pkt(Frame_Sync, CRC_16, Tx_Enable); //! Send Select >

		if(modem_receive_pkt(Auto_Tx_Disable, CRC_16, T1_TYPICAL, NULL, NULL))
		{
			param[0]  = 0xA0;
			param[1]  = 0x01;
			param[2]  = 0x00;
			param[3]  = modem_s.rx.err;

			
			i = 144;
			modem_get_rx_byte(&i, &param[4]); //! PcUIICrc16 Received;
			rcp_write_string(RCP_MSG_RSP, RCP_CMD_STRT_RX_TEST, &param[0], 22);

			
		}
		else
		{
			param[0]  = 0xA0;
			param[1]  = 0x00;
			param[2]  = 0x01;
			param[3]  = modem_s.rx.err;
			
			
			i = 144;
			modem_get_rx_byte(&i, &param[4]); //! PcUIICrc16 Received;
			rcp_write_string(RCP_MSG_RSP, RCP_CMD_STRT_RX_TEST, &param[0], 22);

			
		}
			
	}

	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF); 		

}

//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void test_epc_set_sel_mask()
{
	uint8 i;
	
	memcpy((void*) &backup_cparam, (void*) &iso180006c_ctxt, sizeof(iso180006c_prm_type));
	iso180006c_ctxt.sel.len = (uint8) CERTI_SEL_MASK_BIT_SIZE;
		
	for(i = 0; i < ((iso180006c_ctxt.sel.len + 7) >> 3); i++)
	{
		iso180006c_ctxt.sel.mask[i] = certi_sel_mask[i];
	}

	iso180006c_pktbuild_sel(&iso180006c_rfid_pkt_c.sel); // Select Packet Build

	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
	hal_set_cw(HAL_CW_ON);

    modem_set_tx_fifo((uint16) iso180006c_rfid_pkt_c.sel.bit_length, &iso180006c_rfid_pkt_c.sel.air_pkt[0]);//! Set FIFO

	modem_transmit_pkt(Frame_Sync,CRC_16,Tx_Enable); // Send Select >
	delay_ms(2); // ms
	//delay_long(1000);
	memcpy((void*) &iso180006c_ctxt, (void*) &backup_cparam, sizeof(iso180006c_prm_type));

	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);		
}

//!---------------------------------------------------------------
//!	@brief
//!		Send the Query command for test
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void test_epc_qry()
{
	// 1) Packet Build
	iso180006c_pktbuild_qry(&iso180006c_rfid_pkt_c.qry);  // Query Packet Build	

	iso180006c_ctxt.qry.q = 0;

	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);
	hal_set_cw(HAL_CW_ON);

	// 3) Send Query
	modem_set_tx_fifo((uint16) iso180006c_rfid_pkt_c.qry.bit_length, &iso180006c_rfid_pkt_c.qry.air_pkt[0]);// Set FIFO
	modem_transmit_pkt(Preamble, CRC_5, Tx_Enable); // Send Query >

	delay_10us(150); // 1500 ms
	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);
	
}

//!---------------------------------------------------------------
//!	@brief
//!		Send the Select and Query command for test
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void test_epc_selqry()
{
	// 1) Packet Build
	iso180006c_pktbuild_sel(&iso180006c_rfid_pkt_c.sel); // Select Packet Build
	iso180006c_pktbuild_qry(&iso180006c_rfid_pkt_c.qry);  // Query Packet Build	

	iso180006c_ctxt.qry.q = 0;
	
	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);			
	hal_set_cw(HAL_CW_ON);

	// 2) Send Select
	modem_set_tx_fifo((uint16) iso180006c_rfid_pkt_c.sel.bit_length, &iso180006c_rfid_pkt_c.sel.air_pkt[0]);// Set FIFO
	modem_transmit_pkt(Frame_Sync,CRC_16,Tx_Enable); // Send Select >

	delay_10us(1); // 10us

	// 3) Send Query
	modem_set_tx_fifo((uint16) iso180006c_rfid_pkt_c.qry.bit_length, &iso180006c_rfid_pkt_c.qry.air_pkt[0]);// Set FIFO
	modem_transmit_pkt(Preamble,CRC_5,Tx_Enable); // Send Query >
	
	delay_10us(150); // 1500 ms	
	hal_set_cw(HAL_CW_OFF);	
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);
}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void test_epc_qry_ack()
{

	uint8 pre_inv_mode = inventory_mode;
	// 1) Packet Build	
	iso180006c_pktbuild_qry(&iso180006c_rfid_pkt_c.qry);  // Query Packet Build	
	
	iso180006c_ctxt.qry.q = 0;

	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);			
	hal_set_cw(HAL_CW_ON);

	iso180006c_set_anticol_mode(ANTI_COL_MODE1);			
	iso180006c_select_inventory();			  // sjpark: adjust parameter

	delay_10us(150); // 1500 ms	
	hal_set_cw(HAL_CW_OFF);	
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);

	inventory_mode = pre_inv_mode;

}

//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void test_epc_qry_qryrep()
{
  	uint8 pre_inv_mode = inventory_mode;

	iso180006c_ctxt.qry.q = 0;

	hal_set_rfidblk_pwr(HAL_RFIDBLK_ON);	
	hal_set_cw(HAL_CW_ON);
	
	iso180006c_set_anticol_mode(ANTI_COL_MODE1);	
	iso180006c_select_inventory();	  // sjpark: adjust parameter

	delay_10us(150); // 1500 ms
	hal_set_cw(HAL_CW_OFF);
	hal_set_rfidblk_pwr(HAL_RFIDBLK_OFF);
	
	inventory_mode = pre_inv_mode;
}


#endif // __FEATURE_LAB_TEST__

