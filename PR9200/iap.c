//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file		iap.c
//! @brief	
//! 
//! $Id: iap.c 1763 2012-09-24 06:56:31Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/08/31	sjpark	initial release

//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------     
#include "PR9200.h"
#include "flash.h"
#include "iap.h"
#include "iap_uart.h"
#include "iap_spi.h"
#include "iap_i2c.h"


//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    
// Com Function Pointer
static void (*rcp_send_frame)(uint8 cmd, uint8 *txdata, uint8 txlen,  uint8 h_crc, uint8 l_crc);
static int32  (*rcp_get_byte)(void);   
//Uart, Spi or I2C Data Register Pointer 
volatile uint32 *pdata;						   
// Check uart or spi Flag Register 
static uint8  (*rcp_get_status)(void);			  
// RCP Packet Buffer (4byte Aligned)
rcp_packet_type pkt __attribute__((aligned(4)));

//!-------------------------------------------------------------------                 
//! Macros
//!------------------------------------------------------------------- 
#define iap_timer_run(ms) 		TIMER0->LOAD = (19200*ms);\
								TIMER0->CONTROL |= TIMER_ENABLE;	 
							
#define iap_timer_get_value()	TIMER0->VALUE

#define iap_timer_reload(ms) 	TIMER0->LOAD = (19200*ms)

//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  


void iap_timer_init(void)
{
	TIMER0->CONTROL = 0;
	TIMER0->INTCLR = 0xFFFFFFFF;	
	TIMER0->CONTROL = ONESHOT_MODE | PERIODIC_MODE | TIMER_SIZE_32BIT;	
}


uint8 flash_write_data(uint32 start_addr, uint32 *pData, uint8 len)
{
	uint8 i;

	//Check Write Address for 4Byte Align
	if(start_addr % 4) 
	{		
		return 1;		//not 4byte Aligned
	}

	if(start_addr % 0x400 == 0)	   // Erase Page
		EFLSH_PAGE_ERASE(start_addr);		

	//Write Flash Data 		
	for(i = 0; i < len; i+=4)
	{
		EFLSH_WRITE_WORD((start_addr + i), (*pData++));
	}

	return 0;	
}	


uint16 crc_ccitt_update (uint16 crc, uint8 data)
{
	crc  = ((crc << 8) | ((crc >> 8)& 0xff)) ^ data;
	crc ^= (crc & 0xff) >> 4;
	crc ^= (crc << 8) << 4;
	crc ^= ((crc & 0xff) << 4) << 1;
	return crc; 
}


void rcp_send_response(uint8 cmd, uint8 *data, uint8 len)
{
	uint8 i;
	uint16 crc = 0xffff; 

	//Calculate CRC
	crc = crc_ccitt_update(crc, MSG_RESPONSE);
	crc = crc_ccitt_update(crc, cmd);
	crc = crc_ccitt_update(crc, (char)(len >> 8));
	crc = crc_ccitt_update(crc, (char)(len &  0xff));

	for(i = 0; i < len; i++)
	{
		crc = crc_ccitt_update(crc, data[i]);		
	}
	
	crc = crc_ccitt_update(crc, MSG_ENDMARK);
	
	// Send frame
	rcp_send_frame(cmd, data, len, (uint8)(crc >> 8), (uint8)crc);

}



uint8 rcp_receive_frame(rcp_packet_type *pkt)
{
	uint8 rx_index = MSG_CMD_INDEX;	   
	uint8 data_tail_len = 0;
	 //Wait Preamble 
	while(rcp_get_byte() != MSG_PREAMBLE);

	//Run TimeOut(100ms) Timer 
	iap_timer_run(100);
	
	//Get MSG Type 
	do
	{
		if(rcp_get_status())	 // Data Ready? 
		{	
			//Get Data 
			pkt->rx_buffer[MSG_MSGTYPE_INDEX] = *((uint8 *)pdata);
		 
		 	if(pkt->rx_buffer[MSG_MSGTYPE_INDEX] == MSG_FLASH_DATA)
			{
				goto LABEL_RXDATA;
			}
			else
			{
				goto LABEL_RXCMD;
			}
		}

	}while(iap_timer_get_value() != 0);

LABEL_RXCMD:
	do
	{	
		if(rcp_get_status())
		{	
			// Get Data 
			pkt->rx_buffer[rx_index] = *((uint8 *)pdata);

			// Reload TimerOut Value
			iap_timer_reload(100);

			// Check packet Length 		
			if(rx_index == MSG_PLENH_INDEX)
			{	
				data_tail_len = pkt->rx_buffer[rx_index] << 8;
			}
			else if(rx_index == MSG_PLENL_INDEX)
			{
				data_tail_len |= pkt->rx_buffer[rx_index] + (MSG_HEADTAIL_LEN - 1);
			}
			else if(rx_index > MSG_PLENL_INDEX)
			{
				if(rx_index == data_tail_len)
					return 0;
			}
			
			// Increase rxIndex
			rx_index++;
		}
		
	}while(iap_timer_get_value() != 0);
	
	return CMD_RSP_ERCRC;
LABEL_RXDATA:
	 //Receive Data Frame 
	do
	{	
		if(rcp_get_status())
		{	
			// Get Data
			pkt->rx_buffer[rx_index] = *((uint8 *)pdata);

			// Reload TimerOut Value
			iap_timer_reload(100);

			// Check packet Length
			if(rx_index == MSG_DUMMY_INDEX)
			{
			  // for Data pointer 4byte Align 
			  pkt->rx_buffer[++rx_index] = 0; 
			}
			else if(rx_index == MSG_DATAPKT_LEN)
			{
				return 0;
			}
			// Increase rx_index 
			rx_index++;
		}		
		
	}while(iap_timer_get_value() != 0);
	
	return CMD_RSP_ERCRC;

}	



uint8 rcp_receive_command(rcp_packet_type *pkt)
{
	uint8 err = 0, len, i = 0;
	uint16 crc = 0xffff;

	err = rcp_receive_frame(pkt);
	
	//check Time Out
	if(err) return err;

	//check Msg type 
	if(pkt->rx_buffer[0] == MSG_COMMAND)	// Command Packet
	{
		// Data Length + Header + Endmask
	 	len = 	((pkt->command.data_len_h << 8) | (pkt->command.data_len_l)) + 5; 
	
		//check EndMark 
	 	if(pkt->command.data[((pkt->command.data_len_h << 8) | (pkt->command.data_len_l))] != MSG_ENDMARK)
			return CMD_RSP_ERCRC;
	}
	else if(pkt->rx_buffer[0] == MSG_FLASH_DATA)	// Data Packet
	{										 
		//Data Length + Header + Endmark + Dummy
		len = 21;  
	
		//check EndMark
		if(pkt->data.data[16] != MSG_ENDMARK)
	 		return CMD_RSP_ERCRC;	
		
		//Header CRC Calcu 
		for( ;i < 3; i++)
		{
			crc = crc_ccitt_update(crc, pkt->rx_buffer[i]);			
		}
		
		i++; // Pass Dummy Buffer 
	}
	else		// Undefined packet
	{	
		return CMD_RSP_UNDEF;
	}

	// Calcu Header CRC
	for( ; i < (len); i++)  
	{
		crc = crc_ccitt_update(crc, pkt->rx_buffer[i]);
	}

	//check CRC 
	if(crc !=  (pkt->rx_buffer[i] << 8 | pkt->rx_buffer[i+1]))
		return CMD_RSP_ERCRC;
	
	return 0;
}



void rcp_parse_command(rcp_packet_type *pkt)
{
	uint8 err = 0;
	uint32 faddr;
	
	if(pkt->rx_buffer[0] == MSG_FLASH_DATA)
	{
		//Download Data Packet 
	 	faddr = pkt->data.addr_h << 8 | pkt->data.addr_l;
		
		//Check Address, Must 4 Align
		if(faddr % 4 || faddr >= IAP_START_ADDRESS)
		{
			//err = CMD_RSP_ERCRC;				  
			rcp_send_response(CMD_DOWNLOAD, &err, 1);
		}
		else
		{	  
			err = flash_write_data(faddr, (uint32 *)pkt->data.data, 16);

			if(!err)
				rcp_send_response(CMD_DOWNLOAD, &err, 1);	
		}

	}
	else if(pkt->rx_buffer[0] == MSG_COMMAND)		
	{
		switch(pkt->command.cmd)
		{
		case CMD_RESET:
			{
				rcp_send_response(CMD_RESET, &err, 1);
				iap_timer_run(100);
				while(iap_timer_get_value() != 0);
				NVIC_SystemReset();
			}
			break;
		default:
			err = CMD_RSP_UNDEF;
			break;
		}
	}
	else
	{
		err = CMD_RSP_UNDEF;
	}
	// if Error, Send Nak Response
	if(err)
	   	rcp_send_response(CMD_ERROR, &err, 1);
}



void iap_main(uint8 mode)
{			 
	uint8 err = 0;
	//Init Timer
	iap_timer_init();
								
	// Select RCP Comm Mode
	if(mode == MODE_IAP_SPI)
	{						  		
		//Init SSP
		iap_spi_slave_init();

	   	// Set RCP Com Function Pointer 
		rcp_send_frame = &iap_spi_send_frame;
		rcp_get_status = &iap_spi_get_status;
		rcp_get_byte = & iap_spi_get_char;
		pdata = (volatile uint32 *)(SSP_BASE + 0x08);  // SSP Data Register
	}
	else if(mode == MODE_IAP_UART)
	{
		//Init Uart 
		iap_uart_init();

		//Set RCP Com Function Pointer 
		rcp_send_frame = &iap_uart_send_frame;
		rcp_get_status = &iap_uart_get_status;
		rcp_get_byte = &iap_uart_get_char;
		pdata = (volatile uint32 *)UART0_BASE;	  	// UART Data register
	}
	else
	{
		//Init I2C 
		iap_i2c_slave_init();

		//Set RCP Com Function Pointer 
		rcp_send_frame = &iap_i2c_send_frame;
		rcp_get_status = &iap_i2c_get_status;
		rcp_get_byte = &iap_i2c_get_char;
		pdata = (volatile uint32 *)(I2C_BASE + 0x0C);	  	// I2C Data register
	}	

	while(1)
	{
		//Read RCP Command Packet
		err = rcp_receive_command(&pkt);	
				
		if(err)
		{
			// Recevice Frame is CRC Error
			rcp_send_response(CMD_ERROR, &err, 1);

		}
		else
		{
			//RCP Command Analyze.
		   	rcp_parse_command(&pkt);
		}
	}

}
