/******************** (C) COPYRIGHT 2010 Advanced Design Technolgy ********************
 * File Name		: ADTM3F3SM.h
 * Author			: nsheo@adtek.co.kr
 * Version			: V1.0.0
 * Date				: 05/08/2010
 * brief			: CMSIS Cortex-M3 Device Peripheral Access Layer Header File. 
 *          		  This file contains all the peripheral register's definitions,
 *          		  bits definitions and memory mapping for ADTM3F3SM
 **************************************************************************************/


/* brief Configuration of the Cortex-M3 Processor and Core Peripherals */
#define __MPU_PRESENT             0 /*!< ADTM3F3SM does not provide a MPU present or not  */
#define __NVIC_PRIO_BITS          4 /*!< ADTM3F3SM uses 4 Bits for the Priority Levels    */
#define __Vendor_SysTickConfig    0 /*!< Set to 1 if different SysTick Config is used */


/* Interrupt Number Definition */
typedef enum IRQn
{
/******  Cortex-M3 Processor Exceptions Numbers ***************************************************/
  NonMaskableInt_IRQn         = -14,    /*!< 2 Non Maskable Interrupt                             */
  MemoryManagement_IRQn       = -12,    /*!< 4 Cortex-M3 Memory Management Interrupt              */
  BusFault_IRQn               = -11,    /*!< 5 Cortex-M3 Bus Fault Interrupt                      */
  UsageFault_IRQn             = -10,    /*!< 6 Cortex-M3 Usage Fault Interrupt                    */
  SVCall_IRQn                 = -5,     /*!< 11 Cortex-M3 SV Call Interrupt                       */
  DebugMonitor_IRQn           = -4,     /*!< 12 Cortex-M3 Debug Monitor Interrupt                 */
  PendSV_IRQn                 = -2,     /*!< 14 Cortex-M3 Pend SV Interrupt                       */
  SysTick_IRQn                = -1,     /*!< 15 Cortex-M3 System Tick Interrupt                   */

/******  ADTM3F3SM specific Interrupt Numbers *****************************************************/
  WDT_IRQn						= 0,      /*!< Watchdog Timer Interrupt                           */
  TIMER0_IRQn					= 1,      /*!< Timer0 Interrupt   						          */
  TIMER1_IRQn           	    = 2,      /*!< Timer1 Interrupt          	                  	  */
  TIMER2_IRQn					= 3,      /*!< Timer2 Interrupt              	                  */
  TIMER3_IRQn					= 4,      /*!< Timer3 Interrupt                                   */
  UART0_RX_IRQn					= 5,      /*!< UART0 RX Interrupt                                 */
  UART0_TX_IRQn					= 6,      /*!< UART0 TX Interrupt                   		      */
  UART1_RX_IRQn					= 7,      /*!< UART1 RX Interrupt                           	  */
  UART1_TX_IRQn					= 8,      /*!< UART1 TX Interrupt                                 */
  SPI_RX_IRQn					= 9,      /*!< SPI Interrupt                                  	  */
  SPI_TX_IRQn					= 10,	
  I2C_IRQn						= 11,     /*!< I2C Interrupt                                      */
  EXT0_IRQn						= 12,     /*!< External 0 Interrupt                               */
  EXT1_IRQn						= 13,     /*!< External 1 Interrupt                           	  */
  EXT2_IRQn						= 14,     /*!< External 2 Interrupt                               */
  EXT3_IRQn						= 15,     /*!< External 3 Interrupt                               */
  EXT4_IRQn						= 16,     /*!< External 4 Interrupt   					          */
  EXT5_IRQn						= 17,     /*!< External 5 Interrupt 			                  */
  GPIO0_IRQn					= 18,     /*!< GPIO0 Interrupt             			          	  */
  GPIO1_IRQn					= 19,     /*!< GPIO1 Interrupt  			                      */
  WAKEINT_TX_IRQn				= 20,	  /*!< Wake Up Interrupt								  */		     	
  UART0_ERR_IRQn				= 21,     /*!< UART0 ERROR Interrupt  						      */
  UART1_ERR_IRQn				= 22,     /*!< UART1 ERROR Interrupt   	                          */
  SPI0_ERR_IRQn					= 23,     /*!< SPI0 ERROR Interrupt   	                          */
  RSSI_IRQn						= 24,     /*!< RAMP Interrupt(ES) or RSSI Interrupt(CS)	          */
  MT2_IRQn						= 25,     /*!< MT2 ERROR Interrupt   	                          */
  MT1_IRQn						= 26,     /*!< MT1 ERROR Interrupt   	                          */
  TX_DONE_IRQn					= 27,     /*!< TX DONE Interrupt   	                      		  */
  RX_FAIL_IRQn					= 28,     /*!< RX FAIL Interrupt   	                        	  */
  RX_SUCCESS_IRQn				= 29,     /*!< RX SUCCESS Interrupt   	                          */
  RX_RAW_IRQn					= 30,     /*!< RX RAW Interrupt     	                          */
  LBT_IRQn						= 31, 	  /*!< Reserved Interrupt(ES) or LBT Interrupt			  */
} IRQn_Type;

/******  CMSIS Including  ******/
#include "core_cm0.h"
#include "system_PR9200.h"

#include <stdint.h>
/******  Device Driver Including ******/
#include "debug.h"
#include "uart.h"
#include "spi.h"
#include "timer.h"
#include "i2c.h"




/******  Type Definitions ******/
typedef int32_t  s32;
typedef int16_t s16;
typedef int8_t  s8;

typedef const int32_t sc32;  /*!< Read Only */
typedef const int16_t sc16;  /*!< Read Only */
typedef const int8_t sc8;   /*!< Read Only */

typedef __IO int32_t  vs32;
typedef __IO int16_t  vs16;
typedef __IO int8_t   vs8;

typedef __I int32_t vsc32;  /*!< Read Only */
typedef __I int16_t vsc16;  /*!< Read Only */
typedef __I int8_t vsc8;   /*!< Read Only */

typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;

typedef const uint32_t uc32;  /*!< Read Only */
typedef const uint16_t uc16;  /*!< Read Only */
typedef const uint8_t uc8;   /*!< Read Only */

typedef __IO uint32_t  vu32;
typedef __IO uint16_t vu16;
typedef __IO uint8_t  vu8;

typedef __I uint32_t vuc32;  /*!< Read Only */
typedef __I uint16_t vuc16;  /*!< Read Only */
typedef __I uint8_t vuc8;   /*!< Read Only */





//-------------------------------------------------------------------
// UART Type Definition
//-------------------------------------------------------------------
typedef struct
{
	__IO	uint32_t	DR;				// 0x000
	__IO	uint32_t	RSR;		// 0x004
			uint32_t	RESERVED0[4];	// 0x008 ~ 0x014
	__I		uint32_t	FR;				// 0x018
			uint32_t	RESERVED1;		// 0x01c
	__IO	uint32_t	ILPR;			// 0x020	
	__IO	uint32_t	IBRD;			// 0x024
	__IO	uint32_t	FBRD;			// 0x028
	__IO	uint32_t	LCRH;			// 0x02C
	__IO	uint32_t	CR;				// 0x030
	__IO	uint32_t	IFLS;			// 0x034
	__IO	uint32_t	IMSC;			// 0x038
	__I		uint32_t	RIS;			// 0x03C
	__I		uint32_t 	MIS;			// 0x040
	__O		uint32_t	ICR;			// 0x044
	__IO	uint32_t	DMACR;			// 0x048
}UART_Type;



//-------------------------------------------------------------------
// SSP Type Definition
//-------------------------------------------------------------------
typedef struct
{
	__IO	uint32_t	CR0;			//0x00
	__IO	uint32_t	CR1;			//0x04
	__IO	uint32_t	DR;			//0x08
	__I		uint32_t	SR;			//0x0C
	__IO	uint32_t	CPSR;		//0x10
	__IO	uint32_t	IMSC;		//0x14
	__I		uint32_t	RIS;			//0x18
	__I		uint32_t	MIS;			//0x1C
	__O		uint32_t	ICR;			//0x20
		
}SSP_Type;


//-------------------------------------------------------------------
// I2C Type Definition
//-------------------------------------------------------------------
typedef struct
{
	__IO	uint32_t	ICCR;		//0x00
	__IO	uint32_t	ICSR;		//0x04
	__IO	uint32_t	IAR;		//0x08
	__IO	uint32_t	IDSR;		//0x0C
}I2C_Type;



//-------------------------------------------------------------------
// WDT Type Definition
//-------------------------------------------------------------------
typedef struct
{
	__IO	uint32_t	LOAD;			//0x00
	__I		uint32_t	VALUE;		//0x04
	__IO	uint32_t	CONTROL;		//0x08
	__O		uint32_t	INTCLR;		//0x0C
	__I		uint32_t	RIS;			//0x10
	__I		uint32_t	MIS;			//0x14
	__IO	uint32_t	LOCK;			//0x1C
}WDT_Type;



//-------------------------------------------------------------------
// TIMER Type Definition
//-------------------------------------------------------------------
typedef struct
{
	__IO	uint32_t	LOAD;			//0x00
	__I		uint32_t	VALUE;		//0x04
	__IO	uint32_t	CONTROL;		//0x08
	__O		uint32_t	INTCLR;		//0x0C
	__I		uint32_t	RIS;			//0x10
	__I		uint32_t	MIS;			//0x14
	__IO	uint32_t	BGLOAD;		//0x18
}TIMER_Type;



//-------------------------------------------------------------------
// System Control Type Definition
//-------------------------------------------------------------------
typedef struct
{
			uint32_t	RESERVED3[2];		//0x00, 0x04
	__IO	uint32_t	RESETSTATUS;		//0x08
	__O		uint32_t	RESETSTATUSCLR;		//0x0C
	__IO	uint32_t	CLOCKCON;			//0x10
	__IO	uint32_t	CLOCKCNT;			//0x14
	__I		uint32_t	TESTMODE;			//0x18
	__IO	uint32_t	TXRAW;				//0x1C
	__IO	uint32_t	MODEMRST;			//0x20
}SYSCON_Type;


//-------------------------------------------------------------------
// GPIO Type Definition
//-------------------------------------------------------------------
typedef struct
{
			uint32_t	RESERVED4[0xFF];	
	__IO	uint32_t	DATA;			//0x3FC
	__IO	uint32_t	DIR;			//0x400
	__IO	uint32_t	IS;				//0x404
	__IO	uint32_t	IBE;			//0x408
	__IO	uint32_t	IEV;			//0x40C
	__IO	uint32_t	IE;				//0x410	
	__I		uint32_t	RIS;			//0x414
	__I		uint32_t	MIS;			//0x418
	__O		uint32_t	IC;				//0x41C
	__IO	uint32_t	AFSEL;			//0x420
}GPIO_Type;


//-------------------------------------------------------------------
// Peripheral Memory MAP
//-------------------------------------------------------------------

#define		FALSH_BASE		(0x00000000UL)
#define 	RAM_BASE		(0x20000000UL)

#define 	APB_BASE		(0x50000000UL)
#define 	AHB_BASE		(0x40000000UL)


#define		SYSCON_BASE		(APB_BASE + 0x0000)
#define 	WDT_BASE		(APB_BASE + 0x1000)
#define 	TIMER0_BASE		(APB_BASE + 0x2000)
#define		TIMER1_BASE		(APB_BASE + 0x2020)		// 0x50002020
#define   	TIMER2_BASE		(APB_BASE + 0x3000)
#define 	TIMER3_BASE		(APB_BASE + 0x3020)		//0x50003020
#define 	GPIO0_BASE		(APB_BASE + 0x4000)
#define 	GPIO1_BASE		(APB_BASE + 0x5000)
#define		UART0_BASE		(APB_BASE + 0x8000)
#define 	UART1_BASE		(APB_BASE + 0x9000)
#define 	I2C_BASE		(APB_BASE + 0xA000)
#define		SSP_BASE		(APB_BASE + 0xB000)





#define 	SYSCON			((	 SYSCON_Type *) SYSCON_BASE)
#define   	WDT				((		WDT_Type *)	WDT_BASE)
#define		TIMER0			((	  TIMER_Type *) TIMER0_BASE)
#define 	TIMER1			((    TIMER_Type *) TIMER1_BASE)
#define 	TIMER2			((	  TIMER_Type *) TIMER2_BASE)
#define 	TIMER3 			((	  TIMER_Type *) TIMER3_BASE)
#define 	GPIO0			((	   GPIO_Type *) GPIO0_BASE)
#define		GPIO1			((	   GPIO_Type *) GPIO1_BASE)
#define		UART0			((	   UART_Type *) UART0_BASE)
#define 	UART1			((	   UART_Type *) UART1_BASE)
#define 	I2C				((	    I2C_Type *)	I2C_BASE)
#define 	SSP				((	    SSP_Type *) SSP_BASE)



